package com.example.syneotek001.androidtabsexample25042018.model;

import java.io.Serializable;

public class EmailListModel implements Serializable {
    private String attachment = "";
    private int bgCircle;
    private String body = "";
    private long dateTime;
    private String fromEmail = "";
    private String image = "";
    private boolean isRead;
    private String subject = "";
    private String title = "";
    private String toEmail = "";
    private String type = "";

    public EmailListModel(String type, String title, String toEmail, String fromEmail, String subject, String body, String image, String attachment, long dateTime, int bgCircle, boolean isRead) {
        this.type = type;
        this.title = title;
        this.toEmail = toEmail;
        this.fromEmail = fromEmail;
        this.subject = subject;
        this.body = body;
        this.image = image;
        this.attachment = attachment;
        this.dateTime = dateTime;
        this.bgCircle = bgCircle;
        this.isRead = isRead;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getToEmail() {
        return this.toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getFromEmail() {
        return this.fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAttachment() {
        return this.attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public long getDateTime() {
        return this.dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public int getBgCircle() {
        return this.bgCircle;
    }

    public void setBgCircle(int bgCircle) {
        this.bgCircle = bgCircle;
    }

    public boolean isRead() {
        return this.isRead;
    }

    public void setRead(boolean read) {
        this.isRead = read;
    }

    public String toString() {
        return "EmailListModel{type='" + this.type + '\'' + ", title='" + this.title + '\'' + ", toEmail='" + this.toEmail + '\'' + ", fromEmail='" + this.fromEmail + '\'' + ", subject='" + this.subject + '\'' + ", body='" + this.body + '\'' + ", image='" + this.image + '\'' + ", attachment='" + this.attachment + '\'' + ", dateTime=" + this.dateTime + ", isRead=" + this.isRead + '}';
    }
}
