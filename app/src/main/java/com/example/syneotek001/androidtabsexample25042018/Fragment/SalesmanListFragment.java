package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.SalesmanItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanItemModel;

import java.util.ArrayList;
import java.util.Calendar;

public class SalesmanListFragment extends Fragment implements OnLeadItemViewClickListener {
    public static final String BUNDLE_UPDATE_LABEL_POSITION = "position";
    public static final String BUNDLE_UPDATE_LABEL_VALUE = "labelValue";
    public static final int LEAD_LIST_UPDATE_LABEL = 1;
    ArrayList<SalesmanItemModel> leadItemArray = new ArrayList<>();
    SalesmanItemAdapter mAdapter;
    View view;
    public RecyclerView recyclerView;
    public TextView tvNoDataFound, tvTitle;
    AppCompatImageView ivAdd;
    ImageView ivBack;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_salesman_list, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        tvNoDataFound = view.findViewById(R.id.tvNoDataFound);
        tvTitle = view.findViewById(R.id.tvTitle);
        ivAdd = view.findViewById(R.id.ivAdd);
        ivBack = view.findViewById(R.id.ivBack);

        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open add salesman page
                EditSalesmanFragment editSalesmanFragment = new EditSalesmanFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("is_edit",false);
                bundle.putString("profile_type","salesman");
                editSalesmanFragment.setArguments(bundle);
                ((HomeActivity) getActivity()).replaceFragment(editSalesmanFragment);
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });

        tvTitle.setText(getString(R.string.str_salesman_manager));

        setUpRecyclerView();
        return view;
    }

    private void setUpRecyclerView() {
        leadItemArray.clear();

        leadItemArray.add(new SalesmanItemModel("1", "Abc", "Patel", "abc@gmail.com", "9876655443", "123456", "Maharashtra", "Mumbai", "Pune", "623456", "English", "Mumbai", "Description dummy"));
        leadItemArray.add(new SalesmanItemModel("2", "Xyz", "test", "xyz@gmail.com", "9876655443", "123456", "Tamilnadu", "Chennai", "Kanchipuram", "656456", "Tamil", "Chennai", "Description dummy"));
        leadItemArray.add(new SalesmanItemModel("1", "Pqr", "Shah", "pqr@gmail.com", "9879665987", "123456", "Karnataka", "Bangalore", "Hebbal", "667834", "Kannada", "Bangalore", "Description dummy"));
        leadItemArray.add(new SalesmanItemModel("3", "qwerty", "Panchal", "qwerty@gmail.com", "9978965989", "123456", "Tamilnadu", "Madurai", "Madurai", "600006", "Tamil", "Madurai", "Description dummy"));
        leadItemArray.add(new SalesmanItemModel("2", "Paresh", "Prajapati", "paresh@gmail.com", "9978966598", "123456", "Aandhra", "Hyderabad", "Hyderabad", "611237", "English", "Hyderabad", "Description dummy"));
        leadItemArray.add(new SalesmanItemModel("1", "Raj", "Sadhu", "raj@gmail.com", "9876655443", "123456", "Kerala", "Kochin", "Ernakulam", "623456", "English", "Kochin", "Description dummy"));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_right));
        mAdapter = new SalesmanItemAdapter(getActivity(), leadItemArray, this);
        recyclerView.setAdapter(mAdapter);
    }

    public void onItemClicked(int position) {
    }


    public void onLeadDetailIconClicked(int position) {
//        ((HomeActivity) getActivity()).replaceFragment(new LeadDetailFragment());
    }

    public void onLeadEmailIconClicked(int position) {
        ((HomeActivity) getActivity()).replaceFragment(new ComposeFragment());
    }

    public void onLeadScheduleIconClicked(int position) {
        // open edit page here
        EditSalesmanFragment editSalesmanFragment = new EditSalesmanFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("salesman_profile",leadItemArray.get(position));
        bundle.putBoolean("is_edit",true);
        bundle.putString("profile_type","salesman");
        editSalesmanFragment.setArguments(bundle);
        ((HomeActivity) getActivity()).replaceFragment(editSalesmanFragment);
    }

    public void onLeadInactiveIconClicked(int position) {
    }

    @Override
    public void onLeadLabelClicked(int i) {

    }

    public void onLeadDealRequestClicked(int position) {
    }

    public void onLeadBusinessRequestClicked(int position) {
    }


    @Override
    public void onResume() {
        super.onResume();
        setUpRecyclerView();
    }
}
