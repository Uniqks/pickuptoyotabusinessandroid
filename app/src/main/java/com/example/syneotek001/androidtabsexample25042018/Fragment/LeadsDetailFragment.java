package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.LineGraphDataModel;

import java.util.ArrayList;

public class LeadsDetailFragment extends Fragment implements OnClickListener {
    View view;
    ImageView ivback;
    ArrayList<LineGraphDataModel> leadsEntry = new ArrayList<>();
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_leads_detail, container, false);

        ivback = view.findViewById(R.id.ivBack);
        ivback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        setClickEvents();
        return view;
    }

    private void setClickEvents() {
    }



    public void onClick(View view) {

    }

}
