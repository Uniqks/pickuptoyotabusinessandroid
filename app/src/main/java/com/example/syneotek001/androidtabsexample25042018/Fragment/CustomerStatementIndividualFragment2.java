package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentCustomerStatementIndividual2Binding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentCustomerStatementIndividualBinding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class CustomerStatementIndividualFragment2 extends Fragment implements OnClickListener, OnBackPressed {
    public Button btnSave, btnSaveAndNext;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;
     //FragmentSalesmanProfileStep1Binding binding;
    FragmentCustomerStatementIndividual2Binding binding;
    String name, email, mobile, password, address;
    YoYo yoYo;
    ArrayList<String> arr_salesman_status = new ArrayList<>();

    Typeface typeface;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_customer_statement_individual2, container, false);
        View view = binding.getRoot();

        typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/montserrat_regular.otf");

 /*       btnSave = rootview.findViewById(R.id.btnSave);
        btnSaveAndNext = rootview.findViewById(R.id.btnSaveAndNext);
        etEmail = rootview.findViewById(R.id.etEmail);
        etFirstName = rootview.findViewById(R.id.etFirstName);
        etHomePhone = rootview.findViewById(R.id.etHomePhone);
        etLastName = rootview.findViewById(R.id.etLastName);
        etMobile = rootview.findViewById(R.id.etMobile);
        etTitle = rootview.findViewById(R.id.etTitle);
        etWorkFax = rootview.findViewById(R.id.etWorkFax);
        etWorkPhone = rootview.findViewById(R.id.etWorkPhone);
        ivContact = rootview.findViewById(R.id.ivContact);
        ivEmail = rootview.findViewById(R.id.ivEmail);
        ivMobile = rootview.findViewById(R.id.ivMobile);
        ivTitle = rootview.findViewById(R.id.ivTitle);
        tvPageIndicator = rootview.findViewById(R.id.tvPageIndicator);
        tvTitle = rootview.findViewById(R.id.tvTitle);
        ivBack = rootview.findViewById(R.id.ivBack);*/
        binding.ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });

        binding.rbLeaseOrRent.setTypeface(typeface);
        binding.rbOwnResidenceFree.setTypeface(typeface);
        binding.rbOwnResidenceMortgage.setTypeface(typeface);
        binding.rbWithParents.setTypeface(typeface);

       binding.llStartDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int year;
                int month;
                int day;
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                monthOfYear = monthOfYear + 1;
                                String monthString = monthOfYear < 10 ? "0" + monthOfYear : "" + monthOfYear;
                                String dayString = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
                                binding.txtStartDate.setText(year + "-" + monthString + "-" + dayString);
//                                txtStartDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                                dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            }
                        }, year, month, day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });


        setHasOptionsMenu(false);
        setClickEvents();

        setAnimation(binding.lblAptUnit);
        setAnimation(binding.lblBalance);
        setAnimation(binding.lblCity);
        setAnimation(binding.lblFirstName);
        setAnimation(binding.llStartDate);
        setAnimation(binding.lblLastName);
        setAnimation(binding.lblMiddleName);
        setAnimation(binding.lblMortgageCoLandlord);
        setAnimation(binding.lblPhone);
        setAnimation(binding.lblPostalCode);
        setAnimation(binding.lblPresentAddress);
        setAnimation(binding.lblPreviousAddress);
        setAnimation(binding.lblPreviousCity);
        setAnimation(binding.lblPreviousPostalCode);
        setAnimation(binding.lblPreviousProvince);
        setAnimation(binding.lblPreviousTimeAtAddress);
        setAnimation(binding.lblPreviousTimeAtAddressMonths);
        setAnimation(binding.lblPreviousTimeAtAddressYears);
        setAnimation(binding.lblProvince);
        setAnimation(binding.lblRentPerMonth);
        setAnimation(binding.lblResidenceType);
        setAnimation(binding.lblSocialInsurance);
        setAnimation(binding.lblStartDate);
        setAnimation(binding.lblTimeAtAddress);
        setAnimation(binding.lblTimeAtAddressMonths);
        setAnimation(binding.lblTimeAtAddressYears);

        setAnimation(binding.etAptUnit);
        setAnimation(binding.etBalance);
        setAnimation(binding.etCity);
        setAnimation(binding.etFirstName);
        setAnimation(binding.etLastName);
        setAnimation(binding.etMiddleName);
        setAnimation(binding.etMortgageCoLandlord);
        setAnimation(binding.etPhone);
        setAnimation(binding.etPostalCode);
        setAnimation(binding.etPresentAddress);
        setAnimation(binding.etPreviousAddress);
        setAnimation(binding.etPreviousCity);
        setAnimation(binding.etPreviousPostalCode);
        setAnimation(binding.etPreviousProvince);
        setAnimation(binding.etPreviousTimeAtAddressMonths);
        setAnimation(binding.etPreviousTimeAtAddressYears);
        setAnimation(binding.etProvince);
        setAnimation(binding.etRentPerMonth);
        setAnimation(binding.etSocialInsurance);
        setAnimation(binding.etTimeAtAddressMonths);
        setAnimation(binding.etTimeAtAddressYears);

        arr_salesman_status = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_salesman_status)));


        return view;
    }



    public void setAnimation(View view) {
        YoYo.with(Techniques.FlipInX).duration(1000).delay(150).repeat(0).playOn(view);
    }

    private void setClickEvents() {
        binding.btnSaveAndNext.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSaveAndNext:
                Log.e("btnSaveAndNext","btnSaveAndNext");
                CustomerStatementIndividualFragment3 billSaleFragment2 = new CustomerStatementIndividualFragment3();
                 ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
              /*  if (getArguments() != null) {
                    if (getArguments().getBoolean("is_edit")) {
                        if (getArguments().getSerializable("salesman_profile") != null) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("salesman_profile", getArguments().getSerializable("salesman_profile"));
                            bundle.putBoolean("is_edit", true);
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    } else {
                        if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("is_edit", false);
                            bundle.putString("profile_type",getArguments().getString("profile_type"));
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    }
                }*/
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
