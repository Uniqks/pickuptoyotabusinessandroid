package com.example.syneotek001.androidtabsexample25042018.model;

public class DealRequestsModel {
    private String dealId = "";
    private String customerId = "";
    private String salesManName = "";
    private String status = "";
    private String label = "";
    private boolean isSelected = false;

    public DealRequestsModel(String dealId, String salesManName, String customerId, String label, String status) {
        this.salesManName = salesManName;
        this.customerId = customerId;
        this.status = status;
        this.dealId = dealId;
        this.label = label;
        this.isSelected = false;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSalesManName() {
        return this.salesManName;
    }

    public void setSalesManName(String salesManName) {
        this.salesManName = salesManName;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public String toString() {
        return "DealRequestModel{salesManName='" + this.salesManName + '\'' + ", customerId='" + this.customerId + '\'' + ", status='" + this.status + '\'' + '}';
    }
}
