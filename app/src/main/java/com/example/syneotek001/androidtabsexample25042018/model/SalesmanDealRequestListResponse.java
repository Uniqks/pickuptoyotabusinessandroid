package com.example.syneotek001.androidtabsexample25042018.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bluegenie-24 on 9/8/18.
 */

public class SalesmanDealRequestListResponse implements Serializable {
    public ArrayList<SalesmanDealRequestListData> dealRequests;

    public ArrayList<SalesmanDealRequestListData> getDealRequests() {
        return dealRequests;
    }

    public void setDealRequests(ArrayList<SalesmanDealRequestListData> dealRequests) {
        this.dealRequests = dealRequests;
    }

    public class SalesmanDealRequestListData implements Serializable {

        String dr_count;
        String id;
        String salesman_id;
        String lead_id;
        String deal_number;
        String request_notes;
        String request_attachment;
        String dr_datetime;
        String is_read;
        String customer_id;
        String firstname;
        String lastname;

        public String getDr_count() {
            return dr_count;
        }

        public void setDr_count(String dr_count) {
            this.dr_count = dr_count;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSalesman_id() {
            return salesman_id;
        }

        public void setSalesman_id(String salesman_id) {
            this.salesman_id = salesman_id;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getDeal_number() {
            return deal_number;
        }

        public void setDeal_number(String deal_number) {
            this.deal_number = deal_number;
        }

        public String getRequest_notes() {
            return request_notes;
        }

        public void setRequest_notes(String request_notes) {
            this.request_notes = request_notes;
        }

        public String getRequest_attachment() {
            return request_attachment;
        }

        public void setRequest_attachment(String request_attachment) {
            this.request_attachment = request_attachment;
        }

        public String getDr_datetime() {
            return dr_datetime;
        }

        public void setDr_datetime(String dr_datetime) {
            this.dr_datetime = dr_datetime;
        }

        public String getIs_read() {
            return is_read;
        }

        public void setIs_read(String is_read) {
            this.is_read = is_read;
        }

        public String getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public ArrayList<RequestListData> request_list;

        public ArrayList<RequestListData> getRequest_list() {
            return request_list;
        }

        public void setRequest_list(ArrayList<RequestListData> request_list) {
            this.request_list = request_list;
        }

        public class RequestListData implements Serializable {
            String request_id;
            String request_name;

            public String getRequest_id() {
                return request_id;
            }

            public void setRequest_id(String request_id) {
                this.request_id = request_id;
            }

            public String getRequest_name() {
                return request_name;
            }

            public void setRequest_name(String request_name) {
                this.request_name = request_name;
            }
        }

    }
}
