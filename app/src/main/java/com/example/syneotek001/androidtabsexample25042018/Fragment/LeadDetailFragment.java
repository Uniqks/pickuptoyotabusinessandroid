package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.LineGraphDataModel;
import com.github.mikephil.charting.data.Entry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

public class LeadDetailFragment extends Fragment implements OnClickListener {
    View view;
    TextView tvLeadNotes, tvweb_activity, tv_Schedule, txt_Objective, txt_Chart,tvFullDesc;
    ImageView ivback;
    ArrayList<LineGraphDataModel> leadsEntry = new ArrayList<>();
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lead_detail, container, false);
        tvLeadNotes = view.findViewById(R.id.tvLeadNotes);
        tvFullDesc = view.findViewById(R.id.tvFullDesc);
        tvweb_activity = view.findViewById(R.id.tvweb_activity);
        txt_Objective = view.findViewById(R.id.txt_Objective);
        tv_Schedule = view.findViewById(R.id.tv_Schedule);
        txt_Chart = view.findViewById(R.id.txt_Chart);

        ivback = view.findViewById(R.id.ivBack);
        ivback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        setClickEvents();
        return view;
    }

    private void setClickEvents() {
        tvLeadNotes.setOnClickListener(this);
        tvFullDesc.setOnClickListener(this);
        tvweb_activity.setOnClickListener(this);
        tv_Schedule.setOnClickListener(this);
        txt_Objective.setOnClickListener(this);
        txt_Chart.setOnClickListener(this);
    }

    private void prepareLayouts(int count, float range) {
        ArrayList<Entry> newLeads = new ArrayList();
        ArrayList<Entry> followUpLeads = new ArrayList();
        ArrayList<Entry> underContractLeads = new ArrayList();
        ArrayList<Entry> deliveryLeads = new ArrayList();
        ArrayList<Entry> soldLeads = new ArrayList();
        ArrayList<Entry> inactiveLeads = new ArrayList();
        for (int i = 0; i < count; i++) {
            newLeads.add(new Entry((float) i, ((float) (Math.random() * ((double) range))) + 3.0f, getResources().getDrawable(R.drawable.ic_chart_marker_yellow)));
            followUpLeads.add(new Entry((float) i, 20.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_orange)));
            underContractLeads.add(new Entry((float) i, 50.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_sky)));
            deliveryLeads.add(new Entry((float) i, 30.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_green)));
            soldLeads.add(new Entry((float) i, 10.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_pink)));
            inactiveLeads.add(new Entry((float) i, 25.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_red)));
        }
        leadsEntry = new ArrayList();
        leadsEntry.add(new LineGraphDataModel("New", ContextCompat.getColor(getActivity(), R.color.yellow_opacity_80), ContextCompat.getDrawable(getActivity(), R.drawable.fade_yellow), ContextCompat.getColor(getActivity(), R.color.yellow_opacity_50), newLeads));
        leadsEntry.add(new LineGraphDataModel("FollowUp", ContextCompat.getColor(getActivity(), R.color.orange_opacity_80), ContextCompat.getDrawable(getActivity(), R.drawable.fade_orange), ContextCompat.getColor(getActivity(), R.color.orange_opacity_50), followUpLeads));
        leadsEntry.add(new LineGraphDataModel("UnderContract", ContextCompat.getColor(getActivity(), R.color.sky_opacity_80), ContextCompat.getDrawable(getActivity(), R.drawable.fade_sky), ContextCompat.getColor(getActivity(), R.color.sky_opacity_50), underContractLeads));
        leadsEntry.add(new LineGraphDataModel("Delivery", ContextCompat.getColor(getActivity(), R.color.green_opacity_80), ContextCompat.getDrawable(getActivity(), R.drawable.fade_green), ContextCompat.getColor(getActivity(), R.color.green_opacity_50), deliveryLeads));
        leadsEntry.add(new LineGraphDataModel("Sold", ContextCompat.getColor(getActivity(), R.color.pink_opacity_80), ContextCompat.getDrawable(getActivity(), R.drawable.fade_pink), ContextCompat.getColor(getActivity(), R.color.pink_opacity_50), soldLeads));
        leadsEntry.add(new LineGraphDataModel("Inactive", ContextCompat.getColor(getActivity(), R.color.red_opacity_80), ContextCompat.getDrawable(getActivity(), R.drawable.fade_red), ContextCompat.getColor(getActivity(), R.color.red_opacity_50), inactiveLeads));
        LeadsIndividualFragment leadsIndividualFragment = new LeadsIndividualFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(LeadsIndividualFragment.BUNDLE_DATA_SET, (Serializable) leadsEntry.get(0));
        bundle.putString(LeadsIndividualFragment.BUNDLE_DATA_SET_TYPE, getResources().getString(R.string.lead_detail));
        leadsIndividualFragment.setArguments(bundle);
        ((HomeActivity) getActivity()).replaceFragment(leadsIndividualFragment);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvLeadNotes:
                ((HomeActivity) getActivity()).replaceFragment(new LeadNoteListFragment());
                break;
            case R.id.tvFullDesc:
                ((HomeActivity) getActivity()).replaceFragment(new LeadFullDescFragment());
                break;
            case R.id.tvweb_activity:
                ((HomeActivity) getActivity()).replaceFragment(new LeadWebActivtyFragment());
                break;
            case R.id.tv_Schedule:
//                ((HomeActivity) getActivity()).replaceFragment(new CalinderViewFragment());

                int year;
                int month;
                int day;
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
//                                et_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                                dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                ((HomeActivity) getActivity()).replaceFragment(new AddSheduleFragment());
                            }
                        }, year, month, day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

                break;
            case R.id.txt_Objective:
                ((HomeActivity) getActivity()).replaceFragment(new LeadObjectiveFragment());
                break;

            case R.id.txt_Chart:
                prepareLayouts(8, 800.0f);
                break;

            default:
                return;
        }
    }

}
