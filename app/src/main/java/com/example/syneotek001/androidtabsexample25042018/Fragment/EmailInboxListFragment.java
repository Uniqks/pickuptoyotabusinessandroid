package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.EmailItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.MainActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.EmailAdapterListener;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.EmailListModel;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EmailInboxListFragment extends Fragment implements EmailAdapterListener, View.OnClickListener, OnBackPressed {
    TypedArray bgCircleList;
    private ActionMode actionMode;
    private ActionModeCallback actionModeCallback;
    private ArrayList<EmailListModel> inboxEmailList = new ArrayList<>();
    EmailItemAdapter mAdapter;
    private int selectedItem = -1;
    View view;
    public TextView tvNoEmailFound;
    public RecyclerView recyclerView;

    public FloatingActionButton fabCompose, fabDraft, fabFolders, fabInbox, fabJunk, fabSent, fabTrash;
    public FloatingActionMenu fabEmailMenu;
    private Handler mHandler;
    private Context mcontext;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        mcontext = getActivity();
        View view = inflater.inflate(R.layout.fragment_email_list, container, false);
        // Inflate the layout for this fragment
        bgCircleList = getResources().obtainTypedArray(R.array.bg_circles);
        tvNoEmailFound = view.findViewById(R.id.tvNoEmailFound);
        recyclerView = view.findViewById(R.id.recyclerView);

        fabCompose = view.findViewById(R.id.fabCompose);
        fabDraft = view.findViewById(R.id.fabDraft);
        fabEmailMenu = view.findViewById(R.id.fabEmailMenu);
        fabFolders = view.findViewById(R.id.fabFolders);
        fabInbox = view.findViewById(R.id.fabInbox);
        fabJunk = view.findViewById(R.id.fabJunk);
        fabSent = view.findViewById(R.id.fabSent);
        fabTrash = view.findViewById(R.id.fabTrash);

        prepareFabMenu();
        setUpRecyclerView();
        setHasOptionsMenu(false);

        ImageView ivBack = view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        return view;
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    class onSrollListener extends OnScrollListener {
        onSrollListener() {
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy > 0 && !fabEmailMenu.isMenuButtonHidden()) {
                fabEmailMenu.hideMenuButton(true);
            } else if (dy < 0 && fabEmailMenu.isMenuButtonHidden()) {
                fabEmailMenu.showMenuButton(true);
            }
        }
    }

    private class ActionModeCallback implements Callback {

        class Clickon implements Runnable {
            Clickon() {
            }

            public void run() {
                mAdapter.resetAnimationIndex();
                mAdapter.notifyDataSetChanged();
            }
        }

        private ActionModeCallback() {
        }

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_email_delete, menu);
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            menu.getItem(1).setIcon(ContextCompat.getDrawable(getActivity(), ((EmailListModel) EmailInboxListFragment.this.inboxEmailList.get(EmailInboxListFragment.this.selectedItem)).isRead() ? R.drawable.ic_vector_email_white_unread : R.drawable.ic_vector_email_read_white));
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.delete:
                    EmailInboxListFragment.this.deleteMessages();
                    mode.finish();
                    return true;
                case R.id.read:
                    EmailInboxListFragment.this.readMessages();
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        public void onDestroyActionMode(ActionMode mode) {
            mAdapter.clearSelections();
            actionMode = null;
            recyclerView.post(new Clickon());
        }
    }

    /* @Nullable
     public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         if (this.view == null) {
             this.mBinding = (FragmentEmailListBinding) DataBindingUtil.inflate(inflater, R.layout.fragment_email_list, container, false);
             this.view = this.mBinding.getRoot();
             setupToolBarWithBackArrow(this.mBinding.toolbar.toolbar, this.mContext.getResources().getString(R.string.inbox));

         }
         setHasOptionsMenu(false);
         return this.view;
     }
 */
    private void prepareFabMenu() {
        ArrayList<FloatingActionButton> fabButtons = new ArrayList();
        fabButtons.add(fabCompose);
        fabButtons.add(fabInbox);
        fabButtons.add(fabDraft);
        fabButtons.add(fabSent);
        fabButtons.add(fabJunk);
        fabButtons.add(fabTrash);
        fabButtons.add(fabFolders);
        prepareFabMenu(fabEmailMenu, fabButtons, fabInbox);
    }

    private void setUpRecyclerView() {
        this.inboxEmailList.add(new EmailListModel("1", "Greetings", "abc@gmail.com", "test@gmail.com", "This is test Message Detail", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), false));
        this.inboxEmailList.add(new EmailListModel("1", "Good Morning", "abc@gmail.com", "test@gmail.com", "WelCome to Pickering Toyota", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), false));
        this.inboxEmailList.add(new EmailListModel("1", "Hello", "abc@gmail.com", "test@gmail.com", "Hey!!", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), true));
        this.inboxEmailList.add(new EmailListModel("1", "Congratulations!!", "abc@gmail.com", "test@gmail.com", "Message Detail", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), true));
        this.inboxEmailList.add(new EmailListModel("1", "Thank You", "abc@gmail.com", "test@gmail.com", "Message Detail", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), true));
        this.inboxEmailList.add(new EmailListModel("1", "Sorry", "abc@gmail.com", "test@gmail.com", "Message Detail", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), true));
        this.inboxEmailList.add(new EmailListModel("1", "Greetings!", "abc@gmail.com", "test@gmail.com", "Message Detail", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), true));
        this.inboxEmailList.add(new EmailListModel("1", "Good Evening", "abc@gmail.com", "test@gmail.com", "Message Detail", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), false));
        this.inboxEmailList.add(new EmailListModel("1", "Hello", "abc@gmail.com", "test@gmail.com", "Message Detail", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), false));
        this.inboxEmailList.add(new EmailListModel("1", "Testing", "abc@gmail.com", "test@gmail.com", "Message Detail", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), true));
        this.inboxEmailList.add(new EmailListModel("1", "WelCome", "abc@gmail.com", "test@gmail.com", "Message Detail", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), true));
        this.inboxEmailList.add(new EmailListModel("1", "Demo Update", "abc@gmail.com", "test@gmail.com", "Message Detail", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), true));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down));
        mAdapter = new EmailItemAdapter(getActivity(), this.inboxEmailList, this);
        recyclerView.setAdapter(this.mAdapter);
        EmailInboxListFragment emailInboxListFragment = this;
        actionModeCallback = new ActionModeCallback();
        recyclerView.addOnScrollListener(new onSrollListener());
        refreshData(this.inboxEmailList.size());
    }

    private void refreshData(int size) {
        if (size > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoEmailFound.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.GONE);
        tvNoEmailFound.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_email_empty_inbox, 0, 0);
        tvNoEmailFound.setText(getResources().getString(R.string.no_new_mail));
        tvNoEmailFound.setVisibility(View.VISIBLE);
    }

    public void onIconClicked(int position) {
        enableActionMode(position);
    }

    public void onMessageRowClicked(int position) {
        if (actionMode != null) {
            actionMode.finish();
            unsetActionMode();
        }
        EmailDetailFragment emailDetailFragment = new EmailDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EmailDetailFragment.BUNDLE_EMAIL_DATA, inboxEmailList.get(position));
        emailDetailFragment.setArguments(bundle);
        ((HomeActivity)getActivity()).replaceFragment(emailDetailFragment);
//        ((MainActivity) getActivity()).pushFragment(((MainActivity)getActivity()).mCurrentTab, emailDetailFragment, true, true);
    }

    public void onRowLongClicked(int position) {
    }

    private void enableActionMode(int position) {
        if (this.actionMode == null) {
            this.selectedItem = position;
            if (((AppCompatActivity) getActivity()) != null) {
                this.actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(this.actionModeCallback);
            }
            setActionMode(actionMode);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        mAdapter.toggleSelection(position);
        int count = mAdapter.getSelectedItemCount();
        if (count == 0) {
            actionMode.finish();
            unsetActionMode();
            selectedItem = -1;
            return;
        }
        actionMode.setTitle(String.valueOf(count));
        actionMode.invalidate();
    }

    private void readMessages() {
        mAdapter.resetAnimationIndex();
        List<Integer> selectedItemPositions = this.mAdapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            mAdapter.setRead((Integer) selectedItemPositions.get(i), !inboxEmailList.get(this.selectedItem).isRead());
        }
        mAdapter.notifyDataSetChanged();
    }

    private void deleteMessages() {
        mAdapter.resetAnimationIndex();
        List<Integer> selectedItemPositions = mAdapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            mAdapter.removeData(selectedItemPositions.get(i));
        }
        mAdapter.notifyDataSetChanged();
        refreshData(mAdapter.getItemCount());
    }


    protected void setActionMode(ActionMode actionMode) {
        actionMode = actionMode;
    }


    protected void unsetActionMode() {
        actionMode = null;
    }

    protected void prepareFabMenu(FloatingActionMenu fabEmailMenu, ArrayList<FloatingActionButton> fabButtons, FloatingActionButton fabCurrentButton) {
        mHandler = new Handler();
//       fabEmailMenu = fabEmailMenu;
        fabEmailMenu.setClosedOnTouchOutside(true);
        Iterator it = fabButtons.iterator();
        while (it.hasNext()) {
            FloatingActionButton fabButton = (FloatingActionButton) it.next();
            fabButton.setOnClickListener(this);
            if (fabButton.equals(fabCurrentButton)) {
                fabCurrentButton.setVisibility(View.GONE);
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabCompose:
                ((HomeActivity) mcontext).replaceFragment(new ComposeFragment());
                return;
            case R.id.fabDraft:
                ((HomeActivity) mcontext).replaceFragment(new EmailDraftListFragment());
                return;
            case R.id.fabInbox:
                ((HomeActivity) mcontext).replaceFragment(new EmailInboxListFragment());
                return;
            case R.id.fabJunk:
                ((HomeActivity) mcontext).replaceFragment(new EmailJunkListFragment());
                return;
            case R.id.fabSent:
                ((HomeActivity) mcontext).replaceFragment(new EmailSentListFragment());
                return;
            case R.id.fabTrash:
                ((HomeActivity) mcontext).replaceFragment(new EmailTrashListFragment());
                return;
            case R.id.fabFolders:
                ((HomeActivity) mcontext).replaceFragment(new EmailFoldersFragment());
                return;
            default:
                return;
        }
    }
/*

    private void closeMenuAndPushFragment(final Fragment mFragment) {
        if (this.actionMode != null) {
            this.actionMode.finish();
        }
        this.fabEmailMenu.close(true);
        this.mHandler.postDelayed(new Runnable() {
            public void run() {

                ((HomeActivity)mcontext).replaceFragment(new mFragment);
//                ((MainActivity) getActivity().mActivity).pushFragment(((MainActivity) EmailBaseFragment.this.mContext).mCurrentTab, mFragment, true, true);
            }
        }, 550);
    }
*/

}
