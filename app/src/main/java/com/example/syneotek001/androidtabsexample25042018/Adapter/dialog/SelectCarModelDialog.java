package com.example.syneotek001.androidtabsexample25042018.Adapter.dialog;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.syneotek001.androidtabsexample25042018.Adapter.CarModelItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnRecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.Collections;

public class SelectCarModelDialog extends AppCompatDialogFragment {
    static final /* synthetic */ boolean $assertionsDisabled = (!SelectCarModelDialog.class.desiredAssertionStatus());
    ArrayList<String> carModelList = new ArrayList();
    CarModelItemAdapter mAdapter;
    private int position;
    public RecyclerView recyclerView;

    class OnItemClick implements OnRecyclerViewItemClickListener {
        OnItemClick() {
        }

        public void onItemClicked(int position) {
//            SelectCarModelDialog.this.getTargetFragment().onActivityResult(SelectCarModelDialog.this.getTargetRequestCode(), -1, new Intent().putExtra("position", position));
            SelectCarModelDialog.this.dismiss();
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_select_car_model, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);

        prepareLayout(getArguments());
        return view;
    }

    private void prepareLayout(Bundle bundle) {
        if (bundle != null) {
            String[] carList = (String[]) bundle.getSerializable("carList");
            if ($assertionsDisabled || carList != null) {
                Collections.addAll(carModelList, carList);
                this.position = bundle.getInt("position");
                setUpRecyclerView();
                return;
            }
            throw new AssertionError();
        }
    }

    private void setUpRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new CarModelItemAdapter(getActivity(), carModelList, new OnItemClick());
        recyclerView.setAdapter(mAdapter);
        mAdapter.setLastSelectedPosition(position);
    }
}
