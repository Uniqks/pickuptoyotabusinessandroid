package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.github.sundeepk.compactcalendarview.domain.Event.EventData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by bluegenie-24 on 24/7/18.
 */

public class AdapterViewTask extends BaseAdapter {
    LayoutInflater inflater;
    ArrayList<EventData> arrTasks;
    Context context;
    TaskClick taskClick;

    public interface TaskClick{
        void onTaskClick(int position);
    }

    public void setTaskClick(TaskClick taskClick){
        this.taskClick = taskClick;
    }


    public AdapterViewTask(Context context, ArrayList<EventData> arrTasks) {
        this.context = context;
        this.arrTasks = arrTasks;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return arrTasks.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.adapter_view_task, parent, false);
        Log.e("AdapterViewTask", "getView " + arrTasks.get(position).getUname());

        SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());

        RelativeLayout rlTaskItem;
        TextView tvEventTime, tvName, tvStatus;
        ImageView ivArrow, ivAvatar;

        rlTaskItem = convertView.findViewById(R.id.rlTaskItem);
        tvEventTime = convertView.findViewById(R.id.tvEventTime);
        tvName = convertView.findViewById(R.id.tvName);
        tvStatus = convertView.findViewById(R.id.tvStatus);
        ivArrow = convertView.findViewById(R.id.ivArrow);
        ivAvatar = convertView.findViewById(R.id.ivAvatar);

        if (arrTasks.get(position).getUname() != null && !arrTasks.get(position).getUname().equals("")) {
            tvName.setText(arrTasks.get(position).getUname());
        }

        if (arrTasks.get(position).getUstatus() != null && !arrTasks.get(position).getUstatus().equals("")) {

            if (arrTasks.get(position).getUstatus().equals("1")) {
                tvStatus.setText(context.getString(R.string.str_task_status_pending));
            } else if (arrTasks.get(position).getUstatus().equals("2")) {
                tvStatus.setText(context.getString(R.string.str_task_status_not_reply));
            } else if (arrTasks.get(position).getUstatus().equals("3")) {
                tvStatus.setText(context.getString(R.string.str_task_status_follow_up));
            } else if (arrTasks.get(position).getUstatus().equals("4")) {
                tvStatus.setText(context.getString(R.string.str_task_status_completed));
            }

        }

        if (arrTasks.get(position).isIs_selected()) {
            rlTaskItem.setBackgroundColor(ContextCompat.getColor(context, R.color.bg_task_selected_list));
            tvStatus.setVisibility(View.VISIBLE);
            ivArrow.setVisibility(View.GONE);
        } else if (!arrTasks.get(position).isIs_selected()) {
            rlTaskItem.setBackgroundColor(ContextCompat.getColor(context, R.color.trans_black_calendar_btm));
            tvStatus.setVisibility(View.GONE);
            ivArrow.setVisibility(View.VISIBLE);
        }

        if (arrTasks.get(position).getUgender()!=null && !arrTasks.get(position).getUgender().equals("")){
            if (arrTasks.get(position).getUgender().equals("male")) {
                ivAvatar.setImageResource(R.drawable.ic_avatar_male);
            } else if (arrTasks.get(position).getUgender().equals("female")) {
                ivAvatar.setImageResource(R.drawable.ic_avatar_female);
            }
        }

        if (arrTasks.get(position).getEventTime()!=null && !arrTasks.get(position).getEventTime().equals("")){
            SimpleDateFormat src = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            SimpleDateFormat dest = new SimpleDateFormat("hh:mm a");
            Date date = null;
            try {
                date = src.parse(arrTasks.get(position).getEventTime());
            } catch (ParseException e) {
                //handle exception
            }
            String result = dest.format(date);
            tvEventTime.setText(result+"");
        }

        rlTaskItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskClick.onTaskClick(position);
            }
        });

        return convertView;
    }
}
