package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.SignatureMainActivity;
import com.example.syneotek001.androidtabsexample25042018.TermsActivity;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentCustomerStatementIndividual4Binding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentCustomerStatementIndividual6Binding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class CustomerStatementIndividualFragment6 extends Fragment implements OnClickListener, OnBackPressed,EasyPermissions.PermissionCallbacks {
    public Button btnSave, btnSaveAndNext;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;
     //FragmentSalesmanProfileStep1Binding binding;
    FragmentCustomerStatementIndividual6Binding binding;
    String name, email, mobile, password, address;
    YoYo yoYo;
    ArrayList<String> arr_salesman_status = new ArrayList<>();

    Typeface typeface;

    String Empsign_path = "";
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_customer_statement_individual6, container, false);
        View view = binding.getRoot();

        typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/montserrat_regular.otf");



        TextPaint tp = new TextPaint();
        tp.linkColor = getResources().getColor(R.color.colorPrimary);           //not quite sure what the format should be
        UnderlineSpan us = new UnderlineSpan();
        us.updateDrawState(tp);
        SpannableString content = new SpannableString(getString(R.string.txt_terms_privacy));
        content.setSpan(us, 0, content.length(), 0);
        binding.tvIncludeTaxes.setText(content);
        binding.tvIncludeTaxes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


				/*Uri uri = Uri.parse(getString(R.string.tv_terms_link));
				Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
				startActivityForResult(goToMarket,1);*/
                Intent i =new Intent(getActivity(), TermsActivity.class);
                startActivity(i);

            }
        });



 /*       btnSave = rootview.findViewById(R.id.btnSave);
        btnSaveAndNext = rootview.findViewById(R.id.btnSaveAndNext);
        etEmail = rootview.findViewById(R.id.etEmail);
        etFirstName = rootview.findViewById(R.id.etFirstName);
        etHomePhone = rootview.findViewById(R.id.etHomePhone);
        etLastName = rootview.findViewById(R.id.etLastName);
        etMobile = rootview.findViewById(R.id.etMobile);
        etTitle = rootview.findViewById(R.id.etTitle);
        etWorkFax = rootview.findViewById(R.id.etWorkFax);
        etWorkPhone = rootview.findViewById(R.id.etWorkPhone);
        ivContact = rootview.findViewById(R.id.ivContact);
        ivEmail = rootview.findViewById(R.id.ivEmail);
        ivMobile = rootview.findViewById(R.id.ivMobile);
        ivTitle = rootview.findViewById(R.id.ivTitle);
        tvPageIndicator = rootview.findViewById(R.id.tvPageIndicator);
        tvTitle = rootview.findViewById(R.id.tvTitle);
        ivBack = rootview.findViewById(R.id.ivBack);*/
        binding.ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });

        binding.rbNew.setTypeface(typeface);
        binding.rbUsed.setTypeface(typeface);


        setHasOptionsMenu(false);
        setClickEvents();

        setAnimation(binding.rbNew);
        setAnimation(binding.rbUsed);

        setAnimation(binding.lblCapitalCost);
        setAnimation(binding.lblChargesCarriedForward);
        setAnimation(binding.lblDownPayment);
        setAnimation(binding.lblEstPayment);
        setAnimation(binding.lblLeaseEndValue);
        setAnimation(binding.lblLienAmount);
        setAnimation(binding.lblLoanAmount);
        setAnimation(binding.lblMake);
        setAnimation(binding.lblModel);
        setAnimation(binding.lblMSRP);
        setAnimation(binding.lblNoOfPayments);
        setAnimation(binding.lblOtherSource);
        setAnimation(binding.lblRate);
        setAnimation(binding.lblSecurityDeposit);
        setAnimation(binding.lblSellingPrice);
        setAnimation(binding.lblSuffixCode);
        setAnimation(binding.lblTCCILeaseNo);
        setAnimation(binding.lblTerm);
        setAnimation(binding.lblTradeInAllowance);
        setAnimation(binding.lblTradeInMake);
        setAnimation(binding.lblTradeInYear);
        setAnimation(binding.lblVehicleData);
        setAnimation(binding.lblVehicleType);
        setAnimation(binding.lblYear);

        setAnimation(binding.etCapitalCost);
        setAnimation(binding.etChargesCarriedForward);
        setAnimation(binding.etDownPayment);
        setAnimation(binding.etEstPayment);
        setAnimation(binding.etLeaseEndValue);
        setAnimation(binding.etLienAmount);
        setAnimation(binding.etLoanAmount);
        setAnimation(binding.etMake);
        setAnimation(binding.etModel);
        setAnimation(binding.etMSRP);
        setAnimation(binding.etNoOfPayments);
        setAnimation(binding.etOtherSource);
        setAnimation(binding.etRate);
        setAnimation(binding.etSecurityDeposit);
        setAnimation(binding.etSellingPrice);
        setAnimation(binding.etSuffixCode);
        setAnimation(binding.etTCCILeaseNo);
        setAnimation(binding.etTerm);
        setAnimation(binding.etTradeInAllowance);
        setAnimation(binding.etTradeInMake);
        setAnimation(binding.etTradeInYear);
        setAnimation(binding.etYear);

        arr_salesman_status = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_salesman_status)));


        binding.llSign.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {

                    if (EasyPermissions.hasPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                        startActivityForResult(i, 11);
                        Log.e("imgsos", "Clicked");
                        Log.e("1", "1");

                    } else {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_EXTERNAL_STORAGE);
                        Log.e("2", "2");
                    }

                } else {


                    /*Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    startActivityForResult(intent, PICK_SMS);*/
                    Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                    startActivityForResult(i, 11);
                    Log.e("imgsos", "Clicked");
                    Log.e("3", "3");

                }

            }
        });



        binding.llrightSign.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {

                    if (EasyPermissions.hasPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                        startActivityForResult(i, 12);
                        Log.e("imgsos", "Clicked");
                        Log.e("1", "1");

                    } else {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_EXTERNAL_STORAGE);
                        Log.e("2", "2");
                    }

                } else {


                    /*Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    startActivityForResult(intent, PICK_SMS);*/
                    Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                    startActivityForResult(i, 12);
                    Log.e("imgsos", "Clicked");
                    Log.e("3", "3");

                }

            }
        });


        return view;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(" onActivityResult ", "requestCode " + requestCode+" resultCode "+resultCode);
        if (requestCode == 11 && resultCode == Activity.RESULT_OK) {
            Empsign_path = data.getStringExtra("path");
            Bitmap bmp = BitmapFactory.decodeFile(Empsign_path);
            ImageView img;
            binding.signaturePad.setImageBitmap(bmp);
//            Picasso.with(getActivity()).load(Empsign_path).into(binding.signaturePad);

            Log.e(" onActivityResult ", "Empsign_path " + Empsign_path);

        }
        else   if (requestCode == 12 && resultCode == Activity.RESULT_OK) {
            Empsign_path = data.getStringExtra("path");
            Bitmap bmp = BitmapFactory.decodeFile(Empsign_path);
            ImageView img;
            binding.signaturePad2.setImageBitmap(bmp);
            Log.e(" ", "Empsign_path2 " + Empsign_path);

        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }



    public void setAnimation(View view) {
        YoYo.with(Techniques.FlipInX).duration(1000).delay(150).repeat(0).playOn(view);
    }

    private void setClickEvents() {
        binding.btnSaveAndNext.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSaveAndNext:
                Log.e("btnSaveAndNext","btnSaveAndNext");
                CustomerStatementIndividualFragment5 billSaleFragment2 = new CustomerStatementIndividualFragment5();
//                 ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
              /*  if (getArguments() != null) {
                    if (getArguments().getBoolean("is_edit")) {
                        if (getArguments().getSerializable("salesman_profile") != null) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("salesman_profile", getArguments().getSerializable("salesman_profile"));
                            bundle.putBoolean("is_edit", true);
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    } else {
                        if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("is_edit", false);
                            bundle.putString("profile_type",getArguments().getString("profile_type"));
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    }
                }*/
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
