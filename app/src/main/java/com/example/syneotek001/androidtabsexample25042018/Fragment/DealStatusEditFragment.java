package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.Adapter.DealDescriptionMessageAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.RequestSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.GlideUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.ChatModel;
import com.example.syneotek001.androidtabsexample25042018.model.DealRequestDetailResponse;
import com.example.syneotek001.androidtabsexample25042018.model.DealRequestDetailResponse.DealRequestMessagesData;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse.SalesmanDealRequestListData.RequestListData;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiManager;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiResponseInterface;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;

import java.util.ArrayList;
import java.util.Arrays;

public class DealStatusEditFragment extends Fragment implements OnClickListener, OnBackPressed {

    public ImageView ivBack;
    Spinner spinnerStatus;

    ArrayList<String> arr_status = new ArrayList<>();

    TextView tvLblAttachment;
    Button btnSubmit,btnChooseFile;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_deal_status_edit, container, false);

        ImageView ivBack=mview.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        spinnerStatus = mview.findViewById(R.id.spinnerStatus);

        tvLblAttachment = mview.findViewById(R.id.tvLblAttachment);
        btnSubmit = mview.findViewById(R.id.btnSubmit);
        btnChooseFile = mview.findViewById(R.id.btnChooseFile);

        arr_status = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_status)));

        initCustomSpinner(arr_status);

        btnSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        if (getArguments()!=null) {
            if (getArguments().getString("deal_type") !=null && !getArguments().getString("deal_type").equals("")) {
                if (arr_status.size() > 0) {
                    for (int i=0;i<arr_status.size();i++) {
                        if (arr_status.get(i).equals(getString(R.string.str_new)))
                            spinnerStatus.setSelection(0);
                        else if (!arr_status.get(i).equals(getString(R.string.str_new)))
                            if (arr_status.get(i).equals(getArguments().getString("deal_type")))
                            spinnerStatus.setSelection(i);
                    }
                }
                if (!getArguments().getString("deal_type").equals(getString(R.string.str_ready_for_delivery))) {
                    btnChooseFile.setVisibility(View.GONE);
                    tvLblAttachment.setVisibility(View.GONE);
                }
            }
        }

        return mview;
    }

    private void initCustomSpinner(ArrayList<String> countries) {

        RequestSpinnerAdapter customSpinnerAdapter = new RequestSpinnerAdapter(getActivity(), countries);
        spinnerStatus.setAdapter(customSpinnerAdapter);
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    public void onClick(View view) {

    }
}
