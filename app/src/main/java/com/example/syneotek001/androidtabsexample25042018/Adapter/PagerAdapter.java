package com.example.syneotek001.androidtabsexample25042018.Adapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.Fragment.TabFragment1;
import com.example.syneotek001.androidtabsexample25042018.Fragment.TabFragment2;
import com.example.syneotek001.androidtabsexample25042018.Fragment.TabFragment3;
public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TabFragment1 tab1 = new TabFragment1();
                return tab1;

            case 1:
                TabFragment2 tab2 = new TabFragment2();
                return tab2;
            case 2:
                TabFragment3 tab3 = new TabFragment3();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

/* set the title postion and display its  Tab name workin code'*/
    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        switch (position) {
            case 0:
                title = "TAB 1";
                break;
            case 1:
                title = "TAB 2";
                break;
            case 2:
                title = "TAB 3";
                break;
        }
        return title;
    }
}

