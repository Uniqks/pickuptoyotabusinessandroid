package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentPurchaser2Binding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentPurchaserBinding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class PurchaserFragment2 extends Fragment implements OnClickListener, OnBackPressed {
    public Button btnSave, btnSaveAndNext;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;
     //FragmentSalesmanProfileStep1Binding binding;
    FragmentPurchaser2Binding binding;
    String name, email, mobile, password, address;
    YoYo yoYo;
    ArrayList<String> arr_salesman_status = new ArrayList<>();

    Typeface typeface;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_purchaser2, container, false);
        View view = binding.getRoot();

        typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/montserrat_regular.otf");

 /*       btnSave = rootview.findViewById(R.id.btnSave);
        btnSaveAndNext = rootview.findViewById(R.id.btnSaveAndNext);
        etEmail = rootview.findViewById(R.id.etEmail);
        etFirstName = rootview.findViewById(R.id.etFirstName);
        etHomePhone = rootview.findViewById(R.id.etHomePhone);
        etLastName = rootview.findViewById(R.id.etLastName);
        etMobile = rootview.findViewById(R.id.etMobile);
        etTitle = rootview.findViewById(R.id.etTitle);
        etWorkFax = rootview.findViewById(R.id.etWorkFax);
        etWorkPhone = rootview.findViewById(R.id.etWorkPhone);
        ivContact = rootview.findViewById(R.id.ivContact);
        ivEmail = rootview.findViewById(R.id.ivEmail);
        ivMobile = rootview.findViewById(R.id.ivMobile);
        ivTitle = rootview.findViewById(R.id.ivTitle);
        tvPageIndicator = rootview.findViewById(R.id.tvPageIndicator);
        tvTitle = rootview.findViewById(R.id.tvTitle);
        ivBack = rootview.findViewById(R.id.ivBack);*/
        binding.ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });



        binding.llStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    /**
                     * Called when the user is done setting a new time and the dialog has
                     * closed.
                     *
                     * @param view      the view associated with this listener
                     * @param hourOfDay the hour that was set
                     * @param minute    the minute that was set
                     */
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String AM_PM = " AM";
                        if (hourOfDay >= 12) {
                            AM_PM = " PM";
                            if (hourOfDay >= 13 && hourOfDay < 24) {
                                hourOfDay -= 12;
                            } else {
                                hourOfDay = 12;
                            }
                        } else if (hourOfDay == 0) {
                            hourOfDay = 12;
                        }
                        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
                        String minuteString = minute < 10 ? "0" + minute : "" + minute;
                      binding.txtStartTime.setText(hourString + ":" + minuteString + " " + AM_PM);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        binding.llendTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    /**
                     * Called when the user is done setting a new time and the dialog has
                     * closed.
                     *
                     * @param view      the view associated with this listener
                     * @param hourOfDay the hour that was set
                     * @param minute    the minute that was set
                     */
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String AM_PM = " AM";
                        if (hourOfDay >= 12) {
                            AM_PM = " PM";
                            if (hourOfDay >= 13 && hourOfDay < 24) {
                                hourOfDay -= 12;
                            } else {
                                hourOfDay = 12;
                            }
                        } else if (hourOfDay == 0) {
                            hourOfDay = 12;
                        }
                        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
                        String minuteString = minute < 10 ? "0" + minute : "" + minute;
                       binding.txtendTime.setText(hourString + ":" + minuteString + " " + AM_PM);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

      /*  binding.rbIndividual.setTypeface(typeface);
        binding.rbLease.setTypeface(typeface);
        binding.rbPurchase.setTypeface(typeface);
        binding.rbWithCoSigner.setTypeface(typeface);
        binding.rbWithSpouse.setTypeface(typeface);
*/
       binding.llStartDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int year;
                int month;
                int day;
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                monthOfYear = monthOfYear + 1;
                                String monthString = monthOfYear < 10 ? "0" + monthOfYear : "" + monthOfYear;
                                String dayString = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
                                binding.txtStartDate.setText(year + "-" + monthString + "-" + dayString);
//                                txtStartDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                                dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            }
                        }, year, month, day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });


        setHasOptionsMenu(false);
        setClickEvents();

        setAnimation(binding.lblStockno);
        setAnimation(binding.lblModel);
        setAnimation(binding.lblMake);
        setAnimation(binding.lblTrim);
        setAnimation(binding.llStartDate);

        setAnimation(binding.lblBodyType);
        setAnimation(binding.lblColor);
        setAnimation(binding.lblLicNo);
        setAnimation(binding.lblVin);

        setAnimation(binding.lblManufacture);
        setAnimation(binding.lblDriverLicNo);
        setAnimation(binding.lblDistanceTravelled);
        setAnimation(binding.lblOnDelivery);
        setAnimation(binding.lblPurchseIntial);

        arr_salesman_status = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_salesman_status)));


        return view;
    }



    public void setAnimation(View view) {
        YoYo.with(Techniques.FlipInX).duration(1000).delay(150).repeat(0).playOn(view);
    }

    private void setClickEvents() {
        binding.btnSaveAndNext.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSaveAndNext:
                Log.e("btnSaveAndNext","btnSaveAndNext");
                PurchaserFragment3 billSaleFragment2 = new PurchaserFragment3();
                 ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
              /*  if (getArguments() != null) {
                    if (getArguments().getBoolean("is_edit")) {
                        if (getArguments().getSerializable("salesman_profile") != null) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("salesman_profile", getArguments().getSerializable("salesman_profile"));
                            bundle.putBoolean("is_edit", true);
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    } else {
                        if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("is_edit", false);
                            bundle.putString("profile_type",getArguments().getString("profile_type"));
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    }
                }*/
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
