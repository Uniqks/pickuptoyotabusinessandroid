package com.example.syneotek001.androidtabsexample25042018.model;

import android.graphics.drawable.Drawable;

import com.github.mikephil.charting.data.Entry;

import java.io.Serializable;
import java.util.List;

public class LineGraphDataModel implements Serializable {
    private int colorOpacity50;
    private int colorOpacity80;
    private Drawable fadeDrawable;
    private String title = "";
    private List<Entry> valueList;

    public LineGraphDataModel(String title, int colorOpacity80, Drawable fadeDrawable, int colorOpacity50, List<Entry> valueList) {
        this.title = title;
        this.colorOpacity80 = colorOpacity80;
        this.fadeDrawable = fadeDrawable;
        this.colorOpacity50 = colorOpacity50;
        this.valueList = valueList;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getColorOpacity80() {
        return this.colorOpacity80;
    }

    public void setColorOpacity80(int colorOpacity80) {
        this.colorOpacity80 = colorOpacity80;
    }

    public Drawable getFadeDrawable() {
        return this.fadeDrawable;
    }

    public void setFadeDrawable(Drawable fadeDrawable) {
        this.fadeDrawable = fadeDrawable;
    }

    public int getColorOpacity50() {
        return this.colorOpacity50;
    }

    public void setColorOpacity50(int colorOpacity50) {
        this.colorOpacity50 = colorOpacity50;
    }

    public List<Entry> getValueList() {
        return this.valueList;
    }

    public void setValueList(List<Entry> valueList) {
        this.valueList = valueList;
    }

    public String toString() {
        return "LineGraphDataModel{title='" + this.title + '\'' + ", colorOpacity80=" + this.colorOpacity80 + ", fadeDrawable=" + this.fadeDrawable + ", colorOpacity50=" + this.colorOpacity50 + ", valueList=" + this.valueList + '}';
    }
}
