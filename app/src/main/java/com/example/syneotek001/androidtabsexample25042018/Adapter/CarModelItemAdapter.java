package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnRecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class CarModelItemAdapter extends RecyclerView.Adapter<CarModelItemAdapter.MyViewHolder> {
    private int lastSelectedPosition = -1;
    private Context mContext;
    private List<String> mData = new ArrayList();
    private OnRecyclerViewItemClickListener onItemClickListener;

    class MyViewHolder extends ViewHolder {
        TextView tvCarModelName;

        MyViewHolder(View view) {
            super(view);
            tvCarModelName = view.findViewById(R.id.tvCarModelName);

        }
    }

    public CarModelItemAdapter(Context context, ArrayList<String> list, OnRecyclerViewItemClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    public CarModelItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dialog_item_car_model, parent, false);

        return new CarModelItemAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.tvCarModelName.setText((CharSequence) this.mData.get(position));
        if (this.lastSelectedPosition == position) {
            holder.tvCarModelName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_check_black, 0);
        } else {
            holder.tvCarModelName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        holder.tvCarModelName.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClicked(position);
                    lastSelectedPosition = position;
                    notifyDataSetChanged();
                }
            }
        });
    }

    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        lastSelectedPosition = position;
    }
}
