package com.example.syneotek001.androidtabsexample25042018.model;

public class AssignmentDealRequestItemModel {



    private String customerID = "";
    private String dealRequests = "";
    private String salesManName = "";
    private String date = "";
    private boolean isSelected = false;

    public AssignmentDealRequestItemModel(String customerID, String dealRequests, String salesManName, String date) {
        this.customerID = customerID;
        this.dealRequests = dealRequests;
        this.salesManName = salesManName;
        this.date = date;
        this.isSelected = false;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getDealRequests() {
        return dealRequests;
    }

    public void setDealRequests(String dealRequests) {
        this.dealRequests = dealRequests;
    }

    public String getSalesManName() {
        return salesManName;
    }

    public void setSalesManName(String salesManName) {
        this.salesManName = salesManName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
