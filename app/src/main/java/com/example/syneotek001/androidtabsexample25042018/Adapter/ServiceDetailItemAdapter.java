package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.LeadItemModel;

import java.util.ArrayList;
import java.util.Iterator;

//import com.example.syneotek001.androidtabsexample25042018.Fragment.AddTaskFragment;
//import com.example.syneotek001.androidtabsexample25042018.Fragment.TaskListFragment;

public class ServiceDetailItemAdapter extends RecyclerView.Adapter<ServiceDetailItemAdapter.MyViewHolder> implements OnClickListener {
    private int lastSelectedPosition = 1;
    private Context mContext;
    private ArrayList<LeadItemModel> mData;
    private OnLeadItemViewClickListener onItemClickListener;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivInfo:
                if (ServiceDetailItemAdapter.this.onItemClickListener != null) {
                    ServiceDetailItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivLeadLabel:
                if (ServiceDetailItemAdapter.this.onItemClickListener != null) {
                    ServiceDetailItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivAccept:
                if (ServiceDetailItemAdapter.this.onItemClickListener != null) {
                    ServiceDetailItemAdapter.this.onItemClickListener.onLeadEmailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivSchedule:
                if (ServiceDetailItemAdapter.this.onItemClickListener != null) {
                    ServiceDetailItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            default:
                return;
        }
    }

    class MyViewHolder extends ViewHolder implements OnClickListener {

        CardView cvLead;
        ImageView ivAccept, ivReject, ivInfo, ivLeadLabel;
        LinearLayout llActionItems;
        LinearLayout /*ll_LastActivityDate, ll_Email,*/ lyAssignDate, ll_SourceLink;
        TextView tvAssignDate, tvCustomerId, tvEmail, tvLastActivityDate, tvName, tvPhone, tvSourceLink;
        View viewDummySpace;


        MyViewHolder(View view) {
            super(view);
            cvLead = view.findViewById(R.id.cvLead);
            ivAccept = view.findViewById(R.id.ivAccept);
            ivReject = view.findViewById(R.id.ivReject);
            ivInfo = view.findViewById(R.id.ivInfo);
            ivLeadLabel = view.findViewById(R.id.ivLeadLabel);
            llActionItems = view.findViewById(R.id.llActionItems);
            tvAssignDate = view.findViewById(R.id.tvAssignDate);
            tvCustomerId = view.findViewById(R.id.tvCustomerId);
            tvEmail = view.findViewById(R.id.tvEmail);
            tvLastActivityDate = view.findViewById(R.id.tvLastActivityDate);
            tvName = view.findViewById(R.id.tvName);
            tvPhone = view.findViewById(R.id.tvPhone);
            tvSourceLink = view.findViewById(R.id.tvSourceLink);
            viewDummySpace = view.findViewById(R.id.viewDummySpace);

            /*ll_LastActivityDate = view.findViewById(R.id.ll_LastActivityDate);
            ll_Email = view.findViewById(R.id.ll_Email);*/
            lyAssignDate = view.findViewById(R.id.lyAssignDate);
            ll_SourceLink = view.findViewById(R.id.ll_SourceLink);


        }
        @Override
        public void onClick(View v) {

        }
    }

    public ServiceDetailItemAdapter(Context context, ArrayList<LeadItemModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }



    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_service_detail, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        LeadItemModel mLeadItem = (LeadItemModel) mData.get(position);
        holder.tvCustomerId.setText(mLeadItem.getCustomerId());
        holder.tvAssignDate.setText(TimeStamp.millisToTimeFormat(mLeadItem.getAssignDate()));
        holder.tvLastActivityDate.setText(TimeStamp.millisToTimeFormat(mLeadItem.getLastActivity()));
        holder.tvName.setText(mLeadItem.getClientName());
        holder.tvEmail.setText(mLeadItem.getEmail());
        holder.tvPhone.setText(mLeadItem.getPhone());
        holder.tvSourceLink.setText(mLeadItem.getSourceLink());
        String leadLabel = mLeadItem.getLeadLabel();
        int i = -1;
        switch (leadLabel.hashCode()) {
            case 49:
                if (leadLabel.equals("1")) {
                    i = 0;
                    break;
                }
                break;
            case 50:
                if (leadLabel.equals("2")) {
                    i = 1;
                    break;
                }
                break;
            case 51:
                if (leadLabel.equals("3")) {
                    i = 2;
                    break;
                }
                break;
            case 52:
                if (leadLabel.equals("4")) {
                    i = 3;
                    break;
                }
                break;
            case 53:
                if (leadLabel.equals("5")) {
                    i = 4;
                    break;
                }
                break;
        }
        switch (i) {
            case 0:
                holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_hot);
                break;
            case 1:
                holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_warm);
                break;
            case 2:
                holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_cold);
                break;
            case 3:
                holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_dead);
                break;
            case 4:
                holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_closed);
                break;
        }
        Iterator it;
        ArrayList<View> viewList;
        if (mLeadItem.isSelected()) {
            viewList = new ArrayList<>();
            /*viewList.add(holder.ll_LastActivityDate);
            viewList.add(holder.ll_Email);*/
            viewList.add(holder.lyAssignDate);
            viewList.add((holder.ll_SourceLink));
            viewList.add(holder.viewDummySpace);
            viewList.add(holder.llActionItems);
            it = viewList.iterator();

            while (it.hasNext()) {
                View view = (View) it.next();
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0.0f);
                view.animate().alpha(1.0f).setListener(null);
            }
            return;
        }
        viewList = new ArrayList<>();
        /*viewList.add(holder.ll_LastActivityDate);
        viewList.add(holder.ll_Email);*/
        viewList.add(holder.lyAssignDate);
        viewList.add(holder.ll_SourceLink);
        viewList.add(holder.viewDummySpace);
        viewList.add(holder.llActionItems);
        it = viewList.iterator();
        while (it.hasNext()) {
            final View view = (View) it.next();
            view.animate().alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            });
        }

        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    LeadItemModel mLeadItem = (LeadItemModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);
                }
            }
        });




        holder.ivLeadLabel.setOnClickListener(this);
        holder.ivInfo.setOnClickListener(this);
        holder.ivAccept.setOnClickListener(this);
        holder.ivReject.setOnClickListener(this);






    }


    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
