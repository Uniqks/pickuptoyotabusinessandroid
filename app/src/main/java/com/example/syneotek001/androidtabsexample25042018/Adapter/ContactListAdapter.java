package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.Fragment.ContactDetailFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.DealListInfoDetailsFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.MainActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.HeaderItemDecoration;
import com.example.syneotek001.androidtabsexample25042018.Utils.RecyclerViewFastScroller;
import com.example.syneotek001.androidtabsexample25042018.model.ContactListModel;

import java.util.ArrayList;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> implements RecyclerViewFastScroller.BubbleTextGetter, HeaderItemDecoration.StickyHeaderInterface {
    private Context mContext;
    private final ArrayList<ContactListModel> mData;

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvEmail, tvPhone, tvLetter;

        private ViewHolder(final View mBinder) {
            super(mBinder);
            tvName = mBinder.findViewById(R.id.tvName);
            tvEmail = mBinder.findViewById(R.id.tvEmail);
            tvPhone = mBinder.findViewById(R.id.tvPhone);
            tvLetter = mBinder.findViewById(R.id.tvLetter);
        }
    }

    public ContactListAdapter(Context context, ArrayList<ContactListModel> contactList) {
        this.mContext = context;
        this.mData = contactList;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int i = 0;
        ContactListModel mContact = mData.get(position);
        holder.tvName.setText(String.format("%s %s", new Object[]{mContact.getFirstName(), mContact.getLastName()}));
        holder.tvEmail.setText(mContact.getEmail());
        holder.tvPhone.setText(mContact.getPhone());
        holder.tvLetter.setText(mContact.getLetter());
        TextView textView = holder.tvLetter;
        if (!mContact.isHeader()) {
            i = 4;
        }
        textView.setVisibility(i);


        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ((HomeActivity) mContext).replaceFragment(new ContactDetailFragment());

            }
        });
    }

    public String getTextToShowInBubble(int pos) {
        return mData.get(pos).getLetter();
    }

    public int getItemCount() {
        return mData.size();
    }

    public int getHeaderPositionForItem(int itemPosition) {
        while (!mData.get(itemPosition).isHeader()) {
            itemPosition--;
            if (itemPosition < 0) {
                return 0;
            }
        }
        isHeader(itemPosition);
        return itemPosition;
    }

    public int getHeaderLayout(int headerPosition) {
        return R.layout.item_contact_header;
    }

    public void bindHeaderData(View header, int headerPosition) {
        ((TextView) header.findViewById(R.id.tvLetter)).setText(mData.get(headerPosition).getLetter());
    }

    public boolean isHeader(int itemPosition) {
        return mData.get(itemPosition).isHeader();
    }

    public void headerClickListener(View header, int headerPosition) {
    }
}
