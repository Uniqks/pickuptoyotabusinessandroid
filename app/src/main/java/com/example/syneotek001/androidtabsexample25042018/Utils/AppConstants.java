package com.example.syneotek001.androidtabsexample25042018.Utils;

public class AppConstants {
    public static final String TAB_ANALYTIC = "tab_analytic";
    public static final String TAB_MESSAGE = "tab_message";
    public static final String TAB_PROFILE = "tab_profile";
    public static final String TAB_SETTINGS = "tab_settings";
    public static final String TAB_USER = "tab_user";
}
