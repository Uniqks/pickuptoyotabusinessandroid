package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.ContactIndex;

import java.util.ArrayList;

/**
 * Created by bluegenie-24 on 19/7/18.
 */

public class AdapterEmailTemplateItems extends BaseAdapter {
    LayoutInflater inflater;
    String [] arrTitle;
    //    int [] arrImages;
    TypedArray arrImages;
    Context context;
    ItemClick itemClick;
    public interface ItemClick {
        public void onItemClick(int position);
    }

    public void setItemClick(ItemClick itemClick){
        this.itemClick = itemClick;
    }

    public AdapterEmailTemplateItems(Context context, String [] arrTitle,/*int [] arrImages*/TypedArray arrImages){
        this.context = context;
        this.arrTitle = arrTitle;
        this.arrImages = arrImages;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return arrTitle.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.adapter_email_template_items, parent, false);
        LinearLayout lyEmailTemplateItem;
        AppCompatImageView ivEmailTemplateItem;
        TextView tvEmailTemplateItem;
        lyEmailTemplateItem = convertView.findViewById(R.id.lyEmailTemplateItem);
        ivEmailTemplateItem = convertView.findViewById(R.id.ivEmailTemplateItem);
        tvEmailTemplateItem = convertView.findViewById(R.id.tvEmailTemplateItem);

        tvEmailTemplateItem.setText(arrTitle[position]);

//        ivEmailTemplateItem.setImageResource(arrImages[position]);
        ivEmailTemplateItem.setImageResource(arrImages.getResourceId(position,0));

        lyEmailTemplateItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onItemClick(position);
            }
        });

        return convertView;
    }
}
