package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.CreateDealFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.DealRequestsModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DealRequestsItemAdapter extends Adapter<DealRequestsItemAdapter.MyViewHolder> {
    private Context mContext;
    private List<DealRequestsModel> mData = new ArrayList();
    private OnLeadItemViewClickListener onItemClickListener;

    class MyViewHolder extends ViewHolder {

        TextView tvCustomerId, tvLabel, tvName, tvDealId,tvInfo,tvInquiry,tvStatus;
        LinearLayout lyActionItems;

        MyViewHolder(View view) {
            super(view);
            tvStatus = view.findViewById(R.id.tvStatus);
            tvCustomerId = view.findViewById(R.id.tvCustomerId);
            tvLabel = view.findViewById(R.id.tvLabel);
            tvName = view.findViewById(R.id.tvName);
            tvDealId = view.findViewById(R.id.tvDealId);
            lyActionItems = view.findViewById(R.id.lyActionItems);
            tvInfo = view.findViewById(R.id.tvInfo);
            tvInquiry = view.findViewById(R.id.tvInquiry);
        }
    }

    public DealRequestsItemAdapter(Context context, ArrayList<DealRequestsModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    public DealRequestsItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_deal_requests, parent, false);

        return new DealRequestsItemAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        DealRequestsModel mDealRequestModel = (DealRequestsModel) mData.get(position);
        holder.tvDealId.setText(mDealRequestModel.getDealId());
        holder.tvCustomerId.setText(mDealRequestModel.getCustomerId());
        holder.tvName.setText(mDealRequestModel.getSalesManName());
        holder.tvLabel.setText(mDealRequestModel.getLabel());
        holder.tvStatus.setText(mDealRequestModel.getStatus());

        holder.tvInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onLeadDetailIconClicked(position);
            }
        });

        holder.tvInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onLeadEmailIconClicked(position);
            }
        });

        Iterator it;
        ArrayList<View> viewList;
        if (mDealRequestModel.isSelected()) {
            viewList = new ArrayList<>();
            viewList.add(holder.tvStatus);
            viewList.add(holder.tvCustomerId);
            viewList.add(holder.tvLabel);
            viewList.add((holder.lyActionItems));
            it = viewList.iterator();

            while (it.hasNext()) {
                View view = (View) it.next();
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0.0f);
                view.animate().alpha(1.0f).setListener(null);
            }
            return;
        }
        viewList = new ArrayList<>();
        viewList.add(holder.tvStatus);
        viewList.add(holder.tvCustomerId);
        viewList.add(holder.tvLabel);
        viewList.add(holder.lyActionItems);
        it = viewList.iterator();
        while (it.hasNext()) {
            final View view = (View) it.next();
            view.animate().alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            });
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    DealRequestsModel mLeadItem = (DealRequestsModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);
                }
            }
        });

    }

    public int getItemCount() {
        return mData.size();
    }
}
