package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.DealManagerItemModel;

import java.util.ArrayList;
import java.util.Iterator;

public class DealManagerItemAdapter extends RecyclerView.Adapter<DealManagerItemAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<DealManagerItemModel> mData = new ArrayList();
    private OnLeadItemViewClickListener onItemClickListener;

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivAllocate, ivDescription;
        TextView tvCustomerId, tvLabel, tvName, tvDealId,tvInfo,tvInquiry;
        Button btnStatusFollowUp;
        LinearLayout lyActionItems;

        MyViewHolder(View view) {
            super(view);
            ivAllocate = view.findViewById(R.id.ivAllocate);
            ivDescription = view.findViewById(R.id.ivDescription);
            tvCustomerId = view.findViewById(R.id.tvCustomerId);
            tvLabel = view.findViewById(R.id.tvLabel);
            tvName = view.findViewById(R.id.tvName);
            tvDealId = view.findViewById(R.id.tvDealId);
            btnStatusFollowUp = view.findViewById(R.id.btnStatusFollowUp);
            lyActionItems = view.findViewById(R.id.lyActionItems);
            tvInfo = view.findViewById(R.id.tvInfo);
            tvInquiry = view.findViewById(R.id.tvInquiry);
        }
    }

    public DealManagerItemAdapter(Context context, ArrayList<DealManagerItemModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_deal_manager, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        DealManagerItemModel mDealManagerItem = mData.get(position);
        holder.tvDealId.setText(mDealManagerItem.getDealId());
        holder.tvCustomerId.setText(mDealManagerItem.getCustomerId());
        holder.tvName.setText(mDealManagerItem.getSalesmanName());
        holder.tvLabel.setText(mDealManagerItem.getLabel());
        holder.btnStatusFollowUp.setText(mDealManagerItem.getDealType());

        holder.tvInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onLeadDetailIconClicked(position);
            }
        });

        holder.tvInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onLeadEmailIconClicked(position);
            }
        });

        holder.btnStatusFollowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onLeadDealRequestClicked(position);
            }
        });

        Iterator it;
        ArrayList<View> viewList;
        if (mDealManagerItem.isSelected()) {
            viewList = new ArrayList<>();
            viewList.add(holder.btnStatusFollowUp);
            viewList.add(holder.tvCustomerId);
            viewList.add(holder.tvLabel);
            viewList.add((holder.lyActionItems));
            it = viewList.iterator();

            while (it.hasNext()) {
                View view = (View) it.next();
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0.0f);
                view.animate().alpha(1.0f).setListener(null);
            }
            return;
        }
        viewList = new ArrayList<>();
        viewList.add(holder.btnStatusFollowUp);
        viewList.add(holder.tvCustomerId);
        viewList.add(holder.tvLabel);
        viewList.add(holder.lyActionItems);
        it = viewList.iterator();
        while (it.hasNext()) {
            final View view = (View) it.next();
            view.animate().alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            });
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    DealManagerItemModel mLeadItem = (DealManagerItemModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);
                }
            }
        });

    }


    public int getItemCount() {
        return mData.size();
    }
}
