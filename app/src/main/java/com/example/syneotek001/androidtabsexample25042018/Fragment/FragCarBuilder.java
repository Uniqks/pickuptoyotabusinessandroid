package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.FragmentTabsPagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.dialog.SelectCarModelDialog;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

public class FragCarBuilder extends Fragment implements OnBackPressed {
    ImageView iv_card_builder_setting,backtool,iv_CarFilter;
    FloatingActionButton fabAddCarBuilder;
    String[] carList = new String[]{"Camry", "Corolla", "Corolla iM", "Yaris", "Yaris Sedan", "86", "Prius", "Prius V", "Avalon", "Sienna", "C-HR", "RAV4", "Sequoia", "Highlander", "Tacoma", "4Runner", "Tundra", "Prius C"};
    CarBuilderPagerAdapter adapter;
    ViewPager viewPager;
    TabLayout tabLayout;
    FragmentTabsPagerAdapter mAdapter;
    Button btnSubmit;
    /*public FragCarBuilder() {
        // Required empty public constructor
    }*/

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragbuilder, container, false);


        if(savedInstanceState!=null) {
            Log.v("---------------->","restored!");
          /*  adapter = new CarBuilderPagerAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapter);
            adapter.notifyDataSetChanged();*/
        }

        btnSubmit = view.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).replaceFragment(new LeaseFinanceFragment());
            }
        });

        Log.e("FragCarBuilder","onCreateView");

        tabLayout = (TabLayout)view.findViewById(R.id.tab_layout);
        viewPager = (ViewPager)view.findViewById(R.id.pager);

        setupViewPager(viewPager);

        fabAddCarBuilder = view.findViewById(R.id.fabCarExteriorInterior);
        fabAddCarBuilder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("car_model_name",carList[viewPager.getCurrentItem()]);
                CarExteriorInteriorListHolderFragment fragment = new CarExteriorInteriorListHolderFragment();
                fragment.setArguments(bundle);
                ((HomeActivity)getActivity()).replaceFragment(fragment);
            }
        });
        backtool = view.findViewById(R.id.backtool);

        backtool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        iv_CarFilter=(ImageView)view.findViewById(R.id.iv_CarFilter);
        iv_CarFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectCarModelDialog dialog = new SelectCarModelDialog();
                Bundle args = new Bundle();
                args.putSerializable("carList", carList);
                args.putInt("position", viewPager.getCurrentItem());
                dialog.setArguments(args);
//                dialog.setTargetFragment(this, 1);
                dialog.show(getFragmentManager().beginTransaction(), SelectCarModelDialog.class.getSimpleName());
            }
        });


        iv_card_builder_setting=(ImageView)view.findViewById(R.id.iv_card_builder_setting);
        iv_card_builder_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                window.setGravity(Gravity.CENTER);
                dialog.setContentView(R.layout.car_builder_setting_layout);

                TextView tvLeaseFinance,tvCarDeals;

                tvLeaseFinance = dialog.findViewById(R.id.tvLeaseFinance);
                tvCarDeals = dialog.findViewById(R.id.tvCarDeals);

                tvCarDeals.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                tvLeaseFinance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((HomeActivity)getActivity()).replaceFragment(new LeaseFinanceFragment());
                        dialog.dismiss();
                    }
                });

                dialog.show();

             /*
                Dialog dialog = new Dialog(getActivity());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.car_builder_setting_layout);
                Window dialogWindow = dialog.getWindow();
                dialogWindow.setGravity(Gravity.RIGHT | Gravity.TOP);
                dialog.show();*/
            }
        });



        return view;
    }

    private void tabLayout() {
    }

    private void setupViewPager(ViewPager viewPager) {
        mAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());
        mAdapter.addFragment(new CarListFragment(), carList[0]);
        mAdapter.addFragment(new COROLLAFragment(), carList[1]);
        mAdapter.addFragment(new COROLLAIMFragment(), carList[2]);
        mAdapter.addFragment(new YARISFragment(), carList[3]);
        mAdapter.addFragment(new YARISSEADANFragment(), carList[4]);
        mAdapter.addFragment(new EIGHTYSIXFragment(), carList[5]);
        mAdapter.addFragment(new PRIUSFragment(), carList[6]);
        mAdapter.addFragment(new PRIUSVFragment(), carList[7]);
        mAdapter.addFragment(new AVALONFragment(), carList[8]);
        mAdapter.addFragment(new SIENNAFragment(), carList[9]);
        mAdapter.addFragment(new CH_HRFragment(), carList[10]);
        mAdapter.addFragment(new RAV4Fragment(), carList[11]);
        mAdapter.addFragment(new SEQUOIAFragment(), carList[12]);
        mAdapter.addFragment(new HIGHLANDERFragment(), carList[13]);
        mAdapter.addFragment(new TOCOMAFragment(), carList[14]);
        mAdapter.addFragment(new RUNNER_4Fragment(), carList[15]);
        mAdapter.addFragment(new TUNDRAFragment(), carList[16]);
        mAdapter.addFragment(new PRIUS_CFragment(), carList[17]);

        viewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {
        int tabCount = mAdapter.getCount();
        for (int i = 0; i < tabCount; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                View llTab = LayoutInflater.from(tabLayout.getContext()).inflate(R.layout.layout_tab, null);
                ((TextView) llTab.findViewById(R.id.tvTabTitle)).setText(mAdapter.getPageTitle(i));
                tab.setCustomView(llTab);
            }
        }
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }


    public class CarBuilderPagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;
        public CarBuilderPagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
            Log.e("FragCarBuilder","CarBuilderPagerAdapter mNumOfTabs"+mNumOfTabs);
        }

        @Override
        public Parcelable saveState() {
            return super.saveState();
        }

        @Override
        public Fragment getItem(int position) {
            Log.e("FragCarBuilder","CarBuilderPagerAdapter getItem position "+position);
            switch (position) {
                case 0:
                    return new CarListFragment();
                case 1:
                    COROLLAFragment tab2 = new COROLLAFragment();
                    return tab2;
                case 2:
                    COROLLAIMFragment tab3 = new COROLLAIMFragment();
                    return tab3;
                case 3:
                    YARISFragment tab4=new YARISFragment();
                    return  tab4;
                case 4:
                    YARISSEADANFragment tab5=new YARISSEADANFragment();
                    return  tab5;
                case 5:
                    EIGHTYSIXFragment tab6=new EIGHTYSIXFragment();
                    return tab6;
                case 6:
                    PRIUSFragment tab7=new PRIUSFragment();
                    return tab7;

                case 7:
                    PRIUSVFragment tab8=new PRIUSVFragment();
                    return tab8;
                case 8:
                    AVALONFragment tab9=new AVALONFragment();
                    return tab9;
                case 9:
                    SIENNAFragment tab10=new SIENNAFragment();
                    return tab10;
                case 10:
                    CH_HRFragment tab11=new CH_HRFragment();
                    return tab11;
                case 11:
                    RAV4Fragment tab12=new RAV4Fragment();
                    return tab12;
                case 12:
                    SEQUOIAFragment tab13=new SEQUOIAFragment();
                    return tab13;
                case 13:
                    HIGHLANDERFragment tab14=new HIGHLANDERFragment();
                    return tab14;
                case 14:
                    TOCOMAFragment tab15=new TOCOMAFragment();
                    return tab15;
                case 15:
                    RUNNER_4Fragment tab16=new RUNNER_4Fragment();
                    return tab16;

                case 16:
                    TUNDRAFragment tab17=new TUNDRAFragment();
                    return tab17;
                case 17:
                    PRIUS_CFragment tab18=new PRIUS_CFragment();
                    return tab18;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.e("FragCarBuilder","onActivityCreated");



        tabLayout();

        /*tabLayout.addTab(tabLayout.newTab().setText("CAMRY"));
        tabLayout.addTab(tabLayout.newTab().setText("COROLLA "));
        tabLayout.addTab(tabLayout.newTab().setText("COROLLA IM"));
        tabLayout.addTab(tabLayout.newTab().setText("YARIS"));
        tabLayout.addTab(tabLayout.newTab().setText("YARIS SEDAN"));
        tabLayout.addTab(tabLayout.newTab().setText("86"));
        tabLayout.addTab(tabLayout.newTab().setText("PRIUS"));
        tabLayout.addTab(tabLayout.newTab().setText("PRIUS V"));
        tabLayout.addTab(tabLayout.newTab().setText("AVALON"));
        tabLayout.addTab(tabLayout.newTab().setText("SIENNA"));
        tabLayout.addTab(tabLayout.newTab().setText("C-HR"));
        tabLayout.addTab(tabLayout.newTab().setText("RAV4"));
        tabLayout.addTab(tabLayout.newTab().setText("SEQUOIA"));
        tabLayout.addTab(tabLayout.newTab().setText("HOGHLINDER"));
        tabLayout.addTab(tabLayout.newTab().setText("TOCOMA"));
        tabLayout.addTab(tabLayout.newTab().setText("4RUNNER"));
        tabLayout.addTab(tabLayout.newTab().setText("TUNDRA"));
        tabLayout.addTab(tabLayout.newTab().setText("PRIUS C"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);*/


        /*adapter = new CarBuilderPagerAdapter
                (getActivity().getSupportFragmentManager(), tabLayout.getTabCount());*/
//        viewPager.setAdapter(adapter);
//        viewPager.setOffscreenPageLimit(0);
//        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        /*tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/




    }

    @Override
    public void onResume() {
        super.onResume();

        setupViewPager(viewPager);

        Log.e("FragCarBuilder","onResume");
    }
}
