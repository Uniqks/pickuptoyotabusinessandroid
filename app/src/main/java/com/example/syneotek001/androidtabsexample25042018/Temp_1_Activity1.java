/**
 * Copyright 2014 Magnus Woxblom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.syneotek001.androidtabsexample25042018;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.syneotek001.androidtabsexample25042018.Ui.colorpickerview.dialog.ColorPickerDialog;
import com.example.syneotek001.androidtabsexample25042018.Ui.colorpickerview.view.ColorPanelView;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.databinding.ActivityTemp1Binding;


public class Temp_1_Activity1 extends AppCompatActivity implements  View.OnClickListener {


    ActivityTemp1Binding binding;
    Bundle extra;String pos="";
    String mAlignment ="center";
    ColorPanelView mNewColorView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  DataBindingUtil.setContentView(Temp_1_Activity1.this,R.layout.activity_temp_1);

        extra = getIntent().getExtras();

        if(extra != null)
        {
            pos = extra.getString(Utils.TEMP_POS);
        }

        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        this.mNewColorView = new ColorPanelView(Temp_1_Activity1.this);
        binding.btnCenter.setOnClickListener(this);
        binding.btnRight.setOnClickListener(this);
        binding.btnLeft.setOnClickListener(this);
        mAlignment ="center";
        binding.btnCenter.setBackgroundColor(getResources().getColor(R.color.blue));
        binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
        binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
        binding.btnCenter.setTextColor(getResources().getColor(R.color.white));
        binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
        binding.btnRight.setTextColor(getResources().getColor(R.color.black));

        binding.etTxtColor.setText("#000000");
        binding.etBgColor.setText("#000000");

        binding.etBgColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor(0);
            }
        });

        binding.etTxtColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor(1);
            }
        });

        binding.btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               /* String mData ="<tr><td align=\"right\" data-block-id=\"background\" class=\"title\" style=\"color: rgb(117, 117, 117); padding: 30px 15px;\">" +
                        "<h1 data-block-id=\"main-title\" style=\"font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 0px; color: rgb(224, 27, 27);\">" +
                        "Enter your title here</h1> <h4 data-block-id=\"sub-title\" style=\"font-family: Arial, Helvetica, sans-serif; font-weight: normal; " +
                        "margin: 0px; color: rgb(224, 27, 27);\">Subtitle</h4></td></tr>";*/

                String mTop = binding.etPtop.getText().toString();
                String mRight = binding.etPright.getText().toString();
                String mBottom = binding.etPbottom.getText().toString();
                String mLeft = binding.etPleft.getText().toString();

                mTop = mTop.equals("") ?"0px":mTop+"px";
                mRight = mRight.equals("") ?"0px":mRight+"px";
                mBottom = mBottom.equals("") ?"0px":mBottom+"px";
                mLeft = mLeft.equals("") ?"0px":mLeft+"px";

                String mPadding =mTop+" "+mRight+ " "+mBottom+" "+mLeft;

                String mBg_color = hex2Rgb(binding.etBgColor.getText().toString());
                String mTxt_color = hex2Rgb(binding.etTxtColor.getText().toString());



                String mData = "<body bgcolor=\""+binding.etBgColor.getText().toString()+"\"><tr><td><p  data-block-id=\"background\" class=\"title\" style=\"color:"+mBg_color+"; padding: "+mPadding+";\">" +
                        "<h1 align=\""+mAlignment+"\" data-block-id=\"main-title\" style=\"font-family: Arial, Helvetica, sans-serif; font-weight: normal;" +
                        " margin: 0px; color: "+mTxt_color+";\">" +
                        binding.etTitle.getText().toString()+"</h1> <h4 align=\"" + mAlignment + "\" data-block-id=\"sub-title\" style=\"font-family:" +
                        " Arial, Helvetica, sans-serif; " + "font-weight: normal; margin: 0px; " +
                        "color: "+mTxt_color+"\">"+ binding.etSubtitle.getText().toString()+"</h4></p></td></tr></body>";

                LogUtils.i(" mData "+mData);

                Intent i = getIntent();
                i.putExtra(Utils.TEMP_1,mData);
                i.putExtra(Utils.TEMP_POS,pos);
                setResult(0,i);
                finish();
            }
        });


    }

    public void setColor(final int i) {
        final ColorPickerDialog dialog = new ColorPickerDialog(Temp_1_Activity1.this, 0xFFFFFFFF);
        dialog.setTitle(getString(R.string.temp_picker));
        dialog.setAlphaSliderVisible(true);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.temp_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialg, int which) {



                        mNewColorView.setColor(dialog.getColor());

                        LogUtils.i(hex2Rgb(toHex(mNewColorView.getColor()))+" dialog ");
                        int code = dialog.getColor();
                        if(code == -1)
                        {
                            if(i==0)binding.etBgColor.setText("#000000");
                            else  if(i==1)binding.etTxtColor.setText("#000000");

                        }else {
                            if(i==0)binding.etBgColor.setText("#"+toHex(mNewColorView.getColor())+"");
                            else  if(i==1)binding.etTxtColor.setText("#"+toHex(mNewColorView.getColor())+"");
                        }
                    }
                });


        dialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                getString(R.string.temp_cancel), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialg, int which) {


                        if(i==0)binding.etBgColor.setText("#000000");
                        else  if(i==1)binding.etTxtColor.setText("#000000");

                    }
                });
        dialog.show();
    }

    public static String hex2Rgb(String colorStr) {

        int i1=  Integer.valueOf( colorStr.substring( 1, 3 ), 16 );
        int i2=  Integer.valueOf( colorStr.substring( 3, 5 ), 16 );
        int i3= Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) ;
        return "rgb("+i1+" "+i2+" "+i3+")";
    }



    private static String toHex(int argb) {
        StringBuilder sb = new StringBuilder();
        sb.append(toHexString((byte) Color.alpha(argb)));
        sb.append(toHexString((byte) Color.red(argb)));
        sb.append(toHexString((byte) Color.green(argb)));
        sb.append(toHexString((byte) Color.blue(argb)));
        return sb.toString();
    }

    private static String toHexString(byte v) {
        String hex = Integer.toHexString(v & 0xff);
        if (hex.length() == 1) {
            hex = "0" + hex; //$NON-NLS-1$
        }
        return hex.toUpperCase();
    }
    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btn_center:
                mAlignment ="center";
                binding.btnCenter.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
                binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
                binding.btnCenter.setTextColor(getResources().getColor(R.color.white));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
                binding.btnRight.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.btn_left:
                mAlignment ="left";
                binding.btnCenter.setBackgroundColor(Color.TRANSPARENT);
                binding.btnLeft.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
                binding.btnCenter.setTextColor(getResources().getColor(R.color.black));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.white));
                binding.btnRight.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.btn_right:
                mAlignment ="right";
                binding.btnCenter.setBackgroundColor(Color.TRANSPARENT);
                binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
                binding.btnRight.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnCenter.setTextColor(getResources().getColor(R.color.black));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
                binding.btnRight.setTextColor(getResources().getColor(R.color.white));
                break;
        }

    }
}
