package com.example.syneotek001.androidtabsexample25042018;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.example.syneotek001.androidtabsexample25042018.Ui.colorpickerview.dialog.ColorPickerDialog;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;


public class ColorAlertActivity extends Activity {

    ColorPickerDialog dialog;
    int mColor = Color.BLACK;
    Bundle extra;
    int mCode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        extra = getIntent().getExtras();
        if (extra != null)
            mCode = extra.getInt(Utils.EXTRA_POS);

        dialog = new ColorPickerDialog(this, 0xFFFFFFFF);
        dialog.setTitle(getString(R.string.temp_picker));
        dialog.setAlphaSliderVisible(true);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.temp_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialg, int which) {


                        int code = dialog.getColor();
                        if (code == -1)
                            mColor = (Color.BLACK);
                        else
                            mColor = (dialog.getColor());

                        setcolor();


                    }
                });


        dialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                getString(R.string.temp_cancel), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialg, int which) {
                        mColor = (Color.BLACK);

                        setcolor();


                    }
                });
        dialog.show();
    }


    public void setcolor() {
        Intent n = getIntent();
        n.putExtra(Utils.EXTRA_COLOR, mColor);
        setResult(mCode, n);
        finish();
    }

}
