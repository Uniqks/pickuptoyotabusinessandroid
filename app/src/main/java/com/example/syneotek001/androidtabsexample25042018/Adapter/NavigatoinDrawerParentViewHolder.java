package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnExpandableRecyclerViewChildItemClickListener;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;


public class NavigatoinDrawerParentViewHolder extends GroupViewHolder {
    private ImageView arrow;
    private TextView genreName;
    private OnExpandableRecyclerViewChildItemClickListener mCallBack;

    class C05621 implements OnClickListener {
        C05621() {
        }

        public void onClick(View view) {
            if (NavigatoinDrawerParentViewHolder.this.mCallBack != null) {
                NavigatoinDrawerParentViewHolder.this.mCallBack.onParentItemClicked(String.valueOf(view.getTag()));
            }
        }
    }

    NavigatoinDrawerParentViewHolder(View itemView) {
        super(itemView);
        this.genreName = (TextView) itemView.findViewById(R.id.list_item_parent_name);
        this.arrow = (ImageView) itemView.findViewById(R.id.list_item_parent_arrow);
    }

    void setGenreTitle(ExpandableGroup genre, OnExpandableRecyclerViewChildItemClickListener listener) {
        if (genre instanceof NavigationDrawerParentModel) {
            this.genreName.setText(genre.getTitle());
            this.genreName.setTag(genre.getTitle());
            this.genreName.setCompoundDrawablesWithIntrinsicBounds(((NavigationDrawerParentModel) genre).getIconResId(), 0, 0, 0);
            if (genre.getItemCount() > 0) {
                this.arrow.setVisibility(View.VISIBLE);
                return;
            }
            this.arrow.setVisibility(View.GONE);
            this.mCallBack = listener;
            this.genreName.setOnClickListener(new C05621());
        }
    }
    public void expand() {
        animateExpand();
    }
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate = new RotateAnimation(360.0f, 180.0f, 1, 0.5f, 1, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        this.arrow.setAnimation(rotate);
    }
    private void animateCollapse() {
        RotateAnimation rotate = new RotateAnimation(180.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        this.arrow.setAnimation(rotate);
    }
}
