package com.example.syneotek001.androidtabsexample25042018.rest;

public class AppConstant {
    public static final int LOGIN_REQUEST = 1;
    public static final int SIGNUP_REQUEST = 2;
    public static final int OTP_REQUEST = 2;
    public static final int PROFILE_REQUEST = 3;
    public static final int ADD_DISEASE_SYMPTOM = 4;
    public static final int SP_SIGNUP_REQUEST = 5;
    public static final int SP_SIGNUP_SERVICES_LIST = 6;

    public static final int GET_PROFILE = 7;
    public static final int UPDATE_USER = 8;
    public static final int GET_TICKETS = 9;
    public static final int GET_SUPPORT = 10;
    public static final int UPDATE_TICKETS = 11;
    public static final int CLOSE_TICKET = 12;
    public static final int CANCEL_REQUEST = 13;
    public static final int GET_BOOKING_DETAIL = 14;

    public static final int IS_ONLINE = 15;

    public static final int IS_DETAIL = 15;

    public static final int GET_SMS = 16;

    public static final int GET_CHAT_LIST_REQUEST = 17;
    public static final int GET_CHAT_MESSAGES_LIST_REQUEST = 18;
    public static final int SEND_TEXT_MESSAGE_REQUEST = 19;
    public static final int GET_SALESMAN_LIST_REQUEST = 20;
    public static final int GET_SALESMAN_DEAL_REQUESTS_LIST_REQUEST = 21;
    public static final int GET_SALESMAN_ACTIVE_DEALS_REQUEST = 22;
    public static final int GET_SALESMAN_SOLD_DEALS_REQUEST = 23;
    public static final int GET_DEAL_REQUEST_DETAIL_REQUEST = 24;
    public static final int GET_DEAL_INFO_DETAIL_REQUEST = 25;

    public static final String CONTACT_LIST_ACTION = "contactList";
    public static final String CHAT_LIST_ACTION = "chatList";
    public static final String CHAT_MESSAGE_LIST_ACTION = "chatDetail";
    public static final String CHAT_MESSAGE_SEND_ACTION = "sendMessage";
    public static final String SALESMAN_LIST_ACTION = "salesmanList";
    public static final String SALESMAN_DEAL_REQUESTS_LIST_ACTION = "salesmanDealRequests";
    public static final String SALESMAN_ACTIVE_DEALS_ACTION = "salesmanActiveDeals";
    public static final String SALESMAN_SOLD_DEALS_ACTION = "salesmanSoldDeals";
    public static final String DEAL_REQUEST_DETAIL_ACTION = "dealRequestDetail";
    public static final String DEAL_INFO_DETAIL_ACTION = "DealInfoDetail";
    public static final String CHAT_MESSAGE_TYPE_TEXT = "text";
    public static final String CHAT_MESSAGE_TYPE_IMAGE = "image";

    public static final int LIST_API_VISIBLE_THRESHHOLD = 5;

}