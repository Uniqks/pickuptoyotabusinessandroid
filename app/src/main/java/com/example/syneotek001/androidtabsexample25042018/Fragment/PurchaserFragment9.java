package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.SignatureMainActivity;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentPurchaser2Binding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentPurchaser9Binding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import pub.devrel.easypermissions.EasyPermissions;

public class PurchaserFragment9 extends Fragment implements OnClickListener, OnBackPressed {
    public Button btnSave, btnSaveAndNext;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;
    FragmentPurchaser9Binding binding;
    String name, email, mobile, password, address;
    YoYo yoYo;
    ArrayList<String> arr_salesman_status = new ArrayList<>();
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    Typeface typeface;
    String Empsign_path = "";
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_purchaser9, container, false);
        View view = binding.getRoot();

        typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/montserrat_regular.otf");

        TextPaint tp = new TextPaint();
        tp.linkColor = getResources().getColor(R.color.colorPrimary);           //not quite sure what the format should be
        UnderlineSpan us = new UnderlineSpan();
        us.updateDrawState(tp);
        SpannableString content = new SpannableString(getString(R.string.txt_terms_privacy));
        content.setSpan(us, 0, content.length(), 0);



        binding.ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });





     /*   binding.rbCash.setTypeface(typeface);
        binding.rbCheque.setTypeface(typeface);
        binding.rbCityOfToronto.setTypeface(typeface);
        binding.rbCreditCard.setTypeface(typeface);
        binding.rbDebit.setTypeface(typeface);
        binding.rbNewPlates.setTypeface(typeface);
        binding.rbTransfer.setTypeface(typeface);*/



        setHasOptionsMenu(false);
        setClickEvents();

       setAnimation(binding.lblSalesFinal);
        setAnimation(binding.lblPurchaserSignature);
        setAnimation(binding.lblcoSign);
        setAnimation(binding.ilbNamPostion);
        setAnimation(binding.lblDealsignature);
        setAnimation(binding.lblSalePersonName);
        setAnimation(binding.lblSalePersonSignature);
      /*   setAnimation(binding.etAmountDueOnDelivery);
        setAnimation(binding.etAmountFinanced);
        setAnimation(binding.etDeposit);
        setAnimation(binding.etHSTRegNo);
        setAnimation(binding.etHSTOnTotalPrice);
        setAnimation(binding.etLicenseFee);
        setAnimation(binding.etTotalPurchasePrice);
        setAnimation(binding.etTotalVehiclePrice);
        setAnimation(binding.etTradeInAllowance);
        setAnimation(binding.etPayoutOnLiens);
        setAnimation(binding.etFuel);*/

        binding.llSign.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {

                    if (EasyPermissions.hasPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                        startActivityForResult(i, 11);
                        Log.e("imgsos", "Clicked");
                        Log.e("1", "1");

                    } else {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_EXTERNAL_STORAGE);
                        Log.e("2", "2");
                    }

                } else {


                    /*Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    startActivityForResult(intent, PICK_SMS);*/
                    Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                    startActivityForResult(i, 11);
                    Log.e("imgsos", "Clicked");
                    Log.e("3", "3");

                }

            }
        });


        binding.llSign2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {

                    if (EasyPermissions.hasPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                        startActivityForResult(i, 13);
                        Log.e("imgsos", "Clicked");
                        Log.e("1", "1");

                    } else {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_EXTERNAL_STORAGE);
                        Log.e("2", "2");
                    }

                } else {


                    /*Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    startActivityForResult(intent, PICK_SMS);*/
                    Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                    startActivityForResult(i, 13);
                    Log.e("imgsos", "Clicked");
                    Log.e("3", "3");

                }

            }
        });
        binding.llSign3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {

                    if (EasyPermissions.hasPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                        startActivityForResult(i, 14);
                        Log.e("imgsos", "Clicked");
                        Log.e("1", "1");

                    } else {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_EXTERNAL_STORAGE);
                        Log.e("2", "2");
                    }

                } else {


                    /*Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    startActivityForResult(intent, PICK_SMS);*/
                    Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                    startActivityForResult(i, 14);
                    Log.e("imgsos", "Clicked");
                    Log.e("3", "3");

                }

            }
        });


        binding.llrightSign.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {

                    if (EasyPermissions.hasPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                        startActivityForResult(i, 12);
                        Log.e("imgsos", "Clicked");
                        Log.e("1", "1");

                    } else {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_EXTERNAL_STORAGE);
                        Log.e("2", "2");
                    }

                } else {


                    /*Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    startActivityForResult(intent, PICK_SMS);*/
                    Intent i = new Intent(getActivity(), SignatureMainActivity.class);
                    startActivityForResult(i, 12);
                    Log.e("imgsos", "Clicked");
                    Log.e("3", "3");

                }

            }
        });
        arr_salesman_status = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_salesman_status)));


        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(" onActivityResult ", "requestCode " + requestCode+" resultCode "+resultCode);
        if (requestCode == 11 && resultCode == Activity.RESULT_OK) {
            Empsign_path = data.getStringExtra("path");
            Bitmap bmp = BitmapFactory.decodeFile(Empsign_path);
            ImageView img;
            binding.signaturePad.setImageBitmap(bmp);
//            Picasso.with(getActivity()).load(Empsign_path).into(binding.signaturePad);

            Log.e(" onActivityResult ", "Empsign_path " + Empsign_path);

        }
        else   if (requestCode == 12 && resultCode == Activity.RESULT_OK) {
            Empsign_path = data.getStringExtra("path");
            Bitmap bmp = BitmapFactory.decodeFile(Empsign_path);
            ImageView img;
            binding.signaturePad2.setImageBitmap(bmp);
            Log.e(" ", "Empsign_path2 " + Empsign_path);

        }

        else   if (requestCode == 13 && resultCode == Activity.RESULT_OK) {
            Empsign_path = data.getStringExtra("path");
            Bitmap bmp = BitmapFactory.decodeFile(Empsign_path);
            ImageView img;
            binding.signaturePad3.setImageBitmap(bmp);
            Log.e(" ", "Empsign_path3 " + Empsign_path);

        }

        else   if (requestCode == 14 && resultCode == Activity.RESULT_OK) {
            Empsign_path = data.getStringExtra("path");
            Bitmap bmp = BitmapFactory.decodeFile(Empsign_path);
            ImageView img;
            binding.signaturePad4.setImageBitmap(bmp);
            Log.e(" ", "Empsign_path4 " + Empsign_path);

        }
    }

    public void setAnimation(View view) {
        YoYo.with(Techniques.FlipInX).duration(1000).delay(150).repeat(0).playOn(view);
    }

    private void setClickEvents() {
     //   binding.btnSaveAndNext.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSaveAndNext:
                Log.e("btnSaveAndNext","btnSaveAndNext");

              /*  if (getArguments() != null) {
                    if (getArguments().getBoolean("is_edit")) {
                        if (getArguments().getSerializable("salesman_profile") != null) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("salesman_profile", getArguments().getSerializable("salesman_profile"));
                            bundle.putBoolean("is_edit", true);
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    } else {
                        if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("is_edit", false);
                            bundle.putString("profile_type",getArguments().getString("profile_type"));
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    }
                }*/
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
