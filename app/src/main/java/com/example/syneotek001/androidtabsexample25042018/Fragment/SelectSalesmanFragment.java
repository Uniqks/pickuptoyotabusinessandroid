package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.SelectSalesmanItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Support.RecyclerItemClickListener;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.model.LeadNoteModel;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanListResponse.SalesmanListData;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiManager;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiResponseInterface;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SelectSalesmanFragment extends Fragment implements OnClickListener, ApiResponseInterface {
    SelectSalesmanItemAdapter mAdapter;
    ArrayList<SalesmanListData> mNoteList = new ArrayList<>();
    public RecyclerView recyclerView;
    public TextView tvNoLeadNoteFound;
    ImageView ivBack;
    ApiManager apiManager;
    int fromlimit = 1;
    Handler handler;
    boolean loadMore = false;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_select_salesman, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        tvNoLeadNoteFound = view.findViewById(R.id.tvNoLeadNoteFound);
        ivBack = view.findViewById(R.id.ivBack);
        FloatingActionButton fab = view.findViewById(R.id.fab);

        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        apiManager = new ApiManager(getActivity(), this);
        recyclerView = view.findViewById(R.id.recyclerView);
        tvNoLeadNoteFound = view.findViewById(R.id.tvNoDataFound);
        handler = new Handler();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
        mAdapter = new SelectSalesmanItemAdapter(getActivity(), this.mNoteList, recyclerView);
        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                Log.e("ChatListFragment", " onLoadMore ");

                //add null , so the adapter will check view_type and show progress bar at bottom
                mNoteList.add(null);
                recyclerView.post(new Runnable() {
                    public void run() {
                        mAdapter.notifyItemInserted(mNoteList.size() - 1);
                    }
                });

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        //   remove progress item
                        if (mNoteList.size() > 0)
                            mNoteList.remove(mNoteList.size() - 1);

                        mAdapter.notifyItemRemoved(mNoteList.size());

                        if (Utils.isOnline()) {
                            fromlimit = fromlimit + 1;
                            loadMore = true;
                            apiManager.getSalemanList(fromlimit, AppConstant.SALESMAN_LIST_ACTION);

                        } else {
                            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
                        }

                    }
                }, 1000);
            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("SelectSalesmanFragment"," onResume ");
        loadMore = false;
        mNoteList.clear();
        fromlimit = 1;
        setUpRecyclerView();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llNote:
                ((HomeActivity)getActivity()).replaceFragment(new NoteDetailFragment());
                return;
            default:
                return;
        }
    }

    private void setUpRecyclerView() {
        Log.e("SelectSalesmanFragment"," setUpRecyclerView loadMore "+loadMore+" mNoteList size "+mNoteList.size()+" fromlimit "+fromlimit);
        if (Utils.isOnline()) {

            loadMore = false;
            mNoteList.clear();
            LogUtils.i("BookingHistory" + " loadData arr_myrequestlist size " + mNoteList.size());
            //getTripHistory(loadMore, 0, pos/*,TripHistoryActivity.this*/);
            apiManager.getSalemanList(fromlimit, AppConstant.SALESMAN_LIST_ACTION);
        } else {

            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
        }

        /*mNoteList.clear();
        mNoteList.add(new LeadNoteModel("1", "Sunil Patel", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
        mNoteList.add(new LeadNoteModel("2", "Hitesh Prajapati", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
        mNoteList.add(new LeadNoteModel("3", "Sunil Dev", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
        mNoteList.add(new LeadNoteModel("4", "Arun Kumar", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
        mNoteList.add(new LeadNoteModel("5", "Kishore Raj", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down));
        mAdapter = new SelectSalesmanItemAdapter(getActivity(), mNoteList);
        recyclerView.setAdapter(this.mAdapter);
        refreshData();*/
    }

    private void refreshData() {
        if (mNoteList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoLeadNoteFound.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.GONE);
        tvNoLeadNoteFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void isError(String errorCode) {

    }

    @Override
    public void isSuccess(Object response, int ServiceCode) {
        SalesmanListResponse chatListResponse = (SalesmanListResponse) response;
        Log.e("loadmore_Count", "" + loadMore);

        final List<SalesmanListData> taxiList = chatListResponse.getSalesmanList();


        if (taxiList.size() > 0) {

            mNoteList.addAll(taxiList);

            if (loadMore) {
                Log.e("loadmore_Count", "if");
                mAdapter.notifyItemInserted(mNoteList.size());
                mAdapter.notifyDataSetChanged();
                mAdapter.setLoaded();
                LogUtils.i("BookingHistory" + " getTripHistory onResponse loadMore arr_myrequestlist.size() " + mNoteList.size());

            } else {

                Log.e("loadmore_Count", "else");
                recyclerView.setVisibility(View.VISIBLE);

                Log.i("json", mNoteList.size() + " arr_myrequestlist ");
                recyclerView.setAdapter(mAdapter);
                mAdapter.setLoaded();

                LogUtils.i("BookingHistory" + " getTripHistory onResponse !loadMore arr_myrequestlist.size() " + mNoteList.size());

            }

        } else {

            if (!loadMore) {

                if (mNoteList.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
//                                    empty_view.setVisibility(View.VISIBLE);

                    tvNoLeadNoteFound.setVisibility(View.VISIBLE);

                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    tvNoLeadNoteFound.setVisibility(View.GONE);
                }
            }
        }
    }

    public interface OnLoadMoreListener {

        void onLoadMore();

    }

}
