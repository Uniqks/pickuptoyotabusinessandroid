package com.example.syneotek001.androidtabsexample25042018.model;

public class DealManagerItemModel {
    private String customerId = "";
    private String dealId = "";
    private String id = "";
    private String salesmanName = "";
    private String label = "";
    private String dealType = "";
    private boolean isSelected = false;

    public DealManagerItemModel(String id,String customerId, String dealId, String salesmanName, String label, String dealType) {
        this.id = id;
        this.customerId = customerId;
        this.salesmanName = salesmanName;
        this.dealId = dealId;
        this.label = label;
        this.dealType = dealType;
        this.isSelected = false;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

}
