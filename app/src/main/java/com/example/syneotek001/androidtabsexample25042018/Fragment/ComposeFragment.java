package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.syneotek001.androidtabsexample25042018.Adapter.dialog.DatePickerDialog;
import com.example.syneotek001.androidtabsexample25042018.Adapter.dialog.TimePickerDialog;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;


public class ComposeFragment extends Fragment implements OnClickListener, OnCheckedChangeListener {
    public int mDay;
    public int mHour;
    public int mMin;
    public int mMonth;
    public int mYear;
    String[] tilSelectTemp;
    View view;
    public CheckBox chSchedule;
    public CardView cvSection1, cvSection2;
    public LinearLayout cvSection3, llSection1, llSection2, llSection3, mboundView0;
    public EditText etAttachment, etBcc, etCc, etMessage, etSchedulDate, etSchedulTime, etSelectTem, etSubject, etTo;
    public TextInputLayout tilAttachment, tilBcc, tilCc, tilMessage, tilSchedulDate, tilSchedulTime, tilSubject, tilTo;
    Context mcontext;
    ImageView ivBack;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        mcontext = getActivity();
        View view = inflater.inflate(R.layout.fragment_compose, container, false);
        getView(view);
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        setClickEvent();
        return view;
    }

    private void getView(View view) {
        chSchedule = view.findViewById(R.id.chSchedule);
        cvSection1 = view.findViewById(R.id.cvSection1);
        cvSection2 = view.findViewById(R.id.cvSection2);
        cvSection3 = view.findViewById(R.id.cvSection3);
        llSection1 = view.findViewById(R.id.llSection1);
        llSection2 = view.findViewById(R.id.llSection2);
        llSection3 = view.findViewById(R.id.llSection3);
        etAttachment = view.findViewById(R.id.etAttachment);
        etBcc = view.findViewById(R.id.etBcc);
        etCc = view.findViewById(R.id.etCc);
        etMessage = view.findViewById(R.id.etMessage);
        etSchedulDate = view.findViewById(R.id.etSchedulDate);
        etSchedulTime = view.findViewById(R.id.etSchedulTime);
        etSelectTem = view.findViewById(R.id.etSelectTem);
        etSubject = view.findViewById(R.id.etSubject);
        etTo = view.findViewById(R.id.etTo);
        tilAttachment = view.findViewById(R.id.tilAttachment);
        tilBcc = view.findViewById(R.id.tilBcc);
        tilCc = view.findViewById(R.id.tilCc);
        tilMessage = view.findViewById(R.id.tilMessage);
        tilSchedulDate = view.findViewById(R.id.tilSchedulDate);
        tilSchedulTime = view.findViewById(R.id.tilSchedulTime);
//        tilSelectTemp=view.findViewById(R.id.tilSelectTemp);
        tilSubject = view.findViewById(R.id.tilSubject);
        tilTo = view.findViewById(R.id.tilTo);
        ivBack = view.findViewById(R.id.ivBack);
    }

    class SchedulTimeClick implements OnClickListener {
        SchedulTimeClick() {
        }

        public void onClick(View view) {
            TimePickerDialog dialog = new TimePickerDialog();
            dialog.setTargetFragment(ComposeFragment.this, 1);
            dialog.show(ComposeFragment.this.getFragmentManager().beginTransaction(), android.app.TimePickerDialog.class.getSimpleName());
        }
    }

    class SchedulDateClick implements OnClickListener {
        SchedulDateClick() {
        }

        public void onClick(View view) {
            DatePickerDialog dialog = new DatePickerDialog();
            dialog.setTargetFragment(ComposeFragment.this, 2);
            dialog.show(ComposeFragment.this.getFragmentManager().beginTransaction(), android.app.DatePickerDialog.class.getSimpleName());
        }
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.tilSelectTemp = new String[]{getResources().getString(R.string.email_Select_Templates_category1), getResources().getString(R.string.email_Select_Templates_category2)};
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create_deal, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_create_deal:
                return true;
            default:
                return false;
        }
    }

    private void setClickEvent() {
        etSelectTem.setOnClickListener(this);
        etSchedulDate.setOnClickListener(this);
        chSchedule.setOnCheckedChangeListener(this);
        etSchedulTime.setOnClickListener(new SchedulTimeClick());
        etSchedulDate.setOnClickListener(new SchedulDateClick());
    }

    public void setTime(int hour, int min) {
        String format;
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }
        etSchedulTime.setText(new StringBuilder().append(hour < 10 ? "0" + hour : Integer.valueOf(hour)).append(" : ").append(min < 10 ? "0" + min : Integer.valueOf(min)).append(" ").append(format));
    }

    public void setDate(int mDay, int mMonth, int mYear) {
        etSchedulDate.setText(new StringBuilder().append(mDay).append("-").append(mMonth + 1).append("-").append(mYear));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bundle bundle;
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    bundle = data.getExtras();
                    if (bundle != null) {
                        this.mHour = bundle.getInt("hour");
                        this.mMin = bundle.getInt("min");
                        Logger.m6e("PICKER", "Got hour=" + this.mHour + " and minute=" + this.mMin + ", yay!");
                        etSchedulTime.setText(this.mHour + " and minute=" + this.mMin);
                        setTime(this.mHour, this.mMin);
                        return;
                    }
                    Logger.m6e("PICKER", "Got hour=" + this.mHour + " and minute=" + this.mMin + ", yay!");
                    return;
                } else if (resultCode == 0) {
                    Logger.m6e("PICKER", "Result Cancel");
                    return;
                } else {
                    return;
                }
            case 2:
                if (resultCode == -1) {
                    bundle = data.getExtras();
                    if (bundle != null) {
                        this.mDay = bundle.getInt("dayOfMonth");
                        this.mMonth = bundle.getInt("month");
                        this.mYear = bundle.getInt("year");
                        Logger.m6e("PICKER", "Got dayr=" + this.mDay + " and month=" + this.mMonth + "and year= + mYear, yay!");
                        etSchedulDate.setText(this.mDay + " and month=" + this.mMonth + "and year= + mYear");
                        setDate(this.mDay, this.mMonth, this.mYear);
                        return;
                    }
                    Logger.m6e("PICKER", " Got dayr=" + this.mDay + " and month=" + this.mMonth + "and year= + mYear, yay!");
                    return;
                } else if (resultCode == 0) {
                    Logger.m6e("PICKER", "Result Cancel");
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private void displayDialog(String title, final String[] items, final EditText etView) {
        new Builder(mcontext).setTitle((CharSequence) title).setSingleChoiceItems((CharSequence[]) items, 0, null).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                etView.setText(items[((AlertDialog) dialog).getListView().getCheckedItemPosition()]);
            }
        }).show();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.etSelectTem:
                displayDialog(getResources().getString(R.string.email_Select_Templates), tilSelectTemp, etSelectTem);
                return;
            default:
                return;
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (chSchedule.isChecked()) {
            llSection3.setVisibility(View.VISIBLE);
            tilAttachment.setVisibility(View.GONE);
            return;
        }
        llSection3.setVisibility(View.GONE);
        tilAttachment.setVisibility(View.VISIBLE);
    }
}
