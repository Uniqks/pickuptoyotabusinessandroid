package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.interfaces.EmailAdapterListener;
import com.example.syneotek001.androidtabsexample25042018.model.EmailListModel;

import java.util.ArrayList;
import java.util.List;

public class EmailItemAdapter extends RecyclerView.Adapter<EmailItemAdapter.MyViewHolder> {
    private static int currentSelectedIndex = -1;
    private final Animation anim;
    private SparseBooleanArray animationItemsIndex;
    private EmailAdapterListener listener;
    private Context mContext;
    private ArrayList<EmailListModel> mData = new ArrayList();
    private boolean reverseAllAnimations = false;
    private SparseBooleanArray selectedItems;

    class MyViewHolder extends ViewHolder {
        TextView tvTitle,tvSubject,tvBody,tvDateTime,tvUserLetter;
        ImageView ivUserLetterBg,ivSelectedMail;
        RelativeLayout rlEmailCard,layoutImage;
        MyViewHolder(View view) {
            super(view);
            tvTitle=view.findViewById(R.id.tvTitle);
            tvSubject=view.findViewById(R.id.tvSubject);
            tvBody=view.findViewById(R.id.tvBody);
            ivUserLetterBg=view.findViewById(R.id.ivUserLetterBg);
            tvDateTime=view.findViewById(R.id.tvDateTime);
            tvUserLetter=view.findViewById(R.id.tvUserLetter);
            rlEmailCard=view.findViewById(R.id.rlEmailCard);
            ivSelectedMail=view.findViewById(R.id.ivSelectedMail);
            layoutImage=view.findViewById(R.id.layoutImage);

        }
    }

    public EmailItemAdapter(Context context, ArrayList<EmailListModel> list, EmailAdapterListener listener) {
        this.mContext = context;
        this.mData = list;
        this.anim = AnimationUtils.loadAnimation(this.mContext, R.anim.anim_scale_in_out);
        this.selectedItems = new SparseBooleanArray();
        this.animationItemsIndex = new SparseBooleanArray();
        this.listener = listener;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_email, parent, false);
        return new EmailItemAdapter.MyViewHolder(v);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        EmailListModel mEmailListModel = (EmailListModel) this.mData.get(position);
        holder.tvTitle.setText(mEmailListModel.getTitle());
        holder.tvTitle.setTextColor(ContextCompat.getColor(this.mContext, mEmailListModel.isRead() ? R.color.gray : R.color.black));
        holder.tvSubject.setText(mEmailListModel.getSubject());
        holder.tvBody.setText(mEmailListModel.getBody());
        holder.ivUserLetterBg.setImageResource(mEmailListModel.getBgCircle());
        holder.tvDateTime.setText(TimeStamp.millisToFormat(Long.valueOf(mEmailListModel.getDateTime() * 1000)));
        String title = mEmailListModel.getTitle().toUpperCase();
        if (title.length() > 0) {
            holder.tvUserLetter.setText(title.substring(0, 1).toUpperCase());
        } else {
            holder.tvUserLetter.setText("T");
        }
        applyIconAnimation((MyViewHolder) holder, position);
        setSelectedItem((MyViewHolder) holder, position);
        setItemViewClickListener((MyViewHolder) holder, position);

        holder.layoutImage.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Logger.m5e("User Image Clicked");
                view.startAnimation(EmailItemAdapter.this.anim);
                listener.onIconClicked(position);
            }
        });
    }

    private void setItemViewClickListener(MyViewHolder holder, final int position) {
        holder.rlEmailCard.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                EmailItemAdapter.this.listener.onMessageRowClicked(position);
            }
        });
    }

    private void setSelectedItem(MyViewHolder holder, int position) {
        holder.rlEmailCard.setBackgroundColor(ContextCompat.getColor(this.mContext, this.selectedItems.get(position, false) ? R.color.row_activated : 17170443));
    }

    private void applyIconAnimation(MyViewHolder holder, int position) {
        if (this.selectedItems.get(position, false)) {
            holder.ivUserLetterBg.setVisibility(View.GONE);
            holder.tvUserLetter.setVisibility(View.GONE);
            holder.ivSelectedMail.setVisibility(View.VISIBLE);
            if (currentSelectedIndex == position) {
                resetCurrentIndex();
                return;
            }
            return;
        }
        holder.ivSelectedMail.setVisibility(View.GONE);
        holder.ivUserLetterBg.setVisibility(View.VISIBLE);
        holder.tvUserLetter.setVisibility(View.VISIBLE);
        if ((this.reverseAllAnimations && this.animationItemsIndex.get(position, false)) || currentSelectedIndex == position) {
            resetCurrentIndex();
        }
    }

    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList(this.selectedItems.size());
        for (int i = 0; i < this.selectedItems.size(); i++) {
            items.add(Integer.valueOf(this.selectedItems.keyAt(i)));
        }
        return items;
    }

    public int getSelectedItemCount() {
        return this.selectedItems.size();
    }

    public void removeData(int position) {
        this.mData.remove(position);
        resetCurrentIndex();
    }

    public void setRead(int position, boolean isRead) {
        EmailListModel emailListModel = (EmailListModel) this.mData.get(position);
        emailListModel.setRead(isRead);
        this.mData.set(position, emailListModel);
        resetCurrentIndex();
    }

    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (this.selectedItems.get(pos, false)) {
            this.selectedItems.delete(pos);
            this.animationItemsIndex.delete(pos);
        } else {
            this.selectedItems.put(pos, true);
            this.animationItemsIndex.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public int getItemCount() {
        return this.mData.size();
    }

    public void resetAnimationIndex() {
        this.reverseAllAnimations = false;
        this.animationItemsIndex.clear();
    }

    public void clearSelections() {
        this.reverseAllAnimations = true;
        this.selectedItems.clear();
        notifyDataSetChanged();
    }
}
