package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.syneotek001.androidtabsexample25042018.Adapter.DealManagerItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.DealManagerItemModel;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse.SalesmanDealRequestListData;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse.SalesmanDealRequestListData.RequestListData;

import java.io.Serializable;
import java.util.ArrayList;

public class DealManagerFragment extends Fragment implements OnBackPressed , OnLeadItemViewClickListener {
    ArrayList<DealManagerItemModel> dealManagerList = new ArrayList<>();
    DealManagerItemAdapter mAdapter;
    public RecyclerView recyclerView;
    public LinearLayout ll_NoRecordFound;
    String dealType,salesman_name,sales_man_id;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_deal_manager, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        ll_NoRecordFound = view.findViewById(R.id.ll_NoRecordFound);
        ImageView ivBack=view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        if (getArguments()!=null) {
            salesman_name = getArguments().getString("sales_man_name");
            sales_man_id = getArguments().getString("sales_man_id");
            dealType = getArguments().getString("deal_type");
        }

        setUpRecyclerView();

        return view;
    }

    private void setUpRecyclerView() {
        dealManagerList.add(new DealManagerItemModel("1", "152244893677", "168946121555", salesman_name, "Under Contract", dealType));
        dealManagerList.add(new DealManagerItemModel("2", "152244893477", "168946121555", salesman_name, "Closing", dealType));
        dealManagerList.add(new DealManagerItemModel("3", "152244893489", "168946121555", salesman_name, "Dead", dealType));
        dealManagerList.add(new DealManagerItemModel("4", "152244891563", "168946121555", salesman_name, "Under Contract", dealType));
        dealManagerList.add(new DealManagerItemModel("5", "152244891123", "168946121555", salesman_name, "Closing", dealType));
        dealManagerList.add(new DealManagerItemModel("6", "152244891123", "168946121555", salesman_name, "Under Contract", dealType));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_right));
        mAdapter = new DealManagerItemAdapter(getActivity(), this.dealManagerList,this);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onItemClicked(int i) {

    }

    @Override
    public void onLeadBusinessRequestClicked(int i) {

    }

    @Override
    public void onLeadDealRequestClicked(int i) {
        DealStatusEditFragment dealStatusEditFragment = new DealStatusEditFragment();
        Bundle bundle = new Bundle();
        bundle.putString("deal_type",dealType);
        dealStatusEditFragment.setArguments(bundle);
        ((HomeActivity) getActivity()).replaceFragment(dealStatusEditFragment);
    }

    @Override
    public void onLeadDetailIconClicked(int i) {
        DealInfoDetailsFragment dealInfoDetailsFragment = new DealInfoDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Utils.EXTRA_DEAL_DETAIL_INFO_DEAL_ID,"35"/*leadItemArray.get(position).getDeal_id()*/);
        dealInfoDetailsFragment.setArguments(bundle);
        ((HomeActivity) getActivity()).replaceFragment(dealInfoDetailsFragment);
    }

    @Override
    public void onLeadEmailIconClicked(int i) {


        ArrayList<SalesmanDealRequestListData> leadItemArray = new ArrayList<>();

        SalesmanDealRequestListData salesmanDealRequestListData = new SalesmanDealRequestListResponse(). new SalesmanDealRequestListData();
        RequestListData requestListData = new SalesmanDealRequestListResponse(). new SalesmanDealRequestListData(). new RequestListData();
        /*{
            "dr_count": "1",
                "id": "31",
                "salesman_id": "2",
                "lead_id": "1097",
                "deal_number": "",
                "request_notes": "",
                "request_attachment": "",
                "dr_datetime": "2018-08-28 13:41:42",
                "is_read": "0",
                "customer_id": "1535476726",
                "firstname": "Sunil",
                "lastname": "Patel",
                "request_list": [
            {
                "request_id": "31",
                    "request_name": "Request#31"
            }
      ]
        }*/

        salesmanDealRequestListData.setDr_count("1");
        salesmanDealRequestListData.setId("31");
        salesmanDealRequestListData.setSalesman_id("2");
        salesmanDealRequestListData.setDeal_number("");
        salesmanDealRequestListData.setRequest_notes("Test");
        salesmanDealRequestListData.setRequest_attachment("");
        salesmanDealRequestListData.setDr_datetime("2018-08-28 13:41:42");
        salesmanDealRequestListData.setIs_read("0");
        salesmanDealRequestListData.setCustomer_id("1535476726");
        salesmanDealRequestListData.setFirstname("Sunil");
        salesmanDealRequestListData.setLastname("Patel");

        requestListData.setRequest_id("31");
        requestListData.setRequest_name("Request#31");

        ArrayList<RequestListData> arrRequestListData = new ArrayList<>();
        arrRequestListData.add(requestListData);

        salesmanDealRequestListData.setRequest_list(arrRequestListData);

        leadItemArray.add(salesmanDealRequestListData);

        Bundle bundle = new Bundle();
        bundle.putSerializable(Utils.EXTRA_DEAL_REQUEST_LIST, (Serializable) leadItemArray.get(0).getRequest_list());
        bundle.putString(Utils.EXTRA_DEAL_DETAIL_FRIEND_ID,leadItemArray.get(0).getSalesman_id());
        bundle.putString(Utils.EXTRA_DEAL_DETAIL_FRIEND_NAME,leadItemArray.get(0).getFirstname()+" "+leadItemArray.get(0).getLastname());
        DealsDescriptionFragment dealsDescriptionFragment = new DealsDescriptionFragment();
        dealsDescriptionFragment.setArguments(bundle);
        ((HomeActivity) getActivity()).replaceFragment(dealsDescriptionFragment);
    }

    @Override
    public void onLeadInactiveIconClicked(int i) {

    }

    @Override
    public void onLeadLabelClicked(int i) {

    }

    @Override
    public void onLeadScheduleIconClicked(int i) {

    }
}
