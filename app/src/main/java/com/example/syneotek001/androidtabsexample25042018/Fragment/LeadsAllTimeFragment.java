package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.VerticalLabelView;
import com.example.syneotek001.androidtabsexample25042018.model.LineGraphDataModel;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;

import java.io.Serializable;
import java.util.ArrayList;

public class LeadsAllTimeFragment extends LineGraphBaseFragment implements OnClickListener {
    ArrayList<LineGraphDataModel> leadsEntry;
    public LinearLayout llDelivery, llFollowUpLead, llInactiveLead, llNewLead, llSoldLead, llUnderContractLead;
    public TextView tvLeadNotes;

    public LinearLayout llGraph;
    public LineChart mChart;
    public TextView xAxisTitle;
    public TextView txt_leadCount;
    XAxis xAxis = new XAxis();
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_leads_all_time, container, false);
        llDelivery = mview.findViewById(R.id.llDelivery);
        llFollowUpLead = mview.findViewById(R.id.llFollowUpLead);
        llInactiveLead = mview.findViewById(R.id.llInactiveLead);
        llNewLead = mview.findViewById(R.id.llNewLead);
        llSoldLead = mview.findViewById(R.id.llSoldLead);
        llUnderContractLead = mview.findViewById(R.id.llUnderContractLead);
        tvLeadNotes = mview.findViewById(R.id.tvLeadNotes);
        llGraph = mview.findViewById(R.id.llGraph);
        mChart = mview.findViewById(R.id.mChart);
        xAxisTitle = mview.findViewById(R.id.xAxisTitle);
        txt_leadCount = mview.findViewById(R.id.txt_leadCount);
          xAxis = mChart.getXAxis();
       // xAxis.setMultiLineLabel(true);
        setBinding(mChart);
        prepareLayouts(8, 800.0f);
        setClickEvents();
        return mview;
    }

    private void setClickEvents() {
        llNewLead.setOnClickListener(this);
        llFollowUpLead.setOnClickListener(this);
        llUnderContractLead.setOnClickListener(this);
        llDelivery.setOnClickListener(this);
        llSoldLead.setOnClickListener(this);
        llInactiveLead.setOnClickListener(this);
    }

    private void prepareLayouts(int count, float range) {
        ArrayList<Entry> newLeads = new ArrayList();
        ArrayList<Entry> followUpLeads = new ArrayList();
        ArrayList<Entry> underContractLeads = new ArrayList();
        ArrayList<Entry> deliveryLeads = new ArrayList();
        ArrayList<Entry> soldLeads = new ArrayList();
        ArrayList<Entry> inactiveLeads = new ArrayList();
        for (int i = 0; i < count; i++) {
            newLeads.add(new Entry((float) i, ((float) (Math.random() * ((double) range))) + 3.0f, getResources().getDrawable(R.drawable.ic_chart_marker_yellow)));
            followUpLeads.add(new Entry((float) i, 20.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_orange)));
            underContractLeads.add(new Entry((float) i, 50.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_sky)));
            deliveryLeads.add(new Entry((float) i, 30.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_green)));
            soldLeads.add(new Entry((float) i, 10.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_pink)));
            inactiveLeads.add(new Entry((float) i, 25.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_red)));
        }
        leadsEntry = new ArrayList();
        leadsEntry.add(new LineGraphDataModel("New", ContextCompat.getColor(getActivity(), R.color.yellow_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_yellow), ContextCompat.getColor(this.mContext, R.color.yellow_opacity_50), newLeads));
        leadsEntry.add(new LineGraphDataModel("FollowUp", ContextCompat.getColor(this.mContext, R.color.orange_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_orange), ContextCompat.getColor(this.mContext, R.color.orange_opacity_50), followUpLeads));
        leadsEntry.add(new LineGraphDataModel("UnderContract", ContextCompat.getColor(this.mContext, R.color.sky_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_sky), ContextCompat.getColor(this.mContext, R.color.sky_opacity_50), underContractLeads));
        leadsEntry.add(new LineGraphDataModel("Delivery", ContextCompat.getColor(this.mContext, R.color.green_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_green), ContextCompat.getColor(this.mContext, R.color.green_opacity_50), deliveryLeads));
        leadsEntry.add(new LineGraphDataModel("Sold", ContextCompat.getColor(this.mContext, R.color.pink_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_pink), ContextCompat.getColor(this.mContext, R.color.pink_opacity_50), soldLeads));
        leadsEntry.add(new LineGraphDataModel("Inactive", ContextCompat.getColor(this.mContext, R.color.red_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_red), ContextCompat.getColor(this.mContext, R.color.red_opacity_50), inactiveLeads));
        setUpChart(2010, count + 2010, this.leadsEntry);
        mChart.getLegend().setEnabled(false);
    }

    public void onClick(View view) {
        LeadsIndividualFragment leadsIndividualFragment = new LeadsIndividualFragment();
        Bundle bundle = new Bundle();
        switch (view.getId()) {
            case R.id.llDelivery:
                bundle.putSerializable(LeadsIndividualFragment.BUNDLE_DATA_SET, (Serializable) this.leadsEntry.get(3));
                bundle.putString(LeadsIndividualFragment.BUNDLE_DATA_SET_TYPE, this.mContext.getResources().getString(R.string.delivery));
                break;
            case R.id.llFollowUpLead:
                bundle.putSerializable(LeadsIndividualFragment.BUNDLE_DATA_SET, (Serializable) this.leadsEntry.get(1));
                bundle.putString(LeadsIndividualFragment.BUNDLE_DATA_SET_TYPE, this.mContext.getResources().getString(R.string.follow_up_lead));
                break;
            case R.id.llInactiveLead:
                bundle.putSerializable(LeadsIndividualFragment.BUNDLE_DATA_SET, (Serializable) this.leadsEntry.get(5));
                bundle.putString(LeadsIndividualFragment.BUNDLE_DATA_SET_TYPE, this.mContext.getResources().getString(R.string.inactive_leads));
                break;
            case R.id.llNewLead:
                bundle.putSerializable(LeadsIndividualFragment.BUNDLE_DATA_SET, (Serializable) this.leadsEntry.get(0));
                bundle.putString(LeadsIndividualFragment.BUNDLE_DATA_SET_TYPE, this.mContext.getResources().getString(R.string.new_lead));
                break;
            case R.id.llSoldLead:
                bundle.putSerializable(LeadsIndividualFragment.BUNDLE_DATA_SET, (Serializable) this.leadsEntry.get(4));
                bundle.putString(LeadsIndividualFragment.BUNDLE_DATA_SET_TYPE, this.mContext.getResources().getString(R.string.sold_leads));
                break;
            case R.id.llUnderContractLead:
                bundle.putSerializable(LeadsIndividualFragment.BUNDLE_DATA_SET, (Serializable) this.leadsEntry.get(2));
                bundle.putString(LeadsIndividualFragment.BUNDLE_DATA_SET_TYPE, this.mContext.getResources().getString(R.string.under_contract_lead));
                break;
        }
        leadsIndividualFragment.setArguments(bundle);

        ((HomeActivity) getActivity()).replaceFragment(leadsIndividualFragment);
//        ((MainActivity) this.mActivity).pushFragment(((MainActivity) this.mActivity).mCurrentTab, leadsIndividualFragment, true, true);
    }
}
