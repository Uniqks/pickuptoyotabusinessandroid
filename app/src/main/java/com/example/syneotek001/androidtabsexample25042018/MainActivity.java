package com.example.syneotek001.androidtabsexample25042018;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.ItemAnimator;
import android.view.MenuItem;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;
import com.example.syneotek001.androidtabsexample25042018.Adapter.NavigationDrawerAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.dialog.CustomExpandableListAdapter;
import com.example.syneotek001.androidtabsexample25042018.Fragment.AnalyticHolderFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.FragmentComingSoon;
import com.example.syneotek001.androidtabsexample25042018.Utils.AppConstants;
import com.example.syneotek001.androidtabsexample25042018.Utils.BottomNavigationViewHelper;
import com.example.syneotek001.androidtabsexample25042018.Utils.BottomNavigationViewHelper1;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.databinding.ActivityMainBinding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnExpandableRecyclerViewChildItemClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.ExpandableListDataPump;
import com.example.syneotek001.androidtabsexample25042018.model.NavigationDrawerDataFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;
public class MainActivity extends BaseActivity {
    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDeta;
    public NavigationDrawerAdapter adapter;
    boolean doubleBackToExitPressedOnce = false;
    ActivityMainBinding mBinding;
    public String mCurrentTab;
    private NavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new C05692();
    private HashMap<String, Stack<Fragment>> mStacks;
    class C05681 implements OnExpandableRecyclerViewChildItemClickListener {
        C05681() {
        }

        public void onParentItemClicked(String title) {
            Logger.m6e("Parent Clicked", title);
            if (MainActivity.this.mBinding.drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
                MainActivity.this.mBinding.drawerLayout.closeDrawer((int) GravityCompat.START);
            }MainActivity.this.openClickedNavigationDrawerMenuItem(title);
        }
        public void onChildItemClicked(String title) {
            Logger.m6e("Child Clicked", title);

            if (MainActivity.this.mBinding.drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
                MainActivity.this.mBinding.drawerLayout.closeDrawer((int) GravityCompat.START);
            }
            MainActivity.this.openClickedNavigationDrawerMenuItem(title);
        }
    }
    class C05692 implements NavigationView.OnNavigationItemSelectedListener {
        C05692() {
        }public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_analytics:
                    MainActivity.this.selectedTab(AppConstants.TAB_ANALYTIC);
                    return true;
                case R.id.navigation_message:
                    MainActivity.this.selectedTab(AppConstants.TAB_MESSAGE);
                    return true;
                case R.id.navigation_profile:
                    MainActivity.this.selectedTab(AppConstants.TAB_PROFILE);
                    return true;
                case R.id.navigation_settings:
                    MainActivity.this.selectedTab(AppConstants.TAB_SETTINGS);
                    return true;
                case R.id.navigation_users:
                    MainActivity.this.selectedTab(AppConstants.TAB_USER);
                    return true;
                default:
                    return false;
            }
        }
    }

    class C05703 implements Runnable {
        C05703() {
        }

        public void run() {
            MainActivity.this.doubleBackToExitPressedOnce = false;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        //expandableListView = (ExpandableListView)findViewById(R.id.expandableListView);
        expandableListDeta = ExpandableListDataPump.getData();
        expandableListTitle = new ArrayList<String>(expandableListDeta.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(getApplicationContext(), expandableListTitle, expandableListDeta);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setAdapter(expandableListAdapter);
        prepareTabs();
        setUpNavigationDrawer();
    }
    private void prepareTabs() {
        this.mStacks = new HashMap();
        this.mStacks.put(AppConstants.TAB_PROFILE, new Stack());
        this.mStacks.put(AppConstants.TAB_ANALYTIC, new Stack());
        this.mStacks.put(AppConstants.TAB_USER, new Stack());
        this.mStacks.put(AppConstants.TAB_MESSAGE, new Stack());
        this.mStacks.put(AppConstants.TAB_SETTINGS, new Stack());
        this.mBinding.navigation.setOnNavigationItemSelectedListener((BottomNavigationView.OnNavigationItemSelectedListener) this.mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.removeShiftMode(this.mBinding.navigation);
        this.mCurrentTab = AppConstants.TAB_ANALYTIC;
        this.mBinding.navigation.setSelectedItemId(R.id.navigation_analytics);
    }
    private void setUpNavigationDrawer() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        ItemAnimator animator = this.mBinding.recyclerView.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        this.adapter = new NavigationDrawerAdapter(NavigationDrawerDataFactory.makeGenres(), new C05681());
        this.mBinding.recyclerView.setLayoutManager(layoutManager);
        this.mBinding.recyclerView.setAdapter(this.adapter);
    }
    public void toggleDrawer() {
        if (this.mBinding.drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
            this.mBinding.drawerLayout.closeDrawer((int) GravityCompat.START);
        } else {
            this.mBinding.drawerLayout.openDrawer((int) GravityCompat.START);
        }
    }
    private void selectedTab(String tabId) {
        this.mCurrentTab = tabId;
        if (((Stack) this.mStacks.get(tabId)).size() == 0) {
            boolean z = true;
            switch (tabId.hashCode()) {
                case -1893596835:
                    if (tabId.equals(AppConstants.TAB_MESSAGE)) {
                        z = true;
                        break;
                    }
                    break;
                case -970201715:
                    if (tabId.equals(AppConstants.TAB_SETTINGS)) {
                        z = true;
                        break;
                    }
                    break;
                case -906929611:
                    if (tabId.equals(AppConstants.TAB_USER)) {
                        z = true;
                        break;
                    }
                    break;
                case 865689591:
                    if (tabId.equals(AppConstants.TAB_ANALYTIC)) {
                        z = true;
                        break;
                    }
                    break;
                case 1137019647:
                    if (tabId.equals(AppConstants.TAB_PROFILE)) {
                        z = false;
                        break;
                    }
                    break;
            }
            if (!z) {
                pushFragment(tabId, new FragmentComingSoon(), false, true);
                return;

            } else {
                return;
            }
        }
        pushFragment(tabId, (Fragment) ((Stack) this.mStacks.get(tabId)).lastElement(), false, false);
    }

    public void pushFragment(String tag, Fragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd) {
            ((Stack) this.mStacks.get(tag)).push(fragment);
        }
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate) {
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        ft.replace(R.id.container, fragment);
        ft.commit();
        manager.executePendingTransactions();
    }

    public void popFragment() {
        Fragment fragment = (Fragment) ((Stack) this.mStacks.get(this.mCurrentTab)).elementAt(((Stack) this.mStacks.get(this.mCurrentTab)).size() - 2);
        ((Stack) this.mStacks.get(this.mCurrentTab)).pop();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void onBackPressed() {
        try {
            if (this.mBinding.drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
                this.mBinding.drawerLayout.closeDrawer((int) GravityCompat.START);
            } else if (((BaseFragment) ((Stack) this.mStacks.get(this.mCurrentTab)).lastElement()).onBackPressed()) {
                Logger.m5e("BackPress Manage By Fragment");
            } else if (((Stack) this.mStacks.get(this.mCurrentTab)).size() != 1) {
                popFragment();
            } else if (this.mCurrentTab.equals(AppConstants.TAB_ANALYTIC)) {
                try {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                        getSupportFragmentManager().popBackStack();
                    } else if (this.doubleBackToExitPressedOnce) {
                        finish();
                    } else {
                        this.doubleBackToExitPressedOnce = true;
                        Toast.makeText(this, getString(R.string.prompt_exit), Toast.LENGTH_LONG).show();
                        new Handler().postDelayed(new C05703(), 2000);
                    }
                } catch (Exception e) {
                    super.onBackPressed();
                }
            } else {
                this.mCurrentTab = AppConstants.TAB_ANALYTIC;
                this.mBinding.navigation.getMenu().getItem(1).setChecked(true);
                this.mBinding.navigation.setSelectedItemId(R.id.navigation_analytics);
            }
        } catch (Exception e2) {
            super.onBackPressed();
        }
    }
    private void openClickedNavigationDrawerMenuItem(String title) {
        boolean z = true;
        switch (title.hashCode()) {
            case -2101533192:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_LEAD_MANAGER_IN_ACTIVE_LEADS)) {
                    z = true;
                    break;
                }
                break;
            case -2071445918:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_EMAIL_MARKETING)) {
                    z = true;
                    break;
                }
                break;
            case -2013462102:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_LOGOUT)) {
                    z = true;
                    break;
                }
                break;
            case -1219698498:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_EMAIL_TEMPLATE)) {
                    z = true;
                    break;
                }
                break;
            case -79022321:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_CAR_BUILDER)) {
                    z = true;
                    break;
                }
                break;
            case 2245473:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_HELP)) {
                    z = true;
                    break;
                }
                break;
            case 70791782:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_EMAIL_INBOX)) {
                    z = true;
                    break;
                }
                break;
            case 92169981:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_LEAD_MANAGER_ACTIVE_LEADS)) {
                    z = false;
                    break;
                }
                break;
            case 895701579:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_LEAD_MANAGER_SOLD_LEADS)) {
                    z = true;
                    break;
                }
                break;
            case 1382865118:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_CONTACT_LIST)) {
                    z = true;
                    break;
                }
                break;
            case 1396656103:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_DEAL_MANAGER_DEAL_BUILDER)) {
                    z = true;
                    break;
                }
                break;
            case 1696047835:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_LEAD_MANAGER_ADD_NEW_LEAD)) {
                    z = true;
                    break;
                }
                break;
            case 2000971769:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_DEAL_MANAGER_DEAL_MANAGER)) {
                    z = true;
                    break;
                }
                break;
        }
        /*switch (z) {
            case false:
                pushFragment(this.mCurrentTab, new LeadListHolderFragment(), false, true);
                return;
            case true:
                SoldInactiveLeadListFragment inactiveLeadListFragment = new SoldInactiveLeadListFragment();
                Bundle bundleInactiveLead = new Bundle();
                bundleInactiveLead.putString(SoldInactiveLeadListFragment.BUNDLE_LEAD_STATUS_TYPE, getResources().getString(R.string.inactive_dead_leads));
                inactiveLeadListFragment.setArguments(bundleInactiveLead);
                pushFragment(this.mCurrentTab, inactiveLeadListFragment, false, true);
                return;
            case true:
                SoldInactiveLeadListFragment soldLeadListFragment = new SoldInactiveLeadListFragment();
                Bundle bundleSoldLead = new Bundle();
                bundleSoldLead.putString(SoldInactiveLeadListFragment.BUNDLE_LEAD_STATUS_TYPE, getResources().getString(R.string.sold_leads));
                soldLeadListFragment.setArguments(bundleSoldLead);
                pushFragment(this.mCurrentTab, soldLeadListFragment, false, true);
                return;
            case true:
                pushFragment(this.mCurrentTab, new EmailInboxListFragment(), false, true);
                return;
            case true:
                pushFragment(this.mCurrentTab, new DealManagerListFragment(), false, true);
                return;
            case true:
                pushFragment(this.mCurrentTab, new DealListHolderFragment(), true, true);
                return;
            case true:
                pushFragment(this.mCurrentTab, new ContactListFragment(), false, true);
                return;
            case true:
                pushFragment(this.mCurrentTab, new CarListHolderFragment(), false, true);
                return;
            default:
                return;
        }*/
    }
}
