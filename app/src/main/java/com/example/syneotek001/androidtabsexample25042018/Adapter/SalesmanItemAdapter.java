package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanItemModel;

import java.util.ArrayList;
import java.util.Iterator;


public class SalesmanItemAdapter extends RecyclerView.Adapter<SalesmanItemAdapter.MyViewHolder> implements OnClickListener {
    private int lastSelectedPosition = 1;
    private Context mContext;
    private ArrayList<SalesmanItemModel> mData;
    private OnLeadItemViewClickListener onItemClickListener;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivInfo:
                if (SalesmanItemAdapter.this.onItemClickListener != null) {
                    SalesmanItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            /*case R.id.ivLeadLabel:
                if (SalesmanItemAdapter.this.onItemClickListener != null) {
                    SalesmanItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;*/
            case R.id.ivEmail:
                if (SalesmanItemAdapter.this.onItemClickListener != null) {
                    SalesmanItemAdapter.this.onItemClickListener.onLeadEmailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            /*case R.id.ivEdit:
                if (SalesmanItemAdapter.this.onItemClickListener != null) {
                    SalesmanItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(lastSelectedPosition);
                    return;
                }
                return;*/
            default:
                return;
        }
    }

    class MyViewHolder extends ViewHolder implements OnClickListener {

        CardView cvLead;
        ImageView ivEmail, ivInfo, ivEdit;
        LinearLayout llActionItems;
        LinearLayout ll_Phone, ll_Location;
        TextView tvStatus, tvName, tvEmail, tvPhone, tvLocation;
        View viewDummySpace;

        MyViewHolder(View view) {
            super(view);
            cvLead = view.findViewById(R.id.cvLead);
            ivEmail = view.findViewById(R.id.ivEmail);
            ivEdit = view.findViewById(R.id.ivEdit);
            ivInfo = view.findViewById(R.id.ivInfo);
            llActionItems = view.findViewById(R.id.llActionItems);
            tvStatus = view.findViewById(R.id.tvStatus);
            tvLocation = view.findViewById(R.id.tvLocation);
            tvEmail = view.findViewById(R.id.tvEmail);
            tvName = view.findViewById(R.id.tvName);
            tvPhone = view.findViewById(R.id.tvPhone);
            viewDummySpace = view.findViewById(R.id.viewDummySpace);


            ll_Phone = view.findViewById(R.id.ll_Phone);
            ll_Location = view.findViewById(R.id.ll_Location);


        }

        @Override
        public void onClick(View v) {

        }
    }

    public SalesmanItemAdapter(Context context, ArrayList<SalesmanItemModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_salesman, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        SalesmanItemModel mLeadItem = (SalesmanItemModel) mData.get(position);
        holder.tvName.setText(mLeadItem.getFirstName() + " " + mLeadItem.getLastName());
        holder.tvEmail.setText(mLeadItem.getEmail());
        if (mLeadItem.getStatus().equals("1")) {
            holder.tvStatus.setText(mContext.getString(R.string.str_approve));
        } else if (mLeadItem.getStatus().equals("2")) {
            holder.tvStatus.setText(mContext.getString(R.string.str_reject));
        } else if (mLeadItem.getStatus().equals("3")) {
            holder.tvStatus.setText(mContext.getString(R.string.str_suspend));
        }

        holder.tvPhone.setText(mLeadItem.getPhone());
        holder.tvLocation.setText(mLeadItem.getAddress());

        Iterator it;
        ArrayList<View> viewList;
        if (mLeadItem.isSelected()) {
            viewList = new ArrayList<>();
            viewList.add(holder.ll_Phone);
            viewList.add((holder.ll_Location));
            viewList.add(holder.viewDummySpace);
            viewList.add(holder.llActionItems);
            it = viewList.iterator();

            while (it.hasNext()) {
                View view = (View) it.next();
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0.0f);
                view.animate().alpha(1.0f).setListener(null);
            }
            return;
        }
        viewList = new ArrayList<>();
        viewList.add(holder.ll_Phone);
        viewList.add(holder.ll_Location);
        viewList.add(holder.viewDummySpace);
        viewList.add(holder.llActionItems);
        it = viewList.iterator();
        while (it.hasNext()) {
            final View view = (View) it.next();
            view.animate().alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            });
        }

        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    SalesmanItemModel mLeadItem = (SalesmanItemModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);
                }
            }
        });


        holder.ivInfo.setOnClickListener(this);
        holder.ivEmail.setOnClickListener(this);
        holder.ivEdit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SalesmanItemAdapter.this.onItemClickListener != null) {
                    SalesmanItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(position);
                    return;
                }
            }
        });


    }


    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
