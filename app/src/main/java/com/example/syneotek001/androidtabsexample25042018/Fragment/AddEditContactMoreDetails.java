package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.syneotek001.androidtabsexample25042018.R;

public class AddEditContactMoreDetails extends Fragment {
    View view;
    ImageView ivback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_edit_contact_more_details, container, false);
        ivback = view.findViewById(R.id.ivBack);
        ivback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity()!=null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
        return view;
    }
}

