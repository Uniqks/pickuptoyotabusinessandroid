package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.ConatctModel;

import java.util.List;

public class CustomListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<ConatctModel> movieItems;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public CustomListAdapter(Activity activity, List<ConatctModel> movieItems) {
        this.activity = activity;
        this.movieItems = movieItems;
    }

    @Override
    public int getCount() {
        return movieItems.size();
    }

    @Override
    public Object getItem(int location) {
        return movieItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ConatctModel conatctModel = movieItems.get(position);
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.conatct_cards_layout, null);

        TextView name = (TextView) convertView.findViewById(R.id.textViewName);
        TextView email = (TextView) convertView.findViewById(R.id.email);
        TextView mobile = (TextView) convertView.findViewById(R.id.mobile);
        name.setText(conatctModel.getFirstName());
        email.setText(conatctModel.getEmail());
        mobile.setText(conatctModel.getPhone());
        return convertView;
    }

}