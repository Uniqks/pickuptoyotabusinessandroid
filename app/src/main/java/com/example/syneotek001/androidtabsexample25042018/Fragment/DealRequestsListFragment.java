package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.DealRequestsItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.DealRequestsModel;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse.SalesmanDealRequestListData;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse.SalesmanDealRequestListData.RequestListData;

import java.io.Serializable;
import java.util.ArrayList;

public class DealRequestsListFragment extends Fragment implements OnLeadItemViewClickListener {
    DealRequestsItemAdapter mAdapter;
    ArrayList<DealRequestsModel> mDealRequestList = new ArrayList<>();
    public RecyclerView recyclerView;
    public TextView tvNoDealRequestFound;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_deal_request_list, container, false);
        recyclerView = mview.findViewById(R.id.recyclerView);
        tvNoDealRequestFound = mview.findViewById(R.id.tvNoDealRequestFound);
        setUpRecyclerView();
        setHasOptionsMenu(false);
        return mview;
    }

    private void setUpRecyclerView() {
        mDealRequestList.add(new DealRequestsModel("1525325081","Sunil Patel", "1522404422", "Closing","Approved"));
        mDealRequestList.add(new DealRequestsModel("1525325137", "Sunil Patel", "1522404422","Dead","Approved"));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new DealRequestsItemAdapter(getActivity(), this.mDealRequestList,this);
        recyclerView.setAdapter(mAdapter);
        refreshData();
    }

    private void refreshData() {
        if (mDealRequestList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoDealRequestFound.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.GONE);
        tvNoDealRequestFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLeadDetailIconClicked(int i) {
        DealInfoDetailsFragment dealInfoDetailsFragment = new DealInfoDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Utils.EXTRA_DEAL_DETAIL_INFO_DEAL_ID,"35"/*leadItemArray.get(position).getDeal_id()*/);
        dealInfoDetailsFragment.setArguments(bundle);
        ((HomeActivity) getActivity()).replaceFragment(dealInfoDetailsFragment);
    }

    @Override
    public void onLeadEmailIconClicked(int i) {


        ArrayList<SalesmanDealRequestListData> leadItemArray = new ArrayList<>();

        SalesmanDealRequestListData salesmanDealRequestListData = new SalesmanDealRequestListResponse(). new SalesmanDealRequestListData();
        SalesmanDealRequestListData.RequestListData requestListData = new SalesmanDealRequestListResponse(). new SalesmanDealRequestListData(). new RequestListData();
        /*{
            "dr_count": "1",
                "id": "31",
                "salesman_id": "2",
                "lead_id": "1097",
                "deal_number": "",
                "request_notes": "",
                "request_attachment": "",
                "dr_datetime": "2018-08-28 13:41:42",
                "is_read": "0",
                "customer_id": "1535476726",
                "firstname": "Sunil",
                "lastname": "Patel",
                "request_list": [
            {
                "request_id": "31",
                    "request_name": "Request#31"
            }
      ]
        }*/

        salesmanDealRequestListData.setDr_count("1");
        salesmanDealRequestListData.setId("31");
        salesmanDealRequestListData.setSalesman_id("2");
        salesmanDealRequestListData.setDeal_number("");
        salesmanDealRequestListData.setRequest_notes("Test");
        salesmanDealRequestListData.setRequest_attachment("");
        salesmanDealRequestListData.setDr_datetime("2018-08-28 13:41:42");
        salesmanDealRequestListData.setIs_read("0");
        salesmanDealRequestListData.setCustomer_id("1535476726");
        salesmanDealRequestListData.setFirstname("Sunil");
        salesmanDealRequestListData.setLastname("Patel");

        requestListData.setRequest_id("31");
        requestListData.setRequest_name("Request#31");

        ArrayList<RequestListData> arrRequestListData = new ArrayList<>();
        arrRequestListData.add(requestListData);

        salesmanDealRequestListData.setRequest_list(arrRequestListData);

        leadItemArray.add(salesmanDealRequestListData);

        Bundle bundle = new Bundle();
        bundle.putSerializable(Utils.EXTRA_DEAL_REQUEST_LIST, (Serializable) leadItemArray.get(0).getRequest_list());
        bundle.putString(Utils.EXTRA_DEAL_DETAIL_FRIEND_ID,leadItemArray.get(0).getSalesman_id());
        bundle.putString(Utils.EXTRA_DEAL_DETAIL_FRIEND_NAME,leadItemArray.get(0).getFirstname()+" "+leadItemArray.get(0).getLastname());
        DealsDescriptionFragment dealsDescriptionFragment = new DealsDescriptionFragment();
        dealsDescriptionFragment.setArguments(bundle);
        ((HomeActivity) getActivity()).replaceFragment(dealsDescriptionFragment);
    }

    @Override
    public void onItemClicked(int i) {

    }

    @Override
    public void onLeadBusinessRequestClicked(int i) {

    }

    @Override
    public void onLeadDealRequestClicked(int i) {

    }

    @Override
    public void onLeadInactiveIconClicked(int i) {

    }

    @Override
    public void onLeadLabelClicked(int i) {

    }

    @Override
    public void onLeadScheduleIconClicked(int i) {

    }
}
