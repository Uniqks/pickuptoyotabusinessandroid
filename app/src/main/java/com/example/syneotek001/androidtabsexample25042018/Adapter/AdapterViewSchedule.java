package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.github.sundeepk.compactcalendarview.domain.Event.EventData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by bluegenie-24 on 24/7/18.
 */

public class AdapterViewSchedule extends BaseAdapter {
    LayoutInflater inflater;
    ArrayList<EventData> arrTasks;
    Context context;
    TaskClick taskClick;

    public interface TaskClick{
        void onTaskClick(int position);
    }

    public void setTaskClick(TaskClick taskClick){
        this.taskClick = taskClick;
    }


    public AdapterViewSchedule(Context context, ArrayList<EventData> arrTasks) {
        this.context = context;
        this.arrTasks = arrTasks;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return arrTasks.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.adapter_view_schedule, parent, false);
        Log.e("AdapterViewTask", "getView " + arrTasks.get(position).getUname());

        SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());

        RelativeLayout rlTaskItem;
        TextView tvEventTime, tvName;

        rlTaskItem = convertView.findViewById(R.id.rlTaskItem);
        tvEventTime = convertView.findViewById(R.id.tvEventTime);
        tvName = convertView.findViewById(R.id.tvName);

        if (arrTasks.get(position).getTaskName() != null && !arrTasks.get(position).getTaskName().equals("")) {
            tvName.setText(arrTasks.get(position).getTaskName());
        }


        if (arrTasks.get(position).getTaskStartTime()!=null && !arrTasks.get(position).getTaskStartTime().equals("")
                && arrTasks.get(position).getTaskEndTime()!=null && !arrTasks.get(position).getTaskEndTime().equals("")){
            SimpleDateFormat src = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            SimpleDateFormat dest = new SimpleDateFormat("dd MMM hh:mm a");
            Date startTime = null;
            Date endTime = null;
            try {
                startTime = src.parse(arrTasks.get(position).getTaskStartTime());
                endTime = src.parse(arrTasks.get(position).getTaskEndTime());
            } catch (ParseException e) {
                //handle exception
            }
            String strStartTime = dest.format(startTime);
            String strEndTime = dest.format(endTime);
            tvEventTime.setText(strStartTime+"\n"+strEndTime);
        }

        rlTaskItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskClick.onTaskClick(position);
            }
        });

        return convertView;
    }
}
