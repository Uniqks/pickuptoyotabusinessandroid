package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import com.example.syneotek001.androidtabsexample25042018.R;


public class CalinderViewFragment extends Fragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_calenderview, container, false);
        CalendarView simpleCalendarView = (CalendarView)mview.findViewById(R.id.simpleCalendarView);
        return mview;
    }




}
