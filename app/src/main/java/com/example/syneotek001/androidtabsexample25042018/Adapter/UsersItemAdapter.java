package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.UsersItemModel;

import java.util.ArrayList;

public class UsersItemAdapter extends Adapter<UsersItemAdapter.MyViewHolder> implements OnClickListener  {
    private Context mContext;
    private ArrayList<UsersItemModel> mData = new ArrayList();
    private OnLeadItemViewClickListener onItemClickListener;
    private int lastSelectedPosition = 1;


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLeadLabel:
                if (UsersItemAdapter.this.onItemClickListener != null) {
                    UsersItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;
            default:
                return;
        }
    }

    class MyViewHolder extends ViewHolder {
         TextView tvUserName,tvDateCreated,tvType;
        MyViewHolder(View view) {
            super(view);
            tvUserName=view.findViewById(R.id.tvUserName);
            tvDateCreated=view.findViewById(R.id.tvDateCreated);
            tvType=view.findViewById(R.id.tvType);
        }
    }

    public UsersItemAdapter(Context context, ArrayList<UsersItemModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    /*public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder((ItemLeadSoldInactiveBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_lead_sold_inactive, parent, false));
    }*/

    @NonNull
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_users, parent, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        UsersItemModel mLeadItem = mData.get(position);
        holder.tvUserName.setText(mLeadItem.getUserName());
        holder.tvDateCreated.setText(mLeadItem.getDateCreated());
        holder.tvType.setText(mLeadItem.getUserType());

        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClicked(position);
                }
            }
        });

    }
    
    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
