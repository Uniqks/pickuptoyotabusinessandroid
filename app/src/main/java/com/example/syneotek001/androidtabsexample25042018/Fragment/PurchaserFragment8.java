package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentPruchaser7Binding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentPurchaser2Binding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentPurchaser8Binding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class PurchaserFragment8 extends Fragment implements OnClickListener, OnBackPressed {
    public Button btnSave, btnSaveAndNext;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;
    FragmentPurchaser8Binding binding;
    String name, email, mobile, password, address;
    YoYo yoYo;
    ArrayList<String> arr_salesman_status = new ArrayList<>();

    Typeface typeface;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_purchaser8, container, false);
        View view = binding.getRoot();

        typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/montserrat_regular.otf");

        TextPaint tp = new TextPaint();
        tp.linkColor = getResources().getColor(R.color.colorPrimary);           //not quite sure what the format should be
        UnderlineSpan us = new UnderlineSpan();
        us.updateDrawState(tp);
        SpannableString content = new SpannableString(getString(R.string.txt_terms_privacy));
        content.setSpan(us, 0, content.length(), 0);

        binding.llStartDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int year;
                int month;
                int day;
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                monthOfYear = monthOfYear + 1;
                                String monthString = monthOfYear < 10 ? "0" + monthOfYear : "" + monthOfYear;
                                String dayString = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
                                binding.txtStartDate.setText(year + "-" + monthString + "-" + dayString);
//                                txtStartDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                                dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            }
                        }, year, month, day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        binding.ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });





     /*   binding.rbCash.setTypeface(typeface);
        binding.rbCheque.setTypeface(typeface);
        binding.rbCityOfToronto.setTypeface(typeface);
        binding.rbCreditCard.setTypeface(typeface);
        binding.rbDebit.setTypeface(typeface);
        binding.rbNewPlates.setTypeface(typeface);
        binding.rbTransfer.setTypeface(typeface);*/



        setHasOptionsMenu(false);
        setClickEvents();

       setAnimation(binding.lblPaymentMethod);
        setAnimation(binding.lblTerm);
        setAnimation(binding.lblAmortization);
        setAnimation(binding.lbWithOne);
        setAnimation(binding.lblStartDate);

      /*   setAnimation(binding.etAmountDueOnDelivery);
        setAnimation(binding.etAmountFinanced);
        setAnimation(binding.etDeposit);
        setAnimation(binding.etHSTRegNo);
        setAnimation(binding.etHSTOnTotalPrice);
        setAnimation(binding.etLicenseFee);
        setAnimation(binding.etTotalPurchasePrice);
        setAnimation(binding.etTotalVehiclePrice);
        setAnimation(binding.etTradeInAllowance);
        setAnimation(binding.etPayoutOnLiens);
        setAnimation(binding.etFuel);*/


        arr_salesman_status = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_salesman_status)));


        return view;
    }



    public void setAnimation(View view) {
        YoYo.with(Techniques.FlipInX).duration(1000).delay(150).repeat(0).playOn(view);
    }

    private void setClickEvents() {
   binding.btnSaveAndNext.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSaveAndNext:
                Log.e("btnSaveAndNext","btnSaveAndNext");
                PurchaserFragment9 billSaleFragment2 = new PurchaserFragment9();
                ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
              /*  if (getArguments() != null) {
                    if (getArguments().getBoolean("is_edit")) {
                        if (getArguments().getSerializable("salesman_profile") != null) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("salesman_profile", getArguments().getSerializable("salesman_profile"));
                            bundle.putBoolean("is_edit", true);
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    } else {
                        if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("is_edit", false);
                            bundle.putString("profile_type",getArguments().getString("profile_type"));
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    }
                }*/
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
