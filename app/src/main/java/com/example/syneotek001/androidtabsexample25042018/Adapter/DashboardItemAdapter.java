package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;


import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnRecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class DashboardItemAdapter extends RecyclerView.Adapter<DashboardItemAdapter.MyViewHolder> {
    private int lastSelectedPosition = -1;
    private Context mContext;
    private List<String> mData = new ArrayList();
    private OnRecyclerViewItemClickListener onItemClickListener;

    class MyViewHolder extends ViewHolder {
        TextView tvNavigationDrawerDay;

        MyViewHolder(View mview) {
            super(mview);
            tvNavigationDrawerDay = mview.findViewById(R.id.tvNavigationDrawerDay);
        }
    }

    public DashboardItemAdapter(Context mContext, ArrayList<String> list, OnRecyclerViewItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dashboard, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.tvNavigationDrawerDay.setText((CharSequence) mData.get(position));
        holder.tvNavigationDrawerDay.setSelected(this.lastSelectedPosition == position);
        holder.tvNavigationDrawerDay.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClicked(position);
                    lastSelectedPosition = position;
                    DashboardItemAdapter.this.notifyDataSetChanged();
                }
            }
        });
    }

    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        lastSelectedPosition = position;
    }
}
