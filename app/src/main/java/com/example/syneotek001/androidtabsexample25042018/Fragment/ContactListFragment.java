package com.example.syneotek001.androidtabsexample25042018.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.CustomListAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.ConatctModel;

import java.util.ArrayList;
import java.util.List;

public class ContactListFragment extends Fragment {
    View rootview;
    private List<ConatctModel> contactList = new ArrayList<ConatctModel>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview=  inflater.inflate(R.layout.fragment_contact_list, container, false);

        ListView listView = rootview.findViewById(R.id.list);

        ConatctModel mContactModel = new ConatctModel();
        mContactModel.setFirstName("Abc");
        mContactModel.setLastName("xyz");
        mContactModel.setId("1");
        mContactModel.setEmail("abcxyz@gmail.com");
        mContactModel.setPhone("789456123");
        contactList.add(mContactModel);
        CustomListAdapter adapter = new CustomListAdapter(getActivity(), contactList);
        listView.setAdapter(adapter);
        return  rootview;
    }
}
