package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.syneotek001.androidtabsexample25042018.Adapter.DealManagerItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.DealManagerModel;

import java.util.ArrayList;

public class DealManagerListFragment extends Fragment implements OnBackPressed {
    ArrayList<DealManagerModel> dealManagerList = new ArrayList<>();
    DealManagerItemAdapter mAdapter;
    public RecyclerView recyclerView;
    public LinearLayout ll_NoRecordFound;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_deal_manager_list, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        ll_NoRecordFound = view.findViewById(R.id.ll_NoRecordFound);
        ImageView ivBack=view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        setUpRecyclerView();

        return view;
    }

    private void setUpRecyclerView() {
        dealManagerList.add(new DealManagerModel("1", "152244893677", "Abc Patel", 1503061200, 12));
        dealManagerList.add(new DealManagerModel("2", "152244893477", "Xyz test", 1508751200, 0));
        dealManagerList.add(new DealManagerModel("3", "152244893489", "Pqr Shah", 1490101200, 0));
        dealManagerList.add(new DealManagerModel("4", "152244891563", "qwerty Panchal", 1524754800, 2));
        dealManagerList.add(new DealManagerModel("5", "152244891123", "Paresh Prajapati", 1518894000, 5));
        dealManagerList.add(new DealManagerModel("6", "152244891123", "Raj Sadhu", 1464955200, 8));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_right));
//        mAdapter = new DealManagerItemAdapter(getActivity(), this.dealManagerList);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
