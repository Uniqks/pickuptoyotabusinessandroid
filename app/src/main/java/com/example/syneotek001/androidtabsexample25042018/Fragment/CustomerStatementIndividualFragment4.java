package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentCustomerStatementIndividual3Binding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentCustomerStatementIndividual4Binding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

import java.util.ArrayList;
import java.util.Arrays;

public class CustomerStatementIndividualFragment4 extends Fragment implements OnClickListener, OnBackPressed {
    public Button btnSave, btnSaveAndNext;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;
     //FragmentSalesmanProfileStep1Binding binding;
    FragmentCustomerStatementIndividual4Binding binding;
    String name, email, mobile, password, address;
    YoYo yoYo;
    ArrayList<String> arr_salesman_status = new ArrayList<>();

    Typeface typeface;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_customer_statement_individual4, container, false);
        View view = binding.getRoot();

        typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/montserrat_regular.otf");

 /*       btnSave = rootview.findViewById(R.id.btnSave);
        btnSaveAndNext = rootview.findViewById(R.id.btnSaveAndNext);
        etEmail = rootview.findViewById(R.id.etEmail);
        etFirstName = rootview.findViewById(R.id.etFirstName);
        etHomePhone = rootview.findViewById(R.id.etHomePhone);
        etLastName = rootview.findViewById(R.id.etLastName);
        etMobile = rootview.findViewById(R.id.etMobile);
        etTitle = rootview.findViewById(R.id.etTitle);
        etWorkFax = rootview.findViewById(R.id.etWorkFax);
        etWorkPhone = rootview.findViewById(R.id.etWorkPhone);
        ivContact = rootview.findViewById(R.id.ivContact);
        ivEmail = rootview.findViewById(R.id.ivEmail);
        ivMobile = rootview.findViewById(R.id.ivMobile);
        ivTitle = rootview.findViewById(R.id.ivTitle);
        tvPageIndicator = rootview.findViewById(R.id.tvPageIndicator);
        tvTitle = rootview.findViewById(R.id.tvTitle);
        ivBack = rootview.findViewById(R.id.ivBack);*/
        binding.ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });

        binding.rbBankTruptcyNo.setTypeface(typeface);
        binding.rbBankTruptcyNo.setTypeface(typeface);
        binding.rbBankTruptcyYes.setTypeface(typeface);
        binding.rbCarOrMerchandiseNo.setTypeface(typeface);
        binding.rbCarOrMerchandiseYes.setTypeface(typeface);
        binding.rbChequingBalance.setTypeface(typeface);
        binding.rbNo.setTypeface(typeface);
        binding.rbSavingBalance.setTypeface(typeface);
        binding.rbTermDeposit.setTypeface(typeface);
        binding.rbValidDriverLicenseNo.setTypeface(typeface);
        binding.rbValidDriverLicenseYes.setTypeface(typeface);
        binding.rbYes.setTypeface(typeface);


        setHasOptionsMenu(false);
        setClickEvents();

        setAnimation(binding.lblAccountantName);
        setAnimation(binding.lblAccountantPhone);
        setAnimation(binding.lblAccountNumber);
        setAnimation(binding.lblBalanceType);
        setAnimation(binding.lblBankAddress);
        setAnimation(binding.lblBankName);
        setAnimation(binding.lblIsCarOrMerchandise);
        setAnimation(binding.lblIsFiledForBankTruptcy);
        setAnimation(binding.lblIsTCCICustomer);
        setAnimation(binding.lblIsValidDriverLicense);
        setAnimation(binding.lblPreviousVehicleFinancedBy);
        setAnimation(binding.lblTransitNumber);

        setAnimation(binding.etAccountantName);
        setAnimation(binding.etAccountantPhone);
        setAnimation(binding.etAccountNumber);
        setAnimation(binding.etBankAddress);
        setAnimation(binding.etBankName);
        setAnimation(binding.etPreviousVehicleFinancedBy);
        setAnimation(binding.etTransitNumber);

        arr_salesman_status = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_salesman_status)));


        return view;
    }



    public void setAnimation(View view) {
        YoYo.with(Techniques.FlipInX).duration(1000).delay(150).repeat(0).playOn(view);
    }

    private void setClickEvents() {
        binding.btnSaveAndNext.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSaveAndNext:
                Log.e("btnSaveAndNext","btnSaveAndNext");
                CustomerStatementIndividualFragment5 billSaleFragment2 = new CustomerStatementIndividualFragment5();
                 ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
              /*  if (getArguments() != null) {
                    if (getArguments().getBoolean("is_edit")) {
                        if (getArguments().getSerializable("salesman_profile") != null) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("salesman_profile", getArguments().getSerializable("salesman_profile"));
                            bundle.putBoolean("is_edit", true);
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    } else {
                        if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("is_edit", false);
                            bundle.putString("profile_type",getArguments().getString("profile_type"));
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    }
                }*/
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
