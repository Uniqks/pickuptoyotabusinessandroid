package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.syneotek001.androidtabsexample25042018.Adapter.LeadsStatusSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.SelectSalesmanSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;

import java.util.ArrayList;
import java.util.Arrays;


public class LeadAssignFragment extends Fragment {

    ImageView iv_Close;
    Spinner spinnerSalesman;
    ArrayList<String> arr_schedule_categories = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_lead_assign, container, false);
        iv_Close = view.findViewById(R.id.iv_Close);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        spinnerSalesman = view.findViewById(R.id.spinnerSalesman);
        arr_schedule_categories = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_dummy_salesman)));
        initCustomSpinner(arr_schedule_categories);
        spinnerSalesman.setSelection(1);

        return view;
    }

    private void initCustomSpinner(ArrayList<String> countries) {

        SelectSalesmanSpinnerAdapter customSpinnerAdapter = new SelectSalesmanSpinnerAdapter(getActivity(), countries);
        spinnerSalesman.setAdapter(customSpinnerAdapter);
        spinnerSalesman.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


}
