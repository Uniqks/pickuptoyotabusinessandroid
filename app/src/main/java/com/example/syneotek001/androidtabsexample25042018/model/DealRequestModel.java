package com.example.syneotek001.androidtabsexample25042018.model;

public class DealRequestModel {
    private String customerId = "";
    private String salesManName = "";
    private String status = "";

    public DealRequestModel(String salesManName, String customerId, String status) {
        this.salesManName = salesManName;
        this.customerId = customerId;
        this.status = status;
    }

    public String getSalesManName() {
        return this.salesManName;
    }

    public void setSalesManName(String salesManName) {
        this.salesManName = salesManName;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString() {
        return "DealRequestModel{salesManName='" + this.salesManName + '\'' + ", customerId='" + this.customerId + '\'' + ", status='" + this.status + '\'' + '}';
    }
}
