package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.graphics.Color;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.AdapterViewSchedule;
import com.example.syneotek001.androidtabsexample25042018.Adapter.ScheduleSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Ui.weekview.DateTimeInterpreter;
import com.example.syneotek001.androidtabsexample25042018.Ui.weekview.MonthLoader;
import com.example.syneotek001.androidtabsexample25042018.Ui.weekview.WeekView;
import com.example.syneotek001.androidtabsexample25042018.Ui.weekview.WeekViewEvent;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.github.sundeepk.compactcalendarview.domain.Event.EventData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;


public class FragmentSchedule extends Fragment implements OnBackPressed, AdapterViewSchedule.TaskClick ,WeekView.EventClickListener, MonthLoader.MonthChangeListener, WeekView.EventLongPressListener, WeekView.EmptyViewLongPressListener {


    private static final int TYPE_DAY_VIEW = 1;
    private static final int TYPE_WEEK_VIEW = 3;
    private static final int TYPE_MONTH_VIEW = 4;
    WeekView mWeekView;
    ImageView ivAdd,ivBack;
    private int mWeekViewType = TYPE_WEEK_VIEW;
    RelativeLayout rlCalendarMonth;

    CompactCalendarView compactCalendarView;
    private static final String TAG = "ViewTaskFragment";
    TextView tvDate,tvTitle,tvSelectedDate;
    ImageView ivPrevious,ivNext;
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM yyyy", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth1 = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
    private boolean shouldShow = false;
    final ArrayList<String> mutableBookings = new ArrayList<>();
    final ArrayList<EventData> arrTasks = new ArrayList<>();
    AdapterViewSchedule adapter;
    Spinner spinnerScheduleCategory;
    ArrayList<String> arr_schedule_categories = new ArrayList<>();



    public FragmentSchedule() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_schedule, container, false);

        // Inflate the layout for this fragment
        // Get a reference for the week view in the layout.
        mWeekView = view.findViewById(R.id.weekView);
        tvTitle = view.findViewById(R.id.tvTitle);
        ivAdd = view.findViewById(R.id.ivAdd);
        ivBack = view.findViewById(R.id.ivBack);
        rlCalendarMonth = view.findViewById(R.id.rlCalendarMonth);

        spinnerScheduleCategory = view.findViewById(R.id.spinnerScheduleCategory);
        arr_schedule_categories = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_schedule_categories)));
        initCustomSpinner(arr_schedule_categories);
        spinnerScheduleCategory.setSelection(1);

        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).replaceFragment(new AddNewSheduleFragment());
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });

        tvTitle.setText(getString(R.string.str_schedule));

        // Show a toast message about the touched event.
        mWeekView.setOnEventClickListener(FragmentSchedule.this);

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(FragmentSchedule.this);

        // Set long press listener for events.
        mWeekView.setEventLongPressListener(FragmentSchedule.this);

        // Set long press listener for empty view
        mWeekView.setEmptyViewLongPressListener(FragmentSchedule.this);

        // Set up a date time interpreter to interpret how the date and time will be formatted in
        // the week view. This is optional.
        setupDateTimeInterpreter(false);

        tvSelectedDate = view.findViewById(R.id.tvSelectedDate);
        tvDate = view.findViewById(R.id.tvDate);
        ivPrevious = view.findViewById(R.id.ivPrevious);
        ivNext = view.findViewById(R.id.ivNext);

        final ListView bookingsListView = view.findViewById(R.id.bookings_listview);
//        final ArrayAdapter adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, mutableBookings);
//        bookingsListView.setAdapter(adapter);

        adapter = new AdapterViewSchedule(getActivity(),arrTasks);
        adapter.setTaskClick(FragmentSchedule.this);
        bookingsListView.setAdapter(adapter);

        compactCalendarView = view.findViewById(R.id.compactcalendar_view);

        compactCalendarView.setUseThreeLetterAbbreviation(false);
        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);
        compactCalendarView.setIsRtl(false);
        compactCalendarView.displayOtherMonthDays(false);

        loadEvents();
        loadEventsForYear(2018);
        compactCalendarView.invalidate();

        logEventsByMonth(compactCalendarView);

        tvDate.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()).toUpperCase());
        tvSelectedDate.setText(dateFormatForMonth1.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {

                onDateClick(dateClicked);

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                tvDate.setText(dateFormatForMonth.format(firstDayOfNewMonth).toUpperCase());
                tvSelectedDate.setText(dateFormatForMonth1.format(firstDayOfNewMonth));
            }
        });

        ivPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.scrollLeft();
            }
        });

        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.scrollRight();
            }
        });



        onDateClick(new Date());
        Calendar calendar;
//        onMonthChange(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)+1);
        Log.e("FragmentSchedule","onCreateView mWeekViewType " +mWeekViewType+"");
        if (mWeekViewType == TYPE_MONTH_VIEW) {
            Log.e("FragmentSchedule","onCreateView"+" if (rlCalendarMonth.getVisibility() == View.VISIBLE)");
            calendar = Calendar.getInstance();
          onMonthChange(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)+1);
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void initCustomSpinner(ArrayList<String> countries) {

        ScheduleSpinnerAdapter customSpinnerAdapter = new ScheduleSpinnerAdapter(getActivity(), countries);
        spinnerScheduleCategory.setAdapter(customSpinnerAdapter);
        spinnerScheduleCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("FragmentSchedule","initCustomSpinner " +rlCalendarMonth.getVisibility()+"");
                if (position == 0){
                    /*if (mWeekViewType != TYPE_MONTH_VIEW)*/ {
                        rlCalendarMonth.setVisibility(View.VISIBLE);
                        mWeekView.setVisibility(View.GONE);
                        mWeekViewType = TYPE_MONTH_VIEW;
                    }

                } else if (position == 1){
                    if (rlCalendarMonth.getVisibility() == View.VISIBLE) {
                        rlCalendarMonth.setVisibility(View.GONE);
                        mWeekView.setVisibility(View.VISIBLE);
                    }
                    if (mWeekViewType != TYPE_WEEK_VIEW) {
//                        item.setChecked(!item.isChecked());
                        mWeekViewType = TYPE_WEEK_VIEW;
                        mWeekView.setNumberOfVisibleDays(7);

                        // Lets change some dimensions to best fit the view.
                        mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                        mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                        mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                    }
                } else if (position == 2){
                    if (rlCalendarMonth.getVisibility() == View.VISIBLE) {
                        rlCalendarMonth.setVisibility(View.GONE);
                        mWeekView.setVisibility(View.VISIBLE);
                    }
                    if (mWeekViewType != TYPE_DAY_VIEW) {
//                        item.setChecked(!item.isChecked());
                        mWeekViewType = TYPE_DAY_VIEW;
                        mWeekView.setNumberOfVisibleDays(1);

                        // Lets change some dimensions to best fit the view.
                        mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                        mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                        mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    public void onDateClick(Date dateClicked) {
        tvDate.setText(dateFormatForMonth.format(dateClicked).toUpperCase());
        tvSelectedDate.setText(dateFormatForMonth1.format(dateClicked));
        List<Event> bookingsFromMap = compactCalendarView.getEvents(dateClicked);
        Log.d(TAG, "inside onclick " + dateFormatForDisplaying.format(dateClicked));
        if (bookingsFromMap != null) {
            Log.d(TAG, bookingsFromMap.toString());
//                    mutableBookings.clear();
            arrTasks.clear();
            for (Event booking : bookingsFromMap) {
//                        mutableBookings.add((String) booking.getData());
                arrTasks.add(booking.getEventData());
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void logEventsByMonth(CompactCalendarView compactCalendarView) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        currentCalender.set(Calendar.MONTH, Calendar.AUGUST);
        List<String> dates = new ArrayList<>();
        for (Event e : compactCalendarView.getEventsForMonth(new Date())) {
            dates.add(dateFormatForDisplaying.format(e.getTimeInMillis()));
        }
        Log.d(TAG, "Events for Aug with simple date formatter: " + dates);
        Log.d(TAG, "Events for Aug month using default local and timezone: " + compactCalendarView.getEventsForMonth(currentCalender.getTime()));
    }

    private void loadEvents() {
//        addEvents(-1, -1);
//        addEvents(Calendar.DECEMBER, -1);
//        addEvents(Calendar.AUGUST, -1);
    }

    private void loadEventsForYear(int year) {
//        addEvents(Calendar.DECEMBER, year);
//        addEvents(Calendar.AUGUST, year);
    }

    private void addEvents(int month, int year) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        for (int i = 0; i < 6; i++) {
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(Calendar.MONTH, month);
            }
            if (year > -1) {
                currentCalender.set(Calendar.ERA, GregorianCalendar.AD);
                currentCalender.set(Calendar.YEAR, year);
            }
            currentCalender.add(Calendar.DATE, i);
//            setToMidnight(currentCalender);
            long timeInMillis = currentCalender.getTimeInMillis();

            List<Event> events = getEvents(timeInMillis, i);

            compactCalendarView.addEvents(events);
        }

        List<Event> events = getEvents(new Date().getTime(), 25);

        compactCalendarView.addEvents(events);

    }

    private List<Event> getEvents(long timeInMillis, int day) {
        if (day < 2) {
            return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"John Kumar","male","1",false)));
        } else if ( day > 2 && day <= 4) {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"Jansi Rani","female","2",false)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"Aarya","male","3",false)));
        } else {
            return Arrays.asList(
                    new Event(Color.argb(255, 89, 219, 224), timeInMillis, "Event at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"Roja","female","1",false))/*,
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"Kiruba","male","2",false)),
                    new Event(Color.argb(255, 70, 68, 65), timeInMillis, "Event 3 at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"Shalini","female","3",false))*/);
        }
    }

    @Override
    public void onTaskClick(int position) {
        /*for (int i=0;i<arrTasks.size();i++) {
            if (i!=position)
                arrTasks.get(i).setIs_selected(false);
        }
        arrTasks.get(position).setIs_selected(true);
        adapter.notifyDataSetChanged();*/

        ((HomeActivity) getActivity()).replaceFragment(new ViewScheduleFragment());

    }

    public void show_popup() {


        PopupMenu popup = new PopupMenu(getActivity(), ivAdd);
        popup.getMenuInflater().inflate(R.menu.menu_calendar, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.month:
                        rlCalendarMonth.setVisibility(View.VISIBLE);
                        mWeekView.setVisibility(View.GONE);
                        break;
                    case R.id.week:
                        if (rlCalendarMonth.getVisibility() == View.VISIBLE) {
                            rlCalendarMonth.setVisibility(View.GONE);
                            mWeekView.setVisibility(View.VISIBLE);
                        }
                        if (mWeekViewType != TYPE_WEEK_VIEW) {
                            item.setChecked(!item.isChecked());
                            mWeekViewType = TYPE_WEEK_VIEW;
                            mWeekView.setNumberOfVisibleDays(7);

                            // Lets change some dimensions to best fit the view.
                            mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                            mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                            mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                        }
                        break;
                    case R.id.day:
                        if (rlCalendarMonth.getVisibility() == View.VISIBLE) {
                            rlCalendarMonth.setVisibility(View.GONE);
                            mWeekView.setVisibility(View.VISIBLE);
                        }
                        if (mWeekViewType != TYPE_DAY_VIEW) {
                            item.setChecked(!item.isChecked());
                            mWeekViewType = TYPE_DAY_VIEW;
                            mWeekView.setNumberOfVisibleDays(1);

                            // Lets change some dimensions to best fit the view.
                            mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                            mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                            mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                        }
                        break;
                }

                return true;
            }
        });

        popup.show();


    }

    private void setupDateTimeInterpreter(final boolean shortDate) {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" M/d", Locale.getDefault());

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
                return hour > 11 ? (hour - 12) + " PM" : (hour == 0 ? "12 AM" : hour + " AM");
            }
        });
    }

    protected String getEventTitle(Calendar time) {
        return String.format("Event of %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {

        ((HomeActivity) getActivity()).replaceFragment(new ViewScheduleFragment());

//        Toast.makeText(getActivity(), event.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
//        Toast.makeText(getActivity(), "Long pressed event: " + event.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmptyViewLongPress(Calendar time) {
//        Toast.makeText(getActivity(), "Empty view long pressed: " + getEventTitle(time), Toast.LENGTH_SHORT).show();
    }

    public WeekView getWeekView() {
        return mWeekView;
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        // Populate the week view with some events.

        Log.e("FragmentSchedule","onMonthChange newYear "+newYear+" newMonth "+newMonth);

        List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
        List<Event> eventsMonthView = new ArrayList<Event>();

        Calendar startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 1);
        startTime.add(Calendar.DAY_OF_WEEK, 1);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        Calendar endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR, 2);
//        endTime.add(Calendar.DAY_OF_WEEK, 1);
        endTime.set(Calendar.MONTH, newMonth - 1);
        WeekViewEvent event = new WeekViewEvent(1, /*getEventTitle(startTime)*/"Test Task 1", startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_01));
        events.add(event);
        eventsMonthView.add(new Event(Color.argb(255, 89, 219, 224), startTime.getTimeInMillis(), "Event at " + new Date(startTime.getTimeInMillis()),new Event().new EventData("Test Task 1",""+new Date(startTime.getTimeInMillis()),""+new Date(endTime.getTimeInMillis()))));

        startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 4);
        startTime.add(Calendar.DAY_OF_WEEK, 2);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR, 2);
//        endTime.add(Calendar.DAY_OF_WEEK, 1);
//        endTime.set(Calendar.MONTH, newMonth - 1);
        event = new WeekViewEvent(1, /*getEventTitle(startTime)*/"Test Task 2", startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_02));
        events.add(event);
        eventsMonthView.add(new Event(Color.argb(255, 245, 127, 104), startTime.getTimeInMillis(), "Event at " + new Date(startTime.getTimeInMillis()),new Event().new EventData("Test Task 2",""+new Date(startTime.getTimeInMillis()),""+new Date(endTime.getTimeInMillis()))));

        startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 7);
        startTime.add(Calendar.DAY_OF_WEEK, 3);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR, 2);
//        endTime.add(Calendar.DAY_OF_WEEK, 1);
//        endTime.set(Calendar.MONTH, newMonth - 1);
        event = new WeekViewEvent(1, /*getEventTitle(startTime)*/"Test Task 3", startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_03));
        events.add(event);
        eventsMonthView.add(new Event(Color.argb(255, 135, 210, 136), startTime.getTimeInMillis(), "Event at " + new Date(startTime.getTimeInMillis()),new Event().new EventData("Test Task 3",""+new Date(startTime.getTimeInMillis()),""+new Date(endTime.getTimeInMillis()))));


        compactCalendarView.addEvents(eventsMonthView);

        /*startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 3);
        startTime.set(Calendar.MINUTE, 30);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.set(Calendar.HOUR_OF_DAY, 4);
        endTime.set(Calendar.MINUTE, 30);
        endTime.set(Calendar.MONTH, newMonth - 1);
        event = new WeekViewEvent(10, getEventTitle(startTime), startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_02));
        events.add(event);

        startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 4);
        startTime.set(Calendar.MINUTE, 20);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.set(Calendar.HOUR_OF_DAY, 5);
        endTime.set(Calendar.MINUTE, 0);
        event = new WeekViewEvent(10, getEventTitle(startTime), startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_03));
        events.add(event);

        startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 5);
        startTime.set(Calendar.MINUTE, 30);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR_OF_DAY, 2);
        endTime.set(Calendar.MONTH, newMonth - 1);
        event = new WeekViewEvent(2, getEventTitle(startTime), startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_02));
        events.add(event);

        startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 5);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        startTime.add(Calendar.DATE, 1);
        endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR_OF_DAY, 3);
        endTime.set(Calendar.MONTH, newMonth - 1);
        event = new WeekViewEvent(3, getEventTitle(startTime), startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_03));
        events.add(event);

        startTime = Calendar.getInstance();
        startTime.set(Calendar.DAY_OF_MONTH, 15);
        startTime.set(Calendar.HOUR_OF_DAY, 3);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR_OF_DAY, 3);
        event = new WeekViewEvent(4, getEventTitle(startTime), startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_04));
        events.add(event);

        startTime = Calendar.getInstance();
        startTime.set(Calendar.DAY_OF_MONTH, 1);
        startTime.set(Calendar.HOUR_OF_DAY, 3);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR_OF_DAY, 3);
        event = new WeekViewEvent(5, getEventTitle(startTime), startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_01));
        events.add(event);

        startTime = Calendar.getInstance();
        startTime.set(Calendar.DAY_OF_MONTH, startTime.getActualMaximum(Calendar.DAY_OF_MONTH));
        startTime.set(Calendar.HOUR_OF_DAY, 15);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR_OF_DAY, 3);
        event = new WeekViewEvent(5, getEventTitle(startTime), startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_02));
        events.add(event);

        //AllDay event
        startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 0);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR_OF_DAY, 23);
        event = new WeekViewEvent(7, getEventTitle(startTime), null, startTime, endTime, true);
        event.setColor(getResources().getColor(R.color.event_color_04));
        events.add(event);
        events.add(event);

        startTime = Calendar.getInstance();
        startTime.set(Calendar.DAY_OF_MONTH, 8);
        startTime.set(Calendar.HOUR_OF_DAY, 2);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.set(Calendar.DAY_OF_MONTH, 10);
        endTime.set(Calendar.HOUR_OF_DAY, 23);
        event = new WeekViewEvent(8, getEventTitle(startTime), null, startTime, endTime, true);
        event.setColor(getResources().getColor(R.color.event_color_03));
        events.add(event);

        // All day event until 00:00 next day
        startTime = Calendar.getInstance();
        startTime.set(Calendar.DAY_OF_MONTH, 10);
        startTime.set(Calendar.HOUR_OF_DAY, 0);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.SECOND, 0);
        startTime.set(Calendar.MILLISECOND, 0);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.set(Calendar.DAY_OF_MONTH, 11);
        event = new WeekViewEvent(8, getEventTitle(startTime), null, startTime, endTime, true);
        event.setColor(getResources().getColor(R.color.event_color_01));
        events.add(event);*/

        return events;
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
