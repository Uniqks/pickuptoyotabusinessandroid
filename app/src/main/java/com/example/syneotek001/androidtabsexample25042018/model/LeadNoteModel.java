package com.example.syneotek001.androidtabsexample25042018.model;

public class LeadNoteModel {
    private long activityDate;
    private String attachmentURL = "";
    private long contractEndDate;
    private long contractStartDate;
    private String description = "";
    private String id = "";
    private String title = "";

    public LeadNoteModel(String id, String title, String description, String attachmentURL, long contractStartDate, long contractEndDate, long activityDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.attachmentURL = attachmentURL;
        this.contractStartDate = contractStartDate;
        this.contractEndDate = contractEndDate;
        this.activityDate = activityDate;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAttachmentURL() {
        return this.attachmentURL;
    }

    public void setAttachmentURL(String attachmentURL) {
        this.attachmentURL = attachmentURL;
    }

    public long getContractStartDate() {
        return this.contractStartDate;
    }

    public void setContractStartDate(long contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public long getContractEndDate() {
        return this.contractEndDate;
    }

    public void setContractEndDate(long contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public long getActivityDate() {
        return this.activityDate;
    }

    public void setActivityDate(long activityDate) {
        this.activityDate = activityDate;
    }

    public String toString() {
        return "LeadNoteModel{id='" + this.id + '\'' + ", title='" + this.title + '\'' + ", description='" + this.description + '\'' + ", attachmentURL='" + this.attachmentURL + '\'' + ", contractStartDate=" + this.contractStartDate + ", contractEndDate=" + this.contractEndDate + ", activityDate=" + this.activityDate + '}';
    }
}
