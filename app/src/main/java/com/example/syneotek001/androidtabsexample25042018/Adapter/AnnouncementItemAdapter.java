package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.ChatMessagesFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.DealRequestFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.DealsFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.EmailDetailFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.LeadDetailFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.AnnouncementItemModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

//import com.example.syneotek001.androidtabsexample25042018.Fragment.AddTaskFragment;
//import com.example.syneotek001.androidtabsexample25042018.Fragment.TaskListFragment;

public class AnnouncementItemAdapter extends RecyclerView.Adapter<AnnouncementItemAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<AnnouncementItemModel> mData;

    class MyViewHolder extends ViewHolder implements OnClickListener {

        TextView tvMessage, tvDateTime;
        LinearLayout lyAction;
        ImageView ivAction;

        MyViewHolder(View view) {
            super(view);
            tvMessage = view.findViewById(R.id.tvMessage);
            tvDateTime = view.findViewById(R.id.tvDateTime);
            lyAction = view.findViewById(R.id.lyAction);
            ivAction = view.findViewById(R.id.ivAction);

        }

        @Override
        public void onClick(View v) {
        }
    }

    public AnnouncementItemAdapter(Context context, ArrayList<AnnouncementItemModel> list) {
        this.mContext = context;
        this.mData = list;
    }

    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_announcement, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        final AnnouncementItemModel mAnnouncementItem = (AnnouncementItemModel) mData.get(position);
        holder.tvMessage.setText(mAnnouncementItem.getMessage());
        holder.tvDateTime.setText(mAnnouncementItem.getDatetime());
        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (mAnnouncementItem.getAction().equals("0")) {
                EmailDetailFragment Email = new EmailDetailFragment();
                ((HomeActivity) mContext).replaceFragment(Email);}else if (mAnnouncementItem.getAction().equals("1")) {
                LeadDetailFragment Lead = new LeadDetailFragment();
                ((HomeActivity) mContext).replaceFragment(Lead);}
            }
        });
        if (mAnnouncementItem.getAction().equals("0")) {
            holder.lyAction.setBackgroundResource(R.drawable.bg_circle_red);
            holder.ivAction.setBackgroundResource(R.drawable.ic_unread);
            holder.tvMessage.setText(mContext.getString(R.string.email_full_description));
        } else if (mAnnouncementItem.getAction().equals("1")) {
            holder.lyAction.setBackgroundResource(R.drawable.bg_circle_green);
            holder.ivAction.setBackgroundResource(R.drawable.ic_read);
        }

    }


    public int getItemCount() {
        return mData.size();
    }

}
