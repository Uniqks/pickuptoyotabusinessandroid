package com.example.syneotek001.androidtabsexample25042018.interfaces;

public interface EmailAdapterListener {
    void onIconClicked(int i);

    void onMessageRowClicked(int i);

    void onRowLongClicked(int i);
}
