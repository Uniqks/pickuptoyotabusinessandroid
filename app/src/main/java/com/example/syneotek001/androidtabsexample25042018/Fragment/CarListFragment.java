package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.CarItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.FragmentTabsPagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.CarModel;

import java.util.ArrayList;

public class CarListFragment extends Fragment {
    public static String BUNDLE_DATA_IS_SELECTABLE = "is_item_selectable";
    boolean isItemSelectable;
    CarItemAdapter mAdapter;
    ArrayList<CarModel> mCarList = new ArrayList<>();
    RecyclerView recyclerView;
    TextView tvNoCarFound;
    FloatingActionButton fabAddCamry;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car_list, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        tvNoCarFound = view.findViewById(R.id.tvNoCarFound);
        fabAddCamry = view.findViewById(R.id.fabCarExteriorInterior);
        fabAddCamry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ((HomeActivity)getActivity()).replaceFragment(new CarExteriorInteriorListHolderFragment());

            }
        });

        setUpRecyclerView();
        Log.e("CarListFragment", "onCreateView");
        setHasOptionsMenu(false);
        return view;
    }

    private void setUpRecyclerView() {
        mCarList.clear();
        mCarList.add(new CarModel("1", "CAMRY L", "26,590", "camry_l", false));
        mCarList.add(new CarModel("2", "CAMRY LE", "27,690", "camry_le", false));
        mCarList.add(new CarModel("3", "CAMRY SE", "28,090", "camry_se", false));

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down));
        mAdapter = new CarItemAdapter(getActivity(), mCarList/*, isItemSelectable*/);
        recyclerView.setAdapter(mAdapter);
        refreshData();
    }


    class C05872 extends RecyclerView.OnScrollListener {
        C05872() {
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy > 0 && fabAddCamry.isShown()) {
                fabAddCamry.hide();
            } else if (dy < 0 && !fabAddCamry.isShown()) {
                fabAddCamry.show();
            }
        }
    }

    private void refreshData() {
        if (mCarList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoCarFound.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.GONE);
        tvNoCarFound.setVisibility(View.VISIBLE);
    }

    public int carListSize() {
        return mCarList.size();
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpRecyclerView();
        Log.e("CarListFragment", "onResume");
    }
}
