/**
 * Copyright 2014 Magnus Woxblom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.syneotek001.androidtabsexample25042018;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;

import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.databinding.ActivityTemp5Binding;


public class Temp_5_Activity extends AppCompatActivity implements  View.OnClickListener{


    ActivityTemp5Binding binding;
    Bundle extra;String pos="";
    String mAlignment ="center"; int mColor = Color.BLACK;
    String url = "https://images.pexels.com/photos/35807/rose-red-rose-romantic-rose-bloom.jpg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  DataBindingUtil.setContentView(Temp_5_Activity.this,R.layout.activity_temp_5);

        extra = getIntent().getExtras();

        if(extra != null)
        {
            pos = extra.getString(Utils.TEMP_POS);
        }

        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.btnCenter.setOnClickListener(this);
        binding.btnRight.setOnClickListener(this);
        binding.btnLeft.setOnClickListener(this);
        mAlignment ="center";
        binding.btnCenter.setBackgroundColor(getResources().getColor(R.color.blue));
        binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
        binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
        binding.btnCenter.setTextColor(getResources().getColor(R.color.white));
        binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
        binding.btnRight.setTextColor(getResources().getColor(R.color.black));


        String[] mlist = getResources().getStringArray(R.array.spin_link);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_list, R.id.spin_item, mlist);
        binding.spinWeight.setAdapter(spinnerArrayAdapter);

        binding.etBgColor.setText(getString(R.string.black));

        binding.btnBgColor.setBackgroundColor(Color.BLACK);

        binding.etBgColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Temp_5_Activity.this, ColorAlertActivity.class).putExtra(Utils.EXTRA_POS, 1), 1);

            }
        });

        binding.btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mTop = binding.etPtop.getText().toString();
                String mRight = binding.etPright.getText().toString();
                String mBottom = binding.etPbottom.getText().toString();
                String mLeft = binding.etPleft.getText().toString();

                mTop = mTop.equals("") ?"0px":mTop+"px";
                mRight = mRight.equals("") ?"0px":mRight+"px";
                mBottom = mBottom.equals("") ?"0px":mBottom+"px";
                mLeft = mLeft.equals("") ?"0px":mLeft+"px";

                String mPadding =mTop+" "+mRight+ " "+mBottom+" "+mLeft;

                String mBg_color = (binding.etBgColor.getText().toString());
                String mUrl = binding.etImgUrl.getText().toString();
                String mWidth = binding.etImgWidth.getText().toString();

                mUrl = mUrl.equals("")?url: mUrl;
                mWidth = mWidth.equals("")?"370":mWidth;

                String mData="<table width=\""+mWidth+"\" bgcolor=\"" + mBg_color + "\" align=\"" + mAlignment + "\"><tr><td align=\"" + mAlignment + "\" class=\"image\" style=\"padding:" +
                        "  "+mPadding+";\"><img border=\"0\" " + "src="+mUrl+"" + "tabindex=\"0\" style=\"display: block; max-width: 100%;\"></td></tr></table>";

                /*String mData="<table width=\""+mWidth+"\" bgcolor=\"" + mBg_color + "\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"\"" + mAlignment + "\" data-type=\"image\" class=\"main\" style=\"display: table; " +
                      "><tbody><tr><td align=\"" + mAlignment + "\" class=\"image\" style=\"padding:  "+mPadding+";\"><img border=\"0\" " +
                        "src="+mUrl+"" +
                        "tabindex=\"0\" style=\"display: block; max-width: 100%;\"></td></tr></tbody></table>";*/
                LogUtils.i(" mData "+mData);

                Intent i = getIntent();
                i.putExtra(Utils.TEMP_1,mData);
                i.putExtra(Utils.TEMP_POS,pos);
                setResult(0,i);
                finish();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            mColor = data.getIntExtra(Utils.EXTRA_COLOR, Color.BLACK);
            if (requestCode == 1) {
                binding.etBgColor.setText("#" + Utils.toHex(mColor));
                binding.btnBgColor.setBackgroundColor(Color.parseColor("#" + Utils.toHex(mColor)));
            }

        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_center:
                mAlignment = "center";
                binding.btnCenter.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
                binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
                binding.btnCenter.setTextColor(getResources().getColor(R.color.white));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
                binding.btnRight.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.btn_left:
                mAlignment = "left";
                binding.btnCenter.setBackgroundColor(Color.TRANSPARENT);
                binding.btnLeft.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
                binding.btnCenter.setTextColor(getResources().getColor(R.color.black));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.white));
                binding.btnRight.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.btn_right:
                mAlignment = "right";
                binding.btnCenter.setBackgroundColor(Color.TRANSPARENT);
                binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
                binding.btnRight.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnCenter.setTextColor(getResources().getColor(R.color.black));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
                binding.btnRight.setTextColor(getResources().getColor(R.color.white));
                break;
        }

    }
}
