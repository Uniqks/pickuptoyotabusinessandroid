package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.ChatListFragment.OnLoadMoreListener;
import com.example.syneotek001.androidtabsexample25042018.Fragment.ChatMessagesFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.GlideUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.model.ChatListResponse.ChatListData;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiClient;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChatItemAdapter extends RecyclerView.Adapter<ViewHolder> {
    private GlideUtils glideUtils;
    private Context mContext;
    private List<ChatListData> mData;
    OnLoadMoreListener onLoadMoreListener;
    private int visibleThreshold = AppConstant.LIST_API_VISIBLE_THRESHHOLD;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public static class ProgressViewHolder extends ViewHolder {

        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    class MyViewHolder extends ViewHolder {
        ImageView ivOnlineIndicator, ivUserImage;
        RelativeLayout layoutChatImage;
        TextView tvDateTime, tvUnreadMessage, tvUserLetter, tvUserName;
        LinearLayout lyItemView;

        MyViewHolder(View view) {
            super(view);
            ivOnlineIndicator = view.findViewById(R.id.ivOnlineIndicator);
            ivUserImage = view.findViewById(R.id.ivUserImage);
            layoutChatImage = view.findViewById(R.id.layoutChatImage);
            tvDateTime = view.findViewById(R.id.tvDateTime);
            tvUnreadMessage = view.findViewById(R.id.tvUnreadMessage);
            tvUserLetter = view.findViewById(R.id.tvUserLetter);
            tvUserName = view.findViewById(R.id.tvUserName);
            lyItemView = view.findViewById(R.id.lyItemView);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public ChatItemAdapter(Context context, ArrayList<ChatListData> list, RecyclerView recyclerView) {
        this.mContext = context;
        this.mData = list;
        this.glideUtils = new GlideUtils(mContext);

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    Log.e("onLoadMoreListener"," "+onLoadMoreListener+" "+totalItemCount+" "+lastVisibleItem+" "+visibleThreshold+" "+loading);

                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        Log.e("onLoadMoreListener","if");
                        if (onLoadMoreListener != null) {
                            Log.e("onLoadMoreListener","if2");
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });

        }

    }

    public int getItemViewType(int position) {
        return mData.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewHolder vh;

        if (viewType == VIEW_ITEM) {

            View v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);

            vh = new MyViewHolder(v);


        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }


        return vh;

       /* View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat, parent, false);

        return new ChatItemAdapter.MyViewHolder(v);*/
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        if (holder instanceof MyViewHolder)
        {
            int i;
            ChatListData mChatModel = (ChatListData) this.mData.get(position);
            if (mChatModel==null)
                return;
            if (mChatModel.getFriendname()!=null && !mChatModel.getFriendname().equals(""))
                ((MyViewHolder)holder).tvUserName.setText(mChatModel.getFriendname());
            ImageView imageView = ((MyViewHolder)holder).ivOnlineIndicator;
            if (mChatModel.getSession_status().equals("online")) {
                i = 0;
            } else {
                i = 8;
            }
            imageView.setVisibility(i);
            if (mChatModel.getFriendimage() == null || mChatModel.getFriendimage().length() <= 0) {
                String title = mChatModel.getFriendname().toUpperCase();
                if (title.length() > 0) {
                    ((MyViewHolder)holder).tvUserLetter.setText(title.substring(0, 1).toUpperCase());
                } else {
                    ((MyViewHolder)holder).tvUserLetter.setText("T");
                }
                ((MyViewHolder)holder).tvUserLetter.setVisibility(View.VISIBLE);
                glideUtils.loadImageCircular("", ((MyViewHolder)holder).ivUserImage);
            } else {
                glideUtils.loadImageCircular(ApiClient.SALES_MAN_PROFILE_IMG+mChatModel.getFriendimage(), ((MyViewHolder)holder).ivUserImage);
                ((MyViewHolder)holder).tvUserLetter.setVisibility(View.GONE);
            }

            if (mChatModel.getTotal_unread_messages()!=null && !mChatModel.getTotal_unread_messages().equals("")) {
                if (Integer.parseInt(mChatModel.getTotal_unread_messages()) > 0) {
                    ((MyViewHolder)holder).tvUnreadMessage.setText(this.mContext.getResources().getString(Integer.parseInt(mChatModel.getTotal_unread_messages()) > 1 ? R.string._unread_messages : R.string._unread_message, new Object[]{String.valueOf(mChatModel.getTotal_unread_messages())}));
                    ((MyViewHolder)holder).tvUnreadMessage.setVisibility(View.VISIBLE);
                    return;
                }
                ((MyViewHolder)holder).tvUnreadMessage.setVisibility(View.GONE);
            }



            ((MyViewHolder)holder).lyItemView.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Log.e("ChatItemAdapter","onClick ");
                    /*ChatMessagesFragment chatMessages = new ChatMessagesFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ChatMessagesFragment.BUNDLE_CHAT_FRIEND_MODEL, (Serializable) mData.get(position));
                    bundle.putBoolean(Utils.EXTRA_IS_FROM_CHAT,true);
                    chatMessages.setArguments(bundle);
                    ((HomeActivity) mContext).replaceFragment(chatMessages);*/
//                ((MainActivity) ChatItemAdapter.this.mContext).pushFragment(((MainActivity) ChatItemAdapter.this.mContext).mCurrentTab, chatMessages, true, true);
                }
            });
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }


    }

    public int getItemCount() {
        return this.mData.size();
    }
}
