package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.FragmentTabsPagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

public class DealAssignmentHolderFragment extends Fragment implements OnBackPressed{
    FragmentTabsPagerAdapter mAdapter;
    FragmentTabListHolderBinding mBinding;
    View view;
    public TabLayout tabLayout;
    public ViewPager viewPager;
    Activity activity;
    String sales_man_name,sales_man_id;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view=inflater.inflate(R.layout.fragment_deal_assignment_holder, container, false);
//        activity = getActivity();
//        ((HomeActivity)activity).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) activity));
        // Inflate the layout for this fragment
        tabLayout=view.findViewById(R.id.tabLayout);
        viewPager=view.findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        setHasOptionsMenu(true);
        ImageView ivBack=view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        return view;
    }

    private void setupTabIcons() {
        int tabCount = mAdapter.getCount();
        for (int i = 0; i < tabCount; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                View llTab = LayoutInflater.from(tabLayout.getContext()).inflate(R.layout.layout_tab, null);
                ((TextView) llTab.findViewById(R.id.tvTabTitle)).setText(mAdapter.getPageTitle(i));
                tab.setCustomView(llTab);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
//        mAdapter = new FragmentTabsPagerAdapter(getFragmentManager());
        mAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());
        if (getArguments()!=null) {
            sales_man_name = getArguments().getString("sales_man_name");
            sales_man_id = getArguments().getString("sales_man_id");
        }
        Bundle bundle = new Bundle();
        bundle.putString("sales_man_name",sales_man_name);
        bundle.putString("sales_man_id",sales_man_id);
        bundle.putString("deal_type",getResources().getString(R.string.str_new));
        DealManagerFragment dealManagerFragment = new DealManagerFragment();
        dealManagerFragment.setArguments(bundle);
        mAdapter.addFragment(dealManagerFragment, getResources().getString(R.string.str_new));

        Bundle bundle1 = new Bundle();
        bundle1.putString("sales_man_name",sales_man_name);
        bundle1.putString("sales_man_id",sales_man_id);
        bundle1.putString("deal_type",getResources().getString(R.string.str_pending));
        DealManagerFragment dealManagerFragment1 = new DealManagerFragment();
        dealManagerFragment1.setArguments(bundle1);
        mAdapter.addFragment(dealManagerFragment1, getResources().getString(R.string.str_pending));

        Bundle bundle2 = new Bundle();
        bundle2.putString("sales_man_name",sales_man_name);
        bundle2.putString("sales_man_id",sales_man_id);
        bundle2.putString("deal_type",getResources().getString(R.string.str_in_progress));
        DealManagerFragment dealManagerFragment2 = new DealManagerFragment();
        dealManagerFragment2.setArguments(bundle2);
        mAdapter.addFragment(dealManagerFragment2, getResources().getString(R.string.str_in_progress));

        Bundle bundle3 = new Bundle();
        bundle3.putString("sales_man_name",sales_man_name);
        bundle3.putString("sales_man_id",sales_man_id);
        bundle3.putString("deal_type",getResources().getString(R.string.str_approved));
        DealManagerFragment dealManagerFragment3 = new DealManagerFragment();
        dealManagerFragment3.setArguments(bundle3);
        mAdapter.addFragment(dealManagerFragment3, getResources().getString(R.string.str_approved));

        Bundle bundle4 = new Bundle();
        bundle4.putString("sales_man_name",sales_man_name);
        bundle4.putString("sales_man_id",sales_man_id);
        bundle4.putString("deal_type",getResources().getString(R.string.str_approach));
        DealManagerFragment dealManagerFragment4 = new DealManagerFragment();
        dealManagerFragment4.setArguments(bundle4);
        mAdapter.addFragment(dealManagerFragment4, getResources().getString(R.string.str_approach));

        Bundle bundle5 = new Bundle();
        bundle5.putString("sales_man_name",sales_man_name);
        bundle5.putString("sales_man_id",sales_man_id);
        bundle5.putString("deal_type",getResources().getString(R.string.str_rejected));
        DealManagerFragment dealManagerFragment5 = new DealManagerFragment();
        dealManagerFragment5.setArguments(bundle5);
        mAdapter.addFragment(dealManagerFragment5, getResources().getString(R.string.str_rejected));

        Bundle bundle6 = new Bundle();
        bundle6.putString("sales_man_name",sales_man_name);
        bundle6.putString("sales_man_id",sales_man_id);
        bundle6.putString("deal_type",getResources().getString(R.string.str_co_signer));
        DealManagerFragment dealManagerFragment6 = new DealManagerFragment();
        dealManagerFragment6.setArguments(bundle6);
        mAdapter.addFragment(dealManagerFragment6, getResources().getString(R.string.str_co_signer));

        Bundle bundle7 = new Bundle();
        bundle7.putString("sales_man_name",sales_man_name);
        bundle7.putString("sales_man_id",sales_man_id);
        bundle7.putString("deal_type",getResources().getString(R.string.str_missing_signature));
        DealManagerFragment dealManagerFragment7 = new DealManagerFragment();
        dealManagerFragment7.setArguments(bundle7);
        mAdapter.addFragment(dealManagerFragment7, getResources().getString(R.string.str_missing_signature));

        Bundle bundle8 = new Bundle();
        bundle8.putString("sales_man_name",sales_man_name);
        bundle8.putString("sales_man_id",sales_man_id);
        bundle8.putString("deal_type",getResources().getString(R.string.str_service));
        DealManagerFragment dealManagerFragment8 = new DealManagerFragment();
        dealManagerFragment8.setArguments(bundle8);
        mAdapter.addFragment(dealManagerFragment8, getResources().getString(R.string.str_service));

        Bundle bundle9 = new Bundle();
        bundle9.putString("sales_man_name",sales_man_name);
        bundle9.putString("sales_man_id",sales_man_id);
        bundle9.putString("deal_type",getResources().getString(R.string.str_sold));
        DealManagerFragment dealManagerFragment9 = new DealManagerFragment();
        dealManagerFragment9.setArguments(bundle9);
        mAdapter.addFragment(dealManagerFragment9, getResources().getString(R.string.str_sold));

        Bundle bundle10 = new Bundle();
        bundle10.putString("sales_man_name",sales_man_name);
        bundle10.putString("sales_man_id",sales_man_id);
        bundle10.putString("deal_type",getResources().getString(R.string.str_delivery));
        DealManagerFragment dealManagerFragment10 = new DealManagerFragment();
        dealManagerFragment10.setArguments(bundle10);
        mAdapter.addFragment(dealManagerFragment10, getResources().getString(R.string.str_delivery));

        Bundle bundle11 = new Bundle();
        bundle11.putString("sales_man_name",sales_man_name);
        bundle11.putString("sales_man_id",sales_man_id);
        bundle11.putString("deal_type",getResources().getString(R.string.str_ready_for_delivery));
        DealManagerFragment dealManagerFragment11 = new DealManagerFragment();
        dealManagerFragment11.setArguments(bundle11);
        mAdapter.addFragment(dealManagerFragment11, getResources().getString(R.string.str_ready_for_delivery));

        Bundle bundle12 = new Bundle();
        bundle12.putString("sales_man_name",sales_man_name);
        bundle12.putString("sales_man_id",sales_man_id);
        bundle12.putString("deal_type",getResources().getString(R.string.str_deliveried));
        DealManagerFragment dealManagerFragment12 = new DealManagerFragment();
        dealManagerFragment12.setArguments(bundle12);
        mAdapter.addFragment(dealManagerFragment12, getResources().getString(R.string.str_deliveried));

        Bundle bundle13 = new Bundle();
        bundle13.putString("sales_man_name",sales_man_name);
        bundle13.putString("sales_man_id",sales_man_id);
        bundle13.putString("deal_type",getResources().getString(R.string.str_waiting));
        DealManagerFragment dealManagerFragment13 = new DealManagerFragment();
        dealManagerFragment13.setArguments(bundle13);
        mAdapter.addFragment(dealManagerFragment13, getResources().getString(R.string.str_waiting));

        Bundle bundle14 = new Bundle();
        bundle14.putString("sales_man_name",sales_man_name);
        bundle14.putString("sales_man_id",sales_man_id);
        bundle14.putString("deal_type",getResources().getString(R.string.str_payment));
        DealManagerFragment dealManagerFragment14 = new DealManagerFragment();
        dealManagerFragment14.setArguments(bundle14);
        mAdapter.addFragment(dealManagerFragment14, getResources().getString(R.string.str_payment));

        Bundle bundle15 = new Bundle();
        bundle15.putString("sales_man_name",sales_man_name);
        bundle15.putString("sales_man_id",sales_man_id);
        bundle15.putString("deal_type",getResources().getString(R.string.str_completed));
        DealManagerFragment dealManagerFragment15 = new DealManagerFragment();
        dealManagerFragment15.setArguments(bundle15);
        mAdapter.addFragment(dealManagerFragment15, getResources().getString(R.string.str_completed));



        viewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_lead_management, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_filter:
//                ((MainActivity) this.mActivity).pushFragment(((MainActivity) this.mContext).mCurrentTab, new LeadFilterFragment(), true, true);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViewPager(viewPager);
    }
}
