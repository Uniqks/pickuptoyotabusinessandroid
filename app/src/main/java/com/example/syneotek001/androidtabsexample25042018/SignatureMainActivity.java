package com.example.syneotek001.androidtabsexample25042018;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class SignatureMainActivity extends AppCompatActivity {


    public Bitmap signatureBitmap;
    Bundle extra;
    int mPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new SignatureMainLayoutChilled(this));
        ///setContentView(R.layout.activity_condenser_water);
        extra = getIntent().getExtras();

        if (extra != null) {
            mPos = extra.getInt(Utils.EXTRA_SIGN_POS);
        }
     /*   Intent returnIntent = new Intent();
        returnIntent.putExtra("path","Kattu");

        returnIntent.putExtra(Utils.EXTRA_SIGN_POS,mPos);
        setResult(RESULT_OK, returnIntent);
        finish();*/
    }


    public class SignatureMainLayoutChilled extends LinearLayout implements View.OnClickListener {

        LinearLayout buttonsLayout;
        SignatureView signatureView;
        Context context;
        Activity parentActivity;


        public SignatureMainLayoutChilled(Context context) {
            super(context);
            this.context = context;
            this.setOrientation(LinearLayout.VERTICAL);

            this.buttonsLayout = this.buttonsLayout(context);
            this.signatureView = new SignatureView(context);

            // add the buttons and signature views
            this.addView(this.buttonsLayout);
            this.addView(signatureView);


            //parentActivity = (Activity) context;

        }

        private LinearLayout buttonsLayout(Context context) {

            // create the UI programatically
            LinearLayout linearLayout = new LinearLayout(this.getContext());
            Button saveBtn = new Button(this.getContext());
            Button clearBtn = new Button(this.getContext());
            Button cancelBtn = new Button(this.getContext());

            // set orientation
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setBackgroundColor(Color.GRAY);

            Typeface typeface = Typeface.createFromAsset(context.getAssets(),"fonts/montserrat_regular.otf");

            LayoutParams params = new LayoutParams(
                    LayoutParams.WRAP_CONTENT,
                    70
            );
            params.setMargins(10, 20, 10, 20);

            // set texts, tags and OnClickListener
            saveBtn.setText("Ok");
            saveBtn.setTextColor(ContextCompat.getColor(context,R.color.white));
            saveBtn.setTag("Insert");
            saveBtn.setBackgroundResource(R.drawable.bg_gradient_submit);
            saveBtn.setLayoutParams(params);
            saveBtn.setOnClickListener(this);
            saveBtn.setTypeface(typeface);

            clearBtn.setText("Clear");
            clearBtn.setTextColor(ContextCompat.getColor(context,R.color.white));
            clearBtn.setTag("Clear");
            clearBtn.setBackgroundResource(R.drawable.bg_gradient_submit);
            clearBtn.setLayoutParams(params);
            clearBtn.setOnClickListener(this);
            clearBtn.setTypeface(typeface);

            cancelBtn.setText("Cancel");
            cancelBtn.setTextColor(ContextCompat.getColor(context,R.color.white));
            cancelBtn.setTag("Cancel");
            cancelBtn.setBackgroundResource(R.drawable.bg_gradient_submit);
            cancelBtn.setLayoutParams(params);
            cancelBtn.setOnClickListener(this);
            cancelBtn.setTypeface(typeface);

            linearLayout.addView(saveBtn);
            linearLayout.addView(clearBtn);
            linearLayout.addView(cancelBtn);

            // return the whoe layout
            return linearLayout;
        }

        // the on click listener of 'save' and 'clear' buttons
        @Override
        public void onClick(View v) {
            String tag = v.getTag().toString().trim();

            // save the signature
            if (tag.equalsIgnoreCase("Insert")) {
                this.saveImage(this.signatureView.getSignature());

            }

            // empty the canvas
            else if (tag.equalsIgnoreCase("Clear")){
                this.signatureView.clearSignature();

            } else if (tag.equalsIgnoreCase("Cancel")) {
                finish();
            }

        }

        /**
         * save the signature to an sd card directory
         *
         * @param signature bitmap
         */
        final void saveImage(Bitmap signature) {

            String root = Environment.getExternalStorageDirectory().toString();

            // the directory where the signature will be saved
            File myDir = new File(root + "/saved_signature");

            // make the directory if it does not exist yet
            if (!myDir.exists()) {
                myDir.mkdirs();
            }

            java.util.Date date = new java.util.Date();
            String fname = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(date);

            File file = new File(myDir, fname + ".jpg");
            if (file.exists()) {
                file.delete();
            }

            try {

                // save the signature
                FileOutputStream out = new FileOutputStream(file);
                signature.compress(Bitmap.CompressFormat.PNG, 100, out);


                out.flush();
                out.close();

                //Toast.makeText(this.getContext(), "Signature saved.", Toast.LENGTH_LONG).show();


            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent returnIntent = new Intent();
            returnIntent.putExtra("path",file.getAbsolutePath());

            returnIntent.putExtra(Utils.EXTRA_SIGN_POS,mPos);
            setResult(RESULT_OK, returnIntent);
            finish();
        }


        /**
         * The View where the signature will be drawn
         */
        private class SignatureView extends View {

            // set the stroke width
            private static final float STROKE_WIDTH = 5f;
            private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;

            private Paint paint = new Paint();
            private Path path = new Path();

            private float lastTouchX;
            private float lastTouchY;
            private final RectF dirtyRect = new RectF();

            public SignatureView(Context context) {

                super(context);

                paint.setAntiAlias(true);
                paint.setColor(Color.BLACK);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeJoin(Paint.Join.ROUND);
                paint.setStrokeWidth(STROKE_WIDTH);

                // set the bg color as white
                this.setBackgroundColor(Color.WHITE);

                // width and height should cover the screen
                this.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

            }

            /**
             * Get signature
             *
             * @return
             */
            protected Bitmap getSignature() {

                signatureBitmap = null;

                // set the signature bitmap
                if (signatureBitmap == null) {
                    signatureBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.RGB_565);


                  /*  AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    String chilled = AppsContants.sharedpreferences.getString(AppsContants.CHILLEDID, "");*/


                  /*  if (chilled.equals("1")) {

                        ChilledWaterActivity.empsign.setImageBitmap(signatureBitmap);

                    } else if (chilled.equals("2")) {

                        ChilledWaterActivity.clisign.setImageBitmap(signatureBitmap);


                    } else if (chilled.equals("3")) {

                        ChilledWaterActivity.clisign1.setImageBitmap(signatureBitmap);

                    } else {

                        ChilledWaterActivity.clisign12.setImageBitmap(signatureBitmap);


                    }*/


                }

                // important for saving signature
                final Canvas canvas = new Canvas(signatureBitmap);
                this.draw(canvas);

                return signatureBitmap;

            }

            /**
             * clear signature canvas
             */
            private void clearSignature() {
                path.reset();
                this.invalidate();
            }

            // all touch events during the drawing
            @Override
            protected void onDraw(Canvas canvas) {
                canvas.drawPath(this.path, this.paint);
            }

            @Override
            public boolean onTouchEvent(MotionEvent event) {
                float eventX = event.getX();
                float eventY = event.getY();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        path.moveTo(eventX, eventY);

                        lastTouchX = eventX;
                        lastTouchY = eventY;
                        return true;

                    case MotionEvent.ACTION_MOVE:

                    case MotionEvent.ACTION_UP:

                        resetDirtyRect(eventX, eventY);
                        int historySize = event.getHistorySize();
                        for (int i = 0; i < historySize; i++) {
                            float historicalX = event.getHistoricalX(i);
                            float historicalY = event.getHistoricalY(i);

                            expandDirtyRect(historicalX, historicalY);
                            path.lineTo(historicalX, historicalY);
                        }
                        path.lineTo(eventX, eventY);
                        break;

                    default:

                        return false;
                }

                invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                        (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

                lastTouchX = eventX;
                lastTouchY = eventY;

                return true;
            }

            private void expandDirtyRect(float historicalX, float historicalY) {
                if (historicalX < dirtyRect.left) {
                    dirtyRect.left = historicalX;
                } else if (historicalX > dirtyRect.right) {
                    dirtyRect.right = historicalX;
                }

                if (historicalY < dirtyRect.top) {
                    dirtyRect.top = historicalY;
                } else if (historicalY > dirtyRect.bottom) {
                    dirtyRect.bottom = historicalY;
                }
            }

            private void resetDirtyRect(float eventX, float eventY) {
                dirtyRect.left = Math.min(lastTouchX, eventX);
                dirtyRect.right = Math.max(lastTouchX, eventX);
                dirtyRect.top = Math.min(lastTouchY, eventY);
                dirtyRect.bottom = Math.max(lastTouchY, eventY);
            }

        }

    }
}