package com.example.syneotek001.androidtabsexample25042018.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v4.view.ViewCompat;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.example.syneotek001.androidtabsexample25042018.R;


public class VerticalLabelView extends View {
    static final int DEFAULT_TEXT_SIZE = 15;
    private int mAscent;
    private String mText;
    private TextPaint mTextPaint;
    private Rect text_bounds = new Rect();

    public VerticalLabelView(Context context) {
        super(context);
        initLabelView();
    }

    @SuppressLint("ResourceType")
    public VerticalLabelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLabelView();
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.VerticalLabelView);
        CharSequence s = a.getString(0);
        if (s != null) {
            setText(s.toString());
        }
        setTextColor(a.getColor(1, ViewCompat.MEASURED_STATE_MASK));
        @SuppressLint("ResourceType") int textSize = a.getDimensionPixelOffset(3, 0);
        if (textSize > 0) {
            setTextSize(textSize);
        }
        FontManager.getInstance().applyFont(context, this.mTextPaint, attrs);
        a.recycle();
    }

    private final void initLabelView() {
        this.mTextPaint = new TextPaint();
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setTextSize(15.0f);
        this.mTextPaint.setColor(ViewCompat.MEASURED_STATE_MASK);
        this.mTextPaint.setTextAlign(Align.CENTER);
        setPadding(3, 3, 3, 3);
    }

    public void setText(String text) {
        this.mText = text;
        requestLayout();
        invalidate();
    }

    public void setTextSize(int size) {
        this.mTextPaint.setTextSize((float) size);
        requestLayout();
        invalidate();
    }

    public void setTextColor(int color) {
        this.mTextPaint.setColor(color);
        invalidate();
    }

    public void setFontFamily(Context context) {
        this.mTextPaint.setTypeface(Typeface.createFromAsset(context.getAssets(), "montserrat_regular.otf"));
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d("mTextPaint",this.mTextPaint+"");
        Log.d("mText",this.mText+"");
        Log.d("mText.length()",this.mText.length()+"");
        Log.d("text_bounds",this.text_bounds+"");
        this.mTextPaint.getTextBounds(this.mText, 0, this.mText.length(), this.text_bounds);
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        int result = (this.text_bounds.height() + getPaddingLeft()) + getPaddingRight();
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        this.mAscent = (int) this.mTextPaint.ascent();
        if (specMode == 1073741824) {
            return specSize;
        }
        int result = (this.text_bounds.width() + getPaddingTop()) + getPaddingBottom();
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.translate((float) (getPaddingTop() - this.mAscent), ((float) getPaddingLeft()) + (((float) this.text_bounds.width()) / 2.0f));
        canvas.rotate(-90.0f);
        canvas.drawText(this.mText, 0.0f, 0.0f, this.mTextPaint);
    }
}
