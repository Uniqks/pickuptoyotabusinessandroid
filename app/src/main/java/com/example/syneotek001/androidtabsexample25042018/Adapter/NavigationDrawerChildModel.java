package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.os.Parcel;
import android.os.Parcelable;

public class NavigationDrawerChildModel implements Parcelable {
    public static final Creator<NavigationDrawerChildModel> CREATOR = new C05631();
    private boolean isFavorite;
    private String name;

    static class C05631 implements Creator<NavigationDrawerChildModel> {
        C05631() {
        }

        public NavigationDrawerChildModel createFromParcel(Parcel in) {
            return new NavigationDrawerChildModel(in);
        }

        public NavigationDrawerChildModel[] newArray(int size) {
            return new NavigationDrawerChildModel[size];
        }
    }

    public NavigationDrawerChildModel(String name, boolean isFavorite) {
        this.name = name;
        this.isFavorite = isFavorite;
    }

    private NavigationDrawerChildModel(Parcel in) {
        this.name = in.readString();
    }

    public String getName() {
        return this.name;
    }

    private boolean isFavorite() {
        return this.isFavorite;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NavigationDrawerChildModel)) {
            return false;
        }
        NavigationDrawerChildModel navigationDrawerChildModel = (NavigationDrawerChildModel) o;
        if (isFavorite() == navigationDrawerChildModel.isFavorite()) {
            if (getName() != null) {
                if (getName().equals(navigationDrawerChildModel.getName())) {
                    return true;
                }
            } else if (navigationDrawerChildModel.getName() == null) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (getName() != null) {
            result = getName().hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (isFavorite()) {
            i = 1;
        }
        return i2 + i;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
    }

    public int describeContents() {
        return 0;
    }
}
