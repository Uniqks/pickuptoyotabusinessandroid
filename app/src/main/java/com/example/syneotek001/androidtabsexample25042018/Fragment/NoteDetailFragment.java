package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;


public class NoteDetailFragment extends Fragment {
    public ImageView ivLeadLabel, ivBack;

    public TextView tvAttachmentTitle, tvContractEndDate, tvContractStartDate, tvDescription, tvNoteActivityDate, tvTitle;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note_detail, container, false);
        ivBack = view.findViewById(R.id.ivBack);
        ivLeadLabel = view.findViewById(R.id.ivLeadLabel);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        setHasOptionsMenu(false);
        return view;
    }
}
