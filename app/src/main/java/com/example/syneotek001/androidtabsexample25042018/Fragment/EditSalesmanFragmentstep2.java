package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentProfileStep2Binding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentSalesmanProfileStep2Binding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanItemModel;

public class EditSalesmanFragmentstep2 extends Fragment implements OnClickListener, OnBackPressed {
    public Button btnSave, btnSaveAndNext;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;
    FragmentSalesmanProfileStep2Binding binding;
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_salesman_profile_step2, container, false);
        View view = binding.getRoot();
        /*btnSave = rootview.findViewById(R.id.btnSave);
        btnSaveAndNext = rootview.findViewById(R.id.btnSaveAndNext);
        etEmail = rootview.findViewById(R.id.etEmail);
        etFirstName = rootview.findViewById(R.id.etFirstName);
        etHomePhone = rootview.findViewById(R.id.etHomePhone);
        etLastName = rootview.findViewById(R.id.etLastName);
        etMobile = rootview.findViewById(R.id.etMobile);
        etTitle = rootview.findViewById(R.id.etTitle);
        etWorkFax = rootview.findViewById(R.id.etWorkFax);
        etWorkPhone = rootview.findViewById(R.id.etWorkPhone);
        ivContact = rootview.findViewById(R.id.ivContact);
        ivEmail = rootview.findViewById(R.id.ivEmail);
        ivMobile = rootview.findViewById(R.id.ivMobile);
        ivTitle = rootview.findViewById(R.id.ivTitle);
        tvPageIndicator = rootview.findViewById(R.id.tvPageIndicator);
        tvTitle = rootview.findViewById(R.id.tvTitle);
        ivBack = rootview.findViewById(R.id.ivBack);*/
        binding.ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity()!=null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
        setHasOptionsMenu(false);
        setClickEvents();

        setAnimation(binding.lblProvince);
        setAnimation(binding.lblPostalCode);
        setAnimation(binding.lblLanguage);
        setAnimation(binding.lblServiceArea);

        setAnimation(binding.etProvince);
        setAnimation(binding.etPostalCode);
        setAnimation(binding.etLanguage);
        setAnimation(binding.etServiceArea);
        setAnimation(binding.etDescription);

        /*Drawable innerDrawable = getResources().getDrawable( R.drawable.ic_tag );

        GravityCompoundDrawable gravityDrawable = new GravityCompoundDrawable(innerDrawable);
        // NOTE: next 2 lines are important!
        innerDrawable.setBounds(10, 10, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
        gravityDrawable.setBounds(10, 10, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
        binding.etDescription.setCompoundDrawables(gravityDrawable, null, null, null);*/

        if (getArguments() != null) {
            if (getArguments().getBoolean("is_edit")) {
                if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                    if (getArguments().getString("profile_type").equals("salesman"))
                        binding.tvTitlemain.setText(getString(R.string.str_edit_sales));
                    else if (getArguments().getString("profile_type").equals("receptionist"))
                        binding.tvTitlemain.setText(getString(R.string.str_edit_receptionist));
                }
                if (getArguments().getSerializable("salesman_profile") != null) {
                    SalesmanItemModel salesmanItemModel = (SalesmanItemModel) getArguments().getSerializable("salesman_profile");
                    binding.etCity.setText(salesmanItemModel.getCity());
                    binding.etProvince.setText(salesmanItemModel.getProvince());
                    binding.etPostalCode.setText(salesmanItemModel.getPostal());
                    binding.etLanguage.setText(salesmanItemModel.getLanguages());
                    binding.etServiceArea.setText(salesmanItemModel.getServiceAreas());
                    binding.etDescription.setText(salesmanItemModel.getDescription());
                }

            } else {
                if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                    if (getArguments().getString("profile_type").equals("salesman"))
                        binding.tvTitlemain.setText(getString(R.string.str_add_sales));
                    else if (getArguments().getString("profile_type").equals("receptionist"))
                        binding.tvTitlemain.setText(getString(R.string.str_add_receptionist));
                }
            }
        }

        return view;
    }

    public void setAnimation(View view){
        YoYo.with(Techniques.FlipInX).duration(1000).delay(150).repeat(0).playOn(view);
    }

    private void setClickEvents() {
        binding.btnSave.setOnClickListener(this);
     //   btnSaveAndNext.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                if (getActivity()!=null) {
                    getActivity().onBackPressed();
                }
                return;

            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity()!=null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
