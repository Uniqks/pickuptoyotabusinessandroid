package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.Adapter.DealDescriptionMessageAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.RequestSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.GlideUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.ChatMessageModel;
import com.example.syneotek001.androidtabsexample25042018.model.ChatModel;
import com.example.syneotek001.androidtabsexample25042018.model.DealRequestDetailResponse;
import com.example.syneotek001.androidtabsexample25042018.model.DealRequestDetailResponse.DealRequestMessagesData;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse.SalesmanDealRequestListData.RequestListData;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiManager;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiResponseInterface;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class DealsDescriptionFragment extends Fragment implements OnClickListener, OnBackPressed, ApiResponseInterface {
    public static String BUNDLE_CHAT_FRIEND_MODEL = "chat_friend_data";
    private Animation animScaleIn;
    private Animation animScaleOut;
    DealDescriptionMessageAdapter mAdapter;
    ArrayList<DealRequestMessagesData> mChatMessageList = new ArrayList<>();
    ChatModel mChatModel;

    public EditText etMessage;
    public ImageView ivBack, ivFriendImage, ivOnlineIndicator, ivCamera, ivEmoticon, ivSendMessage, ivUploadFile;
    public LinearLayout llChatBox;
    public RecyclerView recyclerViewMessage;
    public RelativeLayout rel_NoMessageFound;
    public View viewDivider, viewDividerTop;
    TextView tvFriendName, tvLastSeen, tvNoMessageFound, tvFriendLetter, tvRequests, tvNotes;
    //    private SlidingUpPanelLayout mLayout;
    Spinner spinnerRequests;
    ArrayList<String> arr_schedule_categories = new ArrayList<>();
    ArrayList<RequestListData> arrRequestList = new ArrayList<>();
    String deal_request_id, friendName, friendId;
    ApiManager apiManager;


    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_deal_description, container, false);

        etMessage = mview.findViewById(R.id.etMessage);
        ivCamera = mview.findViewById(R.id.ivCamera);
        ivEmoticon = mview.findViewById(R.id.ivEmoticon);
        ivSendMessage = mview.findViewById(R.id.ivSendMessage);
        ivUploadFile = mview.findViewById(R.id.ivUploadFile);
        llChatBox = mview.findViewById(R.id.llChatBox);
        recyclerViewMessage = mview.findViewById(R.id.recyclerViewMessage);
        viewDivider = mview.findViewById(R.id.viewDivider);
        rel_NoMessageFound = mview.findViewById(R.id.rel_NoMessageFound);
        ivBack = mview.findViewById(R.id.ivBack);
        ivFriendImage = mview.findViewById(R.id.ivFriendImage);
        tvFriendName = mview.findViewById(R.id.tvFriendName);
        tvLastSeen = mview.findViewById(R.id.tvLastSeen);
        ivOnlineIndicator = mview.findViewById(R.id.ivOnlineIndicator);
        tvNoMessageFound = mview.findViewById(R.id.tvNoMessageFound);
        tvFriendLetter = mview.findViewById(R.id.tvFriendLetter);
        tvRequests = mview.findViewById(R.id.tvRequests);
        tvNotes = mview.findViewById(R.id.tvNotes);
        spinnerRequests = mview.findViewById(R.id.spinnerRequests);
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        animScaleIn = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale_in);
        animScaleOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_scale_out);
//        setUpRecyclerView();
        setEditTexEvent();
        setClickEvents();
        setHasOptionsMenu(false);


        apiManager = new ApiManager(getActivity(), this);
//        no_of_requests
        if (getArguments() != null) {

            if (getArguments().getString(Utils.EXTRA_DEAL_DETAIL_FRIEND_ID) != null
                    && !getArguments().getString(Utils.EXTRA_DEAL_DETAIL_FRIEND_ID).equals("")) {
                friendId = getArguments().getString(Utils.EXTRA_DEAL_DETAIL_FRIEND_ID);
            }

            if (getArguments().getString(Utils.EXTRA_DEAL_DETAIL_FRIEND_NAME) != null
                    && !getArguments().getString(Utils.EXTRA_DEAL_DETAIL_FRIEND_NAME).equals("")) {
                friendName = getArguments().getString(Utils.EXTRA_DEAL_DETAIL_FRIEND_NAME);
            }

            if (getArguments().getSerializable(Utils.EXTRA_DEAL_REQUEST_LIST) != null) {
                arrRequestList = (ArrayList<RequestListData>) getArguments().getSerializable(Utils.EXTRA_DEAL_REQUEST_LIST);
                LogUtils.i("DealsDescriptionFragment" + " onCreateView arrRequestList size " + arrRequestList.size());
                if (arrRequestList.size() != 0) {
                    if (arrRequestList.size() == 1) {
                        spinnerRequests.setVisibility(View.GONE);
                        tvRequests.setVisibility(View.VISIBLE);
                        tvRequests.setText(arrRequestList.get(0).getRequest_name());
                        deal_request_id = arrRequestList.get(0).getRequest_id();
                        LogUtils.i("DealsDescriptionFragment" + " onCreateView Request_name " + arrRequestList.get(0).getRequest_name() + " deal_request_id " + deal_request_id);

                    } else {
                        spinnerRequests.setVisibility(View.VISIBLE);
                        tvRequests.setVisibility(View.GONE);
                        for (int i = 0; i < arrRequestList.size(); i++)
                            arr_schedule_categories.add(arrRequestList.get(i).getRequest_name());
                        initCustomSpinner(arr_schedule_categories);
                        spinnerRequests.setSelection(0);
                    }
                }
            }
        }

        if (Utils.isOnline()) {

            if (deal_request_id != null && !deal_request_id.equals("")) {
                apiManager.getDealRequestDetail(AppConstant.DEAL_REQUEST_DETAIL_ACTION, deal_request_id);
            }
        } else {
            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
        }

        tvFriendName.setText(getString(R.string.str_requested_deals));
        /*mLayout = mview.findViewById(R.id.sliding_layout);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.d("onPanelSlide", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.d("onPanelStateChanged", "onPanelStateChanged " + newState);
            }
        });
        mLayout.setFadeOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });*/

        return mview;
    }

    private void initCustomSpinner(ArrayList<String> countries) {

        RequestSpinnerAdapter customSpinnerAdapter = new RequestSpinnerAdapter(getActivity(), countries);
        spinnerRequests.setAdapter(customSpinnerAdapter);
        spinnerRequests.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deal_request_id = arrRequestList.get(position).getRequest_id();
                LogUtils.i("DealsDescriptionFragment" + " onCreateView Request_name " + arrRequestList.get(position).getRequest_name() + " deal_request_id " + deal_request_id);
                if (Utils.isOnline()) {

                    if (deal_request_id != null && !deal_request_id.equals("")) {
                        apiManager.getDealRequestDetail(AppConstant.DEAL_REQUEST_DETAIL_ACTION, deal_request_id);
                    }
                } else {
                    Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void isError(String errorCode) {

    }

    @Override
    public void isSuccess(Object response, int ServiceCode) {
        DealRequestDetailResponse dealRequestDetailResponse = (DealRequestDetailResponse) response;
        String str_notes = dealRequestDetailResponse.getDeal_request_detail().getRequest_notes();
        if (str_notes != null &&
                !str_notes.equals("")) {
            tvNotes.setText(str_notes + "");
        }

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        lm.setReverseLayout(true);
        recyclerViewMessage.setLayoutManager(lm);
        recyclerViewMessage.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
        mChatMessageList = dealRequestDetailResponse.getDeal_request_msgs();
        if (friendId != null && !friendId.equals("") && friendName != null && !friendName.equals("")) {
            mAdapter = new DealDescriptionMessageAdapter(getActivity(), mChatMessageList, friendName, friendId, "");
            recyclerViewMessage.setAdapter(mAdapter);
            refreshData();
        }

    }

    class C05811 implements TextWatcher {

        class C05801 implements AnimationListener {
            C05801() {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ivSendMessage.setVisibility(View.VISIBLE);
            }

            public void onAnimationRepeat(Animation animation) {
            }
        }

        C05811() {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.length() == 1 && ivSendMessage.getVisibility() != View.VISIBLE) {
                ivSendMessage.startAnimation(animScaleOut);
                animScaleOut.setAnimationListener(new C05801());
            } else if (charSequence.length() == 0 && ivSendMessage.getVisibility() != View.GONE) {
                ivSendMessage.startAnimation(animScaleIn);
                ivSendMessage.setVisibility(View.GONE);
            }
        }

        public void afterTextChanged(Editable editable) {
        }
    }

    class C05822 implements OnClickListener {
        C05822() {
        }

        public void onClick(View view) {
            getActivity().onBackPressed();
        }
    }


    private void setClickEvents() {
        ivSendMessage.setOnClickListener(this);
        ivEmoticon.setOnClickListener(this);
        ivCamera.setOnClickListener(this);
        ivUploadFile.setOnClickListener(this);
    }

    private void setEditTexEvent() {
        etMessage.addTextChangedListener(new C05811());
    }

    private void setUpRecyclerView() {
        /*if (getArguments() != null) {
            this.mChatModel = (ChatModel) getArguments().getSerializable(BUNDLE_CHAT_FRIEND_MODEL);
            setupChatToolBarWithBackArrow(mChatModel, new GlideUtils(getActivity()));
            mChatMessageList.add(new ChatMessageModel(mChatModel.getName(), this.mChatModel.getImage(), "Hi", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "Hi", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel("You", "", "Hello", 1526119235, ChatMessageModel.Type.SENT));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "How are you?", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel("You", "", "I am fine. \nWhat about you?", 1526119235, ChatMessageModel.Type.SENT));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "Good!", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "Let's go for party this weekend. \nSince long time we didn't do any party. \nWhat you think?", 1526119235, ChatMessageModel.Type.RECEIVED));
            LinearLayoutManager lm = new LinearLayoutManager(getActivity());
            lm.setReverseLayout(true);
            recyclerViewMessage.setLayoutManager(lm);
            recyclerViewMessage.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
            mAdapter = new DealDescriptionMessageAdapter(getActivity(), mChatMessageList,recyclerViewMessage,"","","");
            recyclerViewMessage.setAdapter(mAdapter);
            refreshData();
            return;
        }*/
        Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    private void refreshData() {
        if (mChatMessageList.size() > 0) {
            recyclerViewMessage.setVisibility(View.VISIBLE);
            rel_NoMessageFound.setVisibility(View.GONE);
            return;
        }
        recyclerViewMessage.setVisibility(View.GONE);
        rel_NoMessageFound.setVisibility(View.VISIBLE);
        tvNoMessageFound.setText(getResources().getString(R.string.no_message_found));
        tvNoMessageFound.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_vector_no_chats, 0, 0);
    }

    public void setupChatToolBarWithBackArrow(ChatModel chatModel, GlideUtils glideUtils) {
        /*title = tvFriendName;
        TextView tvLastSeen =tvLastSeen;
        TextView tvFriendLetter = tvFriendLetter;
        ImageView ivBack = ivBack;
        ImageView ivFriendImage = ivFriendImage;
        ImageView ivOnlineIndicator = ivOnlineIndicator;*/
//        tvFriendName.setText(chatModel.getName() != null ? chatModel.getName() : "");
        tvLastSeen.setText(chatModel.isOnline() ? getResources().getString(R.string.online) : getResources().getString(R.string.last_seen_, new Object[]{TimeStamp.millisToTimeFormat(Long.valueOf(chatModel.getLastActive() * 1000))}));
//        ivOnlineIndicator.setVisibility(chatModel.isOnline() ? View.VISIBLE : View.GONE);
        if (chatModel.getImage() == null || chatModel.getImage().length() <= 0) {
            String title = chatModel.getName().toUpperCase();
            if (title.length() > 0) {
                tvFriendLetter.setText(title.substring(0, 1).toUpperCase());
            } else {
                tvFriendLetter.setText("T");
            }
            glideUtils.loadImageCircular("", ivFriendImage, R.drawable.shape_circle_filled_white);
            tvFriendLetter.setVisibility(View.VISIBLE);
        } else {
            glideUtils.loadImageCircular(chatModel.getImage(), ivFriendImage);
            tvFriendLetter.setVisibility(View.GONE);
        }
        ivBack.setOnClickListener(new C05822());
    }

    public void onClick(View view) {
        /*switch (view.getId()) {
            case R.id.ivSendMessage:
                if (etMessage.length() > 0) {
                    mChatMessageList.add(0, new ChatMessageModel("You", "", etMessage.getText().toString(), new Date().getTime() / 1000, ChatMessageModel.Type.SENT));
                    mAdapter.notifyDataSetChanged();
                    etMessage.setText("");
                    if (mChatMessageList.size() == 1) {
                        refreshData();
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }*/
    }
}
