package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.syneotek001.androidtabsexample25042018.Adapter.CustomSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;

import java.util.ArrayList;

public class AddTicketFragment extends Fragment {


    View view;
    Spinner spinnerCustom;
    ArrayList<String> countries;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_add_ticket, container, false);
        ImageView ivBack = view.findViewById(R.id.iv_Back);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        spinnerCustom = view.findViewById(R.id.spinnerCustom);
        countries = new ArrayList<>();
        countries.add("New Ticket");
        countries.add("#1268952659");
        countries.add("#1268952659");
        countries.add("#1268952659");
        countries.add("#1268952659");
        countries.add("#1268952659");
        initCustomSpinner(countries);

        return view;
    }

    private void initCustomSpinner(ArrayList<String> countries) {

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(), countries);
        spinnerCustom.setAdapter(customSpinnerAdapter);
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
    }
}
