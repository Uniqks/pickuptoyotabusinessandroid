package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.BaseFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentAnalyticsHolderBinding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

public class AnalyticHolderFragment extends Fragment implements OnBackPressed {

    public TextView title;
    Toolbar toolbar;
    ImageView ivMenu;
    Context mcontext;
    public AnalyticHolderFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_analytics_holder, container, false);
        mcontext=getActivity();
        toolbar=view.findViewById(R.id.toolbar);
        setupToolBarWithCustomMenu(toolbar, getActivity().getResources().getString(R.string.dashboard));
        ivMenu= view.findViewById(R.id.ivMenu);
         ivMenu.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 ((HomeActivity)getActivity()).opendrawer();
             }
         });
        return view;
    }
    public void setupToolBarWithCustomMenu(Toolbar toolbar, String Title) {
        this.title = toolbar.findViewById(R.id.tvTitle);
        TextView textView = this.title;
        if (Title == null) {
            Title = "";
        }
        textView.setText(Title);
         ivMenu = toolbar.findViewById(R.id.ivMenu);
        ivMenu.setVisibility(View.VISIBLE);
//        ivMenu.setOnClickListener(new C05651());
    }

    @Override
    public void onBackPressed() {
       getActivity().getSupportFragmentManager().popBackStack();
    }
}