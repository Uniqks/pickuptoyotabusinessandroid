package com.example.syneotek001.androidtabsexample25042018.model;

public class DealsModel {
    private String customerId = "";
    private String dealId = "";
    private String salesManName = "";
    private String status = "";

    public DealsModel(String salesManName, String dealId, String customerId, String status) {
        this.salesManName = salesManName;
        this.dealId = dealId;
        this.customerId = customerId;
        this.status = status;
    }

    public String getSalesManName() {
        return this.salesManName;
    }

    public void setSalesManName(String salesManName) {
        this.salesManName = salesManName;
    }

    public String getDealId() {
        return this.dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString() {
        return "DealsModel{salesManName='" + this.salesManName + '\'' + ", dealId='" + this.dealId + '\'' + ", customerId='" + this.customerId + '\'' + ", status='" + this.status + '\'' + '}';
    }
}
