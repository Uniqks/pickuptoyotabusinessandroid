package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.TermsActivity;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentPurchaser2Binding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentPurchaser5Binding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class PurchaserFragment5 extends Fragment implements OnClickListener, OnBackPressed {
    public Button btnSave, btnSaveAndNext;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;
     //FragmentSalesmanProfileStep1Binding binding;
    FragmentPurchaser5Binding binding;
    String name, email, mobile, password, address;
    YoYo yoYo;
    ArrayList<String> arr_salesman_status = new ArrayList<>();

    Typeface typeface;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_purchaser5, container, false);
        View view = binding.getRoot();

        typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/montserrat_regular.otf");

        TextPaint tp = new TextPaint();
        tp.linkColor = getResources().getColor(R.color.colorPrimary);           //not quite sure what the format should be
        UnderlineSpan us = new UnderlineSpan();
        us.updateDrawState(tp);
        SpannableString content = new SpannableString(getString(R.string.txt_terms_privacy));
        content.setSpan(us, 0, content.length(), 0);
        binding.tvIncludeTaxes.setText(content);
        binding.tvIncludeTaxes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


				/*Uri uri = Uri.parse(getString(R.string.tv_terms_link));
				Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
				startActivityForResult(goToMarket,1);*/
                Intent i =new Intent(getActivity(), TermsActivity.class);
                startActivity(i);

            }
        });


        binding.ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });





        binding.rbDailyRental.setTypeface(typeface);
        binding.rbKM.setTypeface(typeface);
        binding.rbMI.setTypeface(typeface);
        binding.rbPoliceEmergencyVehicle.setTypeface(typeface);
        binding.rbTaxiLimousine.setTypeface(typeface);



        setHasOptionsMenu(false);
        setClickEvents();

        setAnimation(binding.lblColour);
        setAnimation(binding.lblDistanceTravelled);
        setAnimation(binding.lblHSTOnTradeIn);
        setAnimation(binding.lblHSTRegNo);
        setAnimation(binding.lblLienHolder);
        setAnimation(binding.lblMake);
        setAnimation(binding.lblModel);
        setAnimation(binding.lblOutstandingLienAmount);
        setAnimation(binding.lblRemarks);
        setAnimation(binding.lblRentType);
        setAnimation(binding.lblVehicleTradedInOn);
        setAnimation(binding.lblVIN);
        setAnimation(binding.lblYear);

        setAnimation(binding.etBodyType);
        setAnimation(binding.etColour);
        setAnimation(binding.etHSTOnTradeIn);
        setAnimation(binding.etHSTRegNo);
        setAnimation(binding.etLienHolder);
        setAnimation(binding.etMake);
        setAnimation(binding.etModel);
        setAnimation(binding.etOutstandingLienAmount);
        setAnimation(binding.etRemarks);
        setAnimation(binding.etTrim);
        setAnimation(binding.etVehicleTradedInOn);
        setAnimation(binding.etVIN);
        setAnimation(binding.etYear);
        setAnimation(binding.tvIncludeTaxes);

        arr_salesman_status = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_salesman_status)));


        return view;
    }



    public void setAnimation(View view) {
        YoYo.with(Techniques.FlipInX).duration(1000).delay(150).repeat(0).playOn(view);
    }

    private void setClickEvents() {
        binding.btnSaveAndNext.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSaveAndNext:
                Log.e("btnSaveAndNext","btnSaveAndNext");
                PurchaserFragment6 billSaleFragment2 = new PurchaserFragment6();
                 ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
              /*  if (getArguments() != null) {
                    if (getArguments().getBoolean("is_edit")) {
                        if (getArguments().getSerializable("salesman_profile") != null) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("salesman_profile", getArguments().getSerializable("salesman_profile"));
                            bundle.putBoolean("is_edit", true);
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    } else {
                        if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("is_edit", false);
                            bundle.putString("profile_type",getArguments().getString("profile_type"));
                            billSaleFragment2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(billSaleFragment2);
                        }
                    }
                }*/
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
