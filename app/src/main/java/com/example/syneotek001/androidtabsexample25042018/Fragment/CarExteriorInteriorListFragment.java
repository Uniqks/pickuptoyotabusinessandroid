package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;


import com.example.syneotek001.androidtabsexample25042018.Adapter.CarExteriorInteriorItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.CarExteriorInteriorModel;

import java.util.ArrayList;

public class CarExteriorInteriorListFragment extends Fragment implements OnBackPressed {
    static final /* synthetic */ boolean $assertionsDisabled = (!CarExteriorInteriorListFragment.class.desiredAssertionStatus());
    public static String BUNDLE_DATA_IS_SELECTABLE = "is_item_selectable";
    String carModel = "";
    boolean isItemSelectable;
    CarExteriorInteriorItemAdapter mAdapter;
    ArrayList<CarExteriorInteriorModel> mCarExteriorList = new ArrayList<>();
    ArrayList<CarExteriorInteriorModel> mCarInteriorList = new ArrayList<>();

    public RecyclerView recyclerView;

    public TextView tvNoCarFound;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        
            View view = inflater.inflate(R.layout.fragment_car_listtwo, container, false);
        recyclerView=view.findViewById(R.id.recyclerView);
        tvNoCarFound=view.findViewById(R.id.tvNoCarFound);
//            this.mBinding = (FragmentCarListBinding) DataBindingUtil.inflate(inflater, R.layout.fragment_car_list, container, false);
//            this.view = this.mBinding.getRoot();
            setUpRecyclerView();
        setHasOptionsMenu(false);
        return view;
    }

    private void setUpRecyclerView() {
        if (getArguments() != null) {
            isItemSelectable = getArguments().getBoolean(BUNDLE_DATA_IS_SELECTABLE);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
          recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
//            carModel = getArguments().getString("carModel");
//            Logger.m6e("CarModel Is: ", String.valueOf(this.carModel));
            String accessoryType = getArguments().getString("accessoryType");
            if ($assertionsDisabled || accessoryType != null) {
                if (accessoryType.equals(getResources().getString(R.string.exterior_accessories))) {
                   mCarExteriorList.add(new CarExteriorInteriorModel("1", "Pro Series Paint Protection Film", "440.00*", "camry_exterior_pro_series_pain_protection_film", "E", "Guard your Camry from weathering, UV radiation and road debris that can chip and scratch the finish with 3M Pro Series Genuine Toyota Paint Protection Film.", 1, false));
                   mCarExteriorList.add(new CarExteriorInteriorModel("2", "Side Window Deflectors", "302.50*", "camry_exterior_side_window_deflectors", "E", "These Toyota Genuine Side Window Deflectors are aerodynamically shaped to minimize wind noise and buffering when driving with open windows.", 1, false));
                   mCarExteriorList.add(new CarExteriorInteriorModel("3", "Hood Deflector", "204.00*", "camry_exterior_hood_deflector", "E", "Made from thick high-grade tinted acrylic, the Hood Deflector offers high impact resistance and reduces the potential damage to your hood from road debris.", 1, false));
                    mCarExteriorList.add(new CarExteriorInteriorModel("4", "Rear Bumper Applique - Clear", "109.50*", "camry_exterior_rear_bumper_applique_clear", "E", "Prevent scrapes and scratches from daily vehicle use with Toyota’s Rear Bumper Appliqué. It is specifically designed to maintain your vehicle’s flawless bumper surface. This durable, clear and UV-resistant protection film is custom fitted to your vehicle and is stylishly outfitted with the Toyota logo. It’s the stain, chemical and fuel resistant barrier your paint has been waiting for.", 1, false));
                    mAdapter = new CarExteriorInteriorItemAdapter(getActivity(), this.mCarExteriorList, this.isItemSelectable);
                } else if (accessoryType.equals(getResources().getString(R.string.interior_accessories))) {
                   mCarInteriorList.add(new CarExteriorInteriorModel("1", "Cargo Liner", "126.50 *", "camry_interior_cargo_liner", "I", "The Cargo Tray provides maximum protection for your cargo area and features a skid-resistant surface to help keep valuables stable.", 1, false));
                   mCarInteriorList.add(new CarExteriorInteriorModel("2", "Kit: Door Sill Protectors", "292.50 **", "camry_interior_kit_door_sill_protectors", "I", "The custom-fit door sill enhancements replace your factory door scuff plates, providing a stylish, custom look to your Camry. Featuring a stainless steel overlay with embossed Camry logo and acrylic window, the door sill enhancements add to the vehicle’s interior presentation. Kit of 4 (front and rear).", 2, false));
                    mCarInteriorList.add(new CarExteriorInteriorModel("3", "Rear Saddle Tray", "55.50 *", "camry_interior_rear_saddle_tray", "I", "Customize the interior storage of your Camry with this high quality, Toyota Genuine Rear Saddle Tray. Designed to sit on the floor behind the centre console, it features a rubberized anti-slip design to help hold your belongings in place while you drive. The Rear Saddle Tray is easy to install and it provides a secure fit with either Toyota Carpet Mats, or Tub Style All Season Floor Mats.", 1, false));
                   mAdapter = new CarExteriorInteriorItemAdapter(getActivity(), this.mCarInteriorList, this.isItemSelectable);
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                   getActivity().onBackPressed();
                }
               recyclerView.setAdapter(mAdapter);
                return;
            }
            throw new AssertionError();
        }
    }

    @Override
    public void onBackPressed() {

    }
}
