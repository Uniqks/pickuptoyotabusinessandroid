package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.TaskManagerItemModel;

import java.util.ArrayList;
import java.util.Iterator;


public class TaskManagerItemAdapter extends RecyclerView.Adapter<TaskManagerItemAdapter.MyViewHolder> implements OnClickListener {
    private int lastSelectedPosition = 1;
    private Context mContext;
    private ArrayList<TaskManagerItemModel> mData;
    private OnLeadItemViewClickListener onItemClickListener;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivInfo:
                if (TaskManagerItemAdapter.this.onItemClickListener != null) {
                    TaskManagerItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivLeadLabel:
                if (TaskManagerItemAdapter.this.onItemClickListener != null) {
                    TaskManagerItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivEdit:
                if (TaskManagerItemAdapter.this.onItemClickListener != null) {
                    TaskManagerItemAdapter.this.onItemClickListener.onLeadEmailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivSchedule:
                if (TaskManagerItemAdapter.this.onItemClickListener != null) {
                    TaskManagerItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            default:
                return;
        }
    }

    class MyViewHolder extends ViewHolder implements OnClickListener {

        CardView cvLead;
        ImageView ivEdit, ivDelete, ivInfo,ivFavourite;
        LinearLayout llActionItems;
        LinearLayout /*ll_Category,*/ ll_TaskStatus, ll_TaskDateTime, ll_TaskNotes;
        TextView tvTaskStatus, tvTaskCategory, tvName, tvTaskDateTime, tvTaskNotes,tvHeadTaskTitle,tvTaskTitle;
        Button btnStatusFollowUp;

        MyViewHolder(View view) {
            super(view);
            cvLead = view.findViewById(R.id.cvLead);
            ivEdit = view.findViewById(R.id.ivEdit);
            ivDelete = view.findViewById(R.id.ivDelete);
            ivInfo = view.findViewById(R.id.ivInfo);
            llActionItems = view.findViewById(R.id.llActionItems);
            tvTaskStatus = view.findViewById(R.id.tvTaskStatus);
            tvTaskCategory = view.findViewById(R.id.tvTaskCategory);
            tvName = view.findViewById(R.id.tvName);
            tvTaskDateTime = view.findViewById(R.id.tvTaskDateTime);
            tvTaskNotes = view.findViewById(R.id.tvTaskNotes);
            tvHeadTaskTitle = view.findViewById(R.id.tvHeadTaskTitle);
            tvTaskTitle = view.findViewById(R.id.tvTaskTitle);

//            ll_Category = view.findViewById(R.id.ll_Category);
            ll_TaskStatus = view.findViewById(R.id.ll_TaskStatus);
            ll_TaskDateTime = view.findViewById(R.id.ll_TaskDateTime);
            ll_TaskNotes = view.findViewById(R.id.ll_TaskNotes);
            btnStatusFollowUp = view.findViewById(R.id.btnStatusFollowUp);
            ivFavourite = view.findViewById(R.id.ivFavourite);

        }

        @Override
        public void onClick(View v) {

        }
    }

    public TaskManagerItemAdapter(Context context, ArrayList<TaskManagerItemModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_task_manager, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        TaskManagerItemModel mLeadItem = (TaskManagerItemModel) mData.get(position);
        holder.tvTaskCategory.setText(mLeadItem.getTask_category());
        holder.tvTaskDateTime.setText(mLeadItem.getTask_date());
        holder.tvTaskNotes.setText(mLeadItem.getTask_notes());
        holder.tvHeadTaskTitle.setText(mLeadItem.getTask_title());
        holder.tvTaskTitle.setText(mLeadItem.getTask_title());
        String taskStatus = mLeadItem.getTask_status();

        if (taskStatus.equals("1")) {
            holder.btnStatusFollowUp.setText(mContext.getString(R.string.str_task_status_high));
            holder.tvTaskStatus.setText(mContext.getString(R.string.str_task_status_high));
            holder.btnStatusFollowUp.setBackgroundResource(R.drawable.bg_task_status_pending);
        } else if (taskStatus.equals("2")) {
            holder.btnStatusFollowUp.setText(mContext.getString(R.string.str_task_status_normal));
            holder.tvTaskStatus.setText(mContext.getString(R.string.str_task_status_normal));
            holder.btnStatusFollowUp.setBackgroundResource(R.drawable.bg_task_status_not_reply);
        } else if (taskStatus.equals("3")) {
            holder.btnStatusFollowUp.setText(mContext.getString(R.string.str_task_status_low));
            holder.tvTaskStatus.setText(mContext.getString(R.string.str_task_status_low));
            holder.btnStatusFollowUp.setBackgroundResource(R.drawable.bg_task_status_follow_up);
        } /*else if (taskStatus.equals("4")) {
            holder.btnStatusFollowUp.setText(mContext.getString(R.string.str_task_status_completed));
            holder.tvTaskStatus.setText(mContext.getString(R.string.str_task_status_completed));
            holder.btnStatusFollowUp.setBackgroundResource(R.drawable.bg_task_status_completed);
        }*/

        if (mData.get(position).isFavourite()) {
            holder.ivFavourite.setImageResource(R.drawable.ic_star_black_24dp);
        } else if (!mData.get(position).isFavourite()) {
            holder.ivFavourite.setImageResource(R.drawable.ic_star_border_black_24dp);
        }

        Iterator it;
        ArrayList<View> viewList;
        if (mLeadItem.isSelected()) {
            viewList = new ArrayList<>();
//            viewList.add(holder.ll_Category);
            viewList.add(holder.ll_TaskStatus);
            viewList.add(holder.ivFavourite);
            viewList.add(holder.ll_TaskDateTime);
            viewList.add((holder.ll_TaskNotes));
            viewList.add((holder.btnStatusFollowUp));
            viewList.add(holder.llActionItems);
            it = viewList.iterator();

            while (it.hasNext()) {
                View view = (View) it.next();
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0.0f);
                view.animate().alpha(1.0f).setListener(null);
            }
            return;
        }
        viewList = new ArrayList<>();
//        viewList.add(holder.ll_Category);
        viewList.add(holder.ll_TaskStatus);
        viewList.add(holder.ivFavourite);
        viewList.add(holder.ll_TaskDateTime);
        viewList.add(holder.ll_TaskNotes);
        viewList.add(holder.btnStatusFollowUp);
        viewList.add(holder.llActionItems);
        it = viewList.iterator();
        while (it.hasNext()) {
            final View view = (View) it.next();
            view.animate().alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            });
        }

        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    TaskManagerItemModel mLeadItem = (TaskManagerItemModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);
                }
            }
        });



        holder.ivInfo.setOnClickListener(this);
        holder.ivEdit.setOnClickListener(this);
        holder.ivDelete.setOnClickListener(this);
        holder.ivFavourite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TaskManagerItemModel mLeadItem = (TaskManagerItemModel) mData.get(position);
                if (mLeadItem.isFavourite())
                    mLeadItem.setFavourite(false);
                else if (!mLeadItem.isFavourite())
                    mLeadItem.setFavourite(true);
                notifyItemChanged(position, mLeadItem);
            }
        });


    }


    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
