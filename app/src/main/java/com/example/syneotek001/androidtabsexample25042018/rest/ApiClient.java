package com.example.syneotek001.androidtabsexample25042018.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {


    static String BASE_URL = "https://pickeringtoyota.com/mastercrm-latest/";

//    https://pickeringtoyota.com/mastercrm-latest/chat/user-uploads/

//    https://pickeringtoyota.com/toyotasalesadmin/salesmanimages

    public static String SALES_MAN_PROFILE_IMG = "https://pickeringtoyota.com/toyotasalesadmin/salesmanimages/";
    public static String CHAT_USER_UPLOADS = "https://pickeringtoyota.com/toyotasalesadmin/chat/user-uploads/";

    static String API_BASE_URL = BASE_URL + "webservices/";


    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }


}
